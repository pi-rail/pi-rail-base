// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.rail.track;

import de.pidata.models.tree.ChildList;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.lang.Integer;
import java.util.Hashtable;

public class PanelRef extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://res.pirail.org/pi-rail-track.xsd" );

  public static final QName ID_REFID = NAMESPACE.getQName( "refID" );
  public static final QName ID_X = NAMESPACE.getQName( "x" );
  public static final QName ID_Y = NAMESPACE.getQName( "y" );

  public PanelRef( Key id ) {
    super( id, PiRailTrackFactory.PANELREF_TYPE, null, null, null );
  }

  public PanelRef( Key key, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, PiRailTrackFactory.PANELREF_TYPE, attributeNames, anyAttribs, childNames );
  }

  protected PanelRef( Key key, ComplexType type, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, type, attributeNames, anyAttribs, childNames );
  }

  /**
   * Returns the attribute refID.
   *
   * @return The attribute refID
   */
  public QName getRefID() {
    return (QName) get( ID_REFID );
  }

  /**
   * Returns the attribute x.
   *
   * @return The attribute x
   */
  public Integer getX() {
    return (Integer) get( ID_X );
  }

  /**
   * Set the attribute x.
   *
   * @param x new value for attribute x
   */
  public void setX( Integer x ) {
    set( ID_X, x );
  }

  /**
   * Returns the attribute y.
   *
   * @return The attribute y
   */
  public Integer getY() {
    return (Integer) get( ID_Y );
  }

  /**
   * Set the attribute y.
   *
   * @param y new value for attribute y
   */
  public void setY( Integer y ) {
    set( ID_Y, y );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen

  public PanelRef( PanelCfg panelCfg, int x, int y ) {
    this( panelCfg.getId() );
    setX( Integer.valueOf( x ) );
    setY( Integer.valueOf( y ) );
  }

  public int getXInt() {
    Integer value = getX();
    if (value == null) {
      return 0;
    }
    else {
      return value.intValue();
    }
  }

  public int getYInt() {
    Integer value = getY();
    if (value == null) {
      return 0;
    }
    else {
      return value.intValue();
    }
  }
}
