// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.rail.model;

import de.pidata.models.tree.ChildList;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.string.Helper;

import java.lang.String;
import java.util.Hashtable;

public class Csv extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://res.pirail.org/pi-rail.xsd" );

  public static final QName ID_DAT = NAMESPACE.getQName( "dat" );
  public static final QName ID_I = NAMESPACE.getQName( "i" );
  public static final QName ID_T = NAMESPACE.getQName( "t" );

  public Csv() {
    super( null, PiRailFactory.CSV_TYPE, null, null, null );
  }

  public Csv( Key key, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, PiRailFactory.CSV_TYPE, attributeNames, anyAttribs, childNames );
  }

  protected Csv( Key key, ComplexType type, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, type, attributeNames, anyAttribs, childNames );
  }

  /**
   * Returns the attribute t.
   *
   * @return The attribute t
   */
  public CsvType getT() {
    return (CsvType) get( ID_T );
  }

  /**
   * Set the attribute t.
   *
   * @param t new value for attribute t
   */
  public void setT( CsvType t ) {
    set( ID_T, t );
  }

  /**
   * Returns the attribute i.
   *
   * @return The attribute i
   */
  public Integer getI() {
    return (Integer) get( ID_I );
  }

  /**
   * Set the attribute i.
   *
   * @param i new value for attribute i
   */
  public void setI( Integer i ) {
    set( ID_I, i );
  }

  /**
   * Returns the attribute dat.
   *
   * @return The attribute dat
   */
  public String getDat() {
    return (String) get( ID_DAT );
  }

  /**
   * Set the attribute dat.
   *
   * @param dat new value for attribute dat
   */
  public void setDat( String dat ) {
    set( ID_DAT, dat );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen

  public int getIndex() {
    Integer i = getI();
    if (i == null) {
      return 0;
    }
    else {
      return i.intValue();
    }
  }

  public boolean isEmpty() {
    String dat = getDat();
    return Helper.isNullOrEmpty( dat );
  }
}
