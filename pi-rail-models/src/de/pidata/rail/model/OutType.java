// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.rail.model;

import java.lang.String;

public enum OutType {

  Logic( "Logic" ),
  LowSide( "LowSide" ),
  HighSide( "HighSide" ),
  Servo( "Servo" ),
  HalfBridge( "HalfBridge" );

  private String value;

  OutType( String value ) {
    this.value = value;
  }

  /**
   * Returns the String value of the OutType instance.
   *
   * @return The String value of the OutType instance.
   */
  public String getValue() {
    return this.value;
  }

  /**
   * Returns the OutType instance associated with the given String value.
   *
   * @param stringValue The value to associate with an OutType instance
   * @return The OutType instance associated with the given String value.
   */
  public static OutType fromString( String stringValue ) {
    if (stringValue == null) {
      return null;
    }
    for (OutType entry : values()) {
      if (entry.value.equals( stringValue )) {
        return entry;
      }
    }
    return null;
  }

  /**
   * Returns the name of this enum constant as declared in the schema.
   *
   * @return The name of this enum constant as declared in the schema.
   */
  @Override
  public String toString() {
    return value;
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen
}
