// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.rail.model;

import de.pidata.models.tree.ChildList;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.QNameType;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.system.base.SystemManager;

import java.lang.Boolean;
import java.lang.Integer;
import java.lang.Short;
import java.util.Hashtable;

public class InCfg extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://res.pirail.org/pi-rail.xsd" );

  public static final QName ID_ID = NAMESPACE.getQName( "id" );
  public static final QName ID_PIN = NAMESPACE.getQName( "pin" );
  public static final QName ID_PULLUP = NAMESPACE.getQName( "pullup" );

  public InCfg( Key id ) {
    super( id, PiRailFactory.INCFG_TYPE, null, null, null );
  }

  public InCfg( Key key, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, PiRailFactory.INCFG_TYPE, attributeNames, anyAttribs, childNames );
  }

  protected InCfg( Key key, ComplexType type, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, type, attributeNames, anyAttribs, childNames );
  }

  /**
   * Returns the attribute id.
   *
   * @return The attribute id
   */
  public QName getId() {
    return (QName) get( ID_ID );
  }

  /**
   * Returns the attribute pin.
   *
   * @return The attribute pin
   */
  public Integer getPin() {
    return (Integer) get( ID_PIN );
  }

  /**
   * Set the attribute pin.
   *
   * @param pin new value for attribute pin
   */
  public void setPin( Integer pin ) {
    set( ID_PIN, pin );
  }

  /**
   * Returns the attribute pullup.
   *
   * @return The attribute pullup
   */
  public Boolean getPullup() {
    return (Boolean) get( ID_PULLUP );
  }

  /**
   * Set the attribute pullup.
   *
   * @param pullup new value for attribute pullup
   */
  public void setPullup( Boolean pullup ) {
    set( ID_PULLUP, pullup );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen

  public static ComplexType TRANSIENT_TYPE;
  public static final QName ID_KIND = NAMESPACE.getQName( "kind" );
  public static final QName ID_PRODUCT = NAMESPACE.getQName( "product" );
  public static final QName ID_PRODUCT_PIN = NAMESPACE.getQName( "productPin" );
  public static final QName ID_ACTION = NAMESPACE.getQName( "action" );
  public static final QName ID_LABEL = NAMESPACE.getQName( "label" );

  private InPin connectedPin = null;

  @Override
  public ComplexType transientType() {
    if(TRANSIENT_TYPE == null){
      DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "InCfg_Transient" ), InCfg.class.getName(), 0 );
      TRANSIENT_TYPE = type;
      type.addAttributeType( ID_KIND, StringType.getDefString() );
      type.addAttributeType( ID_PRODUCT, QNameType.getQName() );
      type.addAttributeType( ID_PRODUCT_PIN, QNameType.getQName() );
      type.addAttributeType( ID_ACTION, StringType.getDefString() );
      type.addAttributeType( ID_LABEL, StringType.getDefString() );
    }
    return TRANSIENT_TYPE;
  }

  private QName getProductPin( InPin inPin ) {
    if (inPin == null) {
      return null;
    }
    else {
      return inPin.getId();
    }
  }

  private QName getProduct( InPin inPin ) {
    if (inPin == null) {
      return null;
    }
    else {
      ItemConn itemConn = (ItemConn) inPin.getParent( false );
      if (itemConn == null) {
        return null;
      }
      else {
        return itemConn.getId();
      }
    }
  }
  @Override
  public Object transientGet( int transientIndex) {
    QName attributeName = transientType().getAttributeName( transientIndex );
    if (attributeName == ID_KIND) {
      return "In";
    }
    else if (attributeName == ID_PRODUCT) {
      return getProduct( connectedPin );
    }
    else if (attributeName == ID_PRODUCT_PIN) {
      return getProductPin( connectedPin );
    }
    else if (attributeName == ID_ACTION) {
      if (connectedPin == null) {
        return SystemManager.getInstance().getLocalizedMessage( "attach_BTN", null, null );
      }
      else {
        return SystemManager.getInstance().getLocalizedMessage( "detach_BTN", null, null );
      }
    }
    else if (attributeName == ID_LABEL) {
      return getLabel();
    }
    else {
      return super.transientGet( transientIndex );
    }
  }

  public InPin getConnectedPin() {
    return connectedPin;
  }

  public void setConnectedPin( InPin connectedPin ) {
    InPin oldPin = this.connectedPin;
    this.connectedPin = connectedPin;
    if (oldPin == connectedPin) {
      fireDataChanged( ID_PRODUCT, getProduct( oldPin ), getProduct( connectedPin ) );
      fireDataChanged( ID_PRODUCT_PIN, getProductPin( oldPin ), getProductPin( connectedPin ) );
      fireDataChanged( ID_ACTION, oldPin, connectedPin );
      fireDataChanged( ID_LABEL, null, getLabel() );
    }
  }

  public String getLabel() {
    String typeStr = SystemManager.getInstance().getGlossaryString( "inputPin" );
    if (connectedPin == null) {
      return getId().getName() + ": " + typeStr + " #" + getPin() + " <--> " + SystemManager.getInstance().getGlossaryString( "pinOPEN" );
    }
    else {
      return getId().getName() + ": " + typeStr + " #" + getPin() + " <--> "
          + getProduct( connectedPin ).getName() + "." + getProductPin( connectedPin ).getName();
    }
  }

  public String toString() {
    return "InCfg "+getLabel();
  }
}
