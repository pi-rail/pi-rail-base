// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.rail.model;

import de.pidata.models.tree.ChildList;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.system.base.SystemManager;

import java.lang.Integer;
import java.lang.Short;
import java.util.Hashtable;

public class PortPin extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://res.pirail.org/pi-rail.xsd" );

  public static final QName ID_PIN = NAMESPACE.getQName( "pin" );
  public static final QName ID_TYPE = NAMESPACE.getQName( "type" );

  public PortPin() {
    super( null, PiRailFactory.PORTPIN_TYPE, null, null, null );
  }

  public PortPin( Key key, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, PiRailFactory.PORTPIN_TYPE, attributeNames, anyAttribs, childNames );
  }

  protected PortPin( Key key, ComplexType type, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, type, attributeNames, anyAttribs, childNames );
  }

  /**
   * Returns the attribute type.
   *
   * @return The attribute type
   */
  public PortPinType getType() {
    return (PortPinType) get( ID_TYPE );
  }

  /**
   * Set the attribute type.
   *
   * @param type new value for attribute type
   */
  public void setType( PortPinType type ) {
    set( ID_TYPE, type );
  }

  /**
   * Returns the attribute pin.
   *
   * @return The attribute pin
   */
  public Integer getPin() {
    return (Integer) get( ID_PIN );
  }

  /**
   * Set the attribute pin.
   *
   * @param pin new value for attribute pin
   */
  public void setPin( Integer pin ) {
    set( ID_PIN, pin );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen

  public static ComplexType TRANSIENT_TYPE;
  public static final QName ID_LABEL = NAMESPACE.getQName( "label" );

  @Override
  protected void initTransient() {
    super.initTransient();
  }

  @Override
  public ComplexType transientType() {
    if(TRANSIENT_TYPE == null){
      DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "IoCfg_Transient" ), EnumAction.class.getName(), 0, super.transientType() );
      TRANSIENT_TYPE = type;
      type.addAttributeType( ID_LABEL, StringType.getDefString() );
    }
    return TRANSIENT_TYPE;
  }

  @Override
  public Object transientGet( int transientIndex) {
    QName attributeName = transientType().getAttributeName( transientIndex );
    if (attributeName == ID_LABEL) {
      return getLabel();
    }
    else{
      return super.transientGet( transientIndex );
    }
  }

  @Override
  protected void fireEvent( int eventID, Object source, QName elementID, Object oldValue, Object newValue ) {
    super.fireEvent( eventID, source, elementID, oldValue, newValue );
    if ((elementID == ID_PIN) || (elementID == ID_TYPE)) {
      fireDataChanged( ID_LABEL, null, getLabel() );
    }
  }

  public String getLabel() {
    String typeStr = SystemManager.getInstance().getGlossaryString( "pin" );
    return typeStr + " #" + getPin() + " (" +getType() + ")";
  }
}
