// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.rail.model;

import de.pidata.models.tree.ChildList;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.lang.Boolean;
import java.lang.Integer;
import java.lang.Short;
import java.lang.String;
import java.util.Hashtable;

public class MoCmd extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://res.pirail.org/pi-rail.xsd" );

  public static final QName ID_DIR = NAMESPACE.getQName( "dir" );
  public static final QName ID_ID = NAMESPACE.getQName( "id" );
  public static final QName ID_IMMEDIATE = NAMESPACE.getQName( "immediate" );
  public static final QName ID_SPEED = NAMESPACE.getQName( "speed" );

  public MoCmd() {
    super( null, PiRailFactory.MOCMD_TYPE, null, null, null );
  }

  public MoCmd( Key key, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, PiRailFactory.MOCMD_TYPE, attributeNames, anyAttribs, childNames );
  }

  protected MoCmd( Key key, ComplexType type, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, type, attributeNames, anyAttribs, childNames );
  }

  /**
   * Returns the attribute id.
   *
   * @return The attribute id
   */
  public QName getId() {
    return (QName) get( ID_ID );
  }

  /**
   * Set the attribute id.
   *
   * @param id new value for attribute id
   */
  public void setId( QName id ) {
    set( ID_ID, id );
  }

  /**
   * Returns the attribute speed.
   *
   * @return The attribute speed
   */
  public Integer getSpeed() {
    return (Integer) get( ID_SPEED );
  }

  /**
   * Set the attribute speed.
   *
   * @param speed new value for attribute speed
   */
  public void setSpeed( Integer speed ) {
    set( ID_SPEED, speed );
  }

  /**
   * Returns the attribute dir.
   *
   * @return The attribute dir
   */
  public String getDir() {
    return (String) get( ID_DIR );
  }

  /**
   * Set the attribute dir.
   *
   * @param dir new value for attribute dir
   */
  public void setDir( String dir ) {
    set( ID_DIR, dir );
  }

  /**
   * Returns the attribute immediate.
   *
   * @return The attribute immediate
   */
  public Boolean getImmediate() {
    return (Boolean) get( ID_IMMEDIATE );
  }

  /**
   * Set the attribute immediate.
   *
   * @param immediate new value for attribute immediate
   */
  public void setImmediate( Boolean immediate ) {
    set( ID_IMMEDIATE, immediate );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen

  public MoCmd( QName id, char dir, int speed, boolean immediate ) {
    this();
    setId( id );
    setSpeed( Integer.valueOf( speed ) );
    setDir( ""+dir );
    setImmediate( Boolean.valueOf( immediate ) );
  }

  public int getSpeedInt() {
    Integer value = getSpeed();
    if (value == null) {
      return 0;
    }
    else {
      return value.intValue();
    }
  }
}
