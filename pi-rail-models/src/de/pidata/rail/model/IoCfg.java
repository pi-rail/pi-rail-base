// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.rail.model;

import de.pidata.models.tree.*;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.QNameType;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.system.base.SystemManager;

import java.lang.Integer;
import java.util.Collection;
import java.util.Hashtable;

public class IoCfg extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://res.pirail.org/pi-rail.xsd" );

  public static final QName ID_EXTCFG = NAMESPACE.getQName( "extCfg" );
  public static final QName ID_FW = NAMESPACE.getQName( "fw" );
  public static final QName ID_INCFG = NAMESPACE.getQName( "inCfg" );
  public static final QName ID_OUTCFG = NAMESPACE.getQName( "outCfg" );
  public static final QName ID_PORTCFG = NAMESPACE.getQName( "portCfg" );
  public static final QName ID_VERSION = NAMESPACE.getQName( "version" );

  private final Collection<PortCfg> portCfgs = new ModelCollection<>( ID_PORTCFG, this );
  private final Collection<ExtCfg> extCfgs = new ModelCollection<>( ID_EXTCFG, this );
  private final Collection<OutCfg> outCfgs = new ModelCollection<>( ID_OUTCFG, this );
  private final Collection<InCfg> inCfgs = new ModelCollection<>( ID_INCFG, this );

  public IoCfg() {
    super( null, PiRailFactory.IOCFG_TYPE, null, null, null );
  }

  public IoCfg( Key key, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, PiRailFactory.IOCFG_TYPE, attributeNames, anyAttribs, childNames );
  }

  protected IoCfg( Key key, ComplexType type, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, type, attributeNames, anyAttribs, childNames );
  }

  /**
   * Returns the attribute fw.
   *
   * @return The attribute fw
   */
  public String getFw() {
    return (String) get( ID_FW );
  }

  /**
   * Set the attribute fw.
   *
   * @param fw new value for attribute fw
   */
  public void setFw( String fw ) {
    set( ID_FW, fw );
  }

  /**
   * Returns the attribute version.
   *
   * @return The attribute version
   */
  public String getVersion() {
    return (String) get( ID_VERSION );
  }

  /**
   * Set the attribute version.
   *
   * @param version new value for attribute version
   */
  public void setVersion( String version ) {
    set( ID_VERSION, version );
  }

  /**
   * Returns the portCfg element identified by the given key.
   *
   * @return the portCfg element identified by the given key
   */
  public PortCfg getPortCfg( Key portCfgID ) {
    return (PortCfg) get( ID_PORTCFG, portCfgID );
  }

  /**
   * Adds the portCfg element.
   *
   * @param portCfg the portCfg element to add
   */
  public void addPortCfg( PortCfg portCfg ) {
    add( ID_PORTCFG, portCfg );
  }

  /**
   * Removes the portCfg element.
   *
   * @param portCfg the portCfg element to remove
   */
  public void removePortCfg( PortCfg portCfg ) {
    remove( ID_PORTCFG, portCfg );
  }

  /**
   * Returns the portCfg iterator.
   *
   * @return the portCfg iterator
   */
  public ModelIterator<PortCfg> portCfgIter() {
    return iterator( ID_PORTCFG, null );
  }

  /**
   * Returns the number of portCfgs.
   *
   * @return the number of portCfgs
   */
  public int portCfgCount() {
    return childCount( ID_PORTCFG );
  }

  /**
   * Returns the portCfg collection.
   *
   * @return the portCfg collection
   */
  public Collection<PortCfg> getPortCfgs() {
    return portCfgs;
  }

  /**
   * Returns the extCfg element identified by the given key.
   *
   * @return the extCfg element identified by the given key
   */
  public ExtCfg getExtCfg( Key extCfgID ) {
    return (ExtCfg) get( ID_EXTCFG, extCfgID );
  }

  /**
   * Adds the extCfg element.
   *
   * @param extCfg the extCfg element to add
   */
  public void addExtCfg( ExtCfg extCfg ) {
    add( ID_EXTCFG, extCfg );
  }

  /**
   * Removes the extCfg element.
   *
   * @param extCfg the extCfg element to remove
   */
  public void removeExtCfg( ExtCfg extCfg ) {
    remove( ID_EXTCFG, extCfg );
  }

  /**
   * Returns the extCfg iterator.
   *
   * @return the extCfg iterator
   */
  public ModelIterator<ExtCfg> extCfgIter() {
    return iterator( ID_EXTCFG, null );
  }

  /**
   * Returns the number of extCfgs.
   *
   * @return the number of extCfgs
   */
  public int extCfgCount() {
    return childCount( ID_EXTCFG );
  }

  /**
   * Returns the extCfg collection.
   *
   * @return the extCfg collection
   */
  public Collection<ExtCfg> getExtCfgs() {
    return extCfgs;
  }

  /**
   * Returns the outCfg element identified by the given key.
   *
   * @return the outCfg element identified by the given key
   */
  public OutCfg getOutCfg( Key outCfgID ) {
    return (OutCfg) get( ID_OUTCFG, outCfgID );
  }

  /**
   * Adds the outCfg element.
   *
   * @param outCfg the outCfg element to add
   */
  public void addOutCfg( OutCfg outCfg ) {
    add( ID_OUTCFG, outCfg );
  }

  /**
   * Removes the outCfg element.
   *
   * @param outCfg the outCfg element to remove
   */
  public void removeOutCfg( OutCfg outCfg ) {
    remove( ID_OUTCFG, outCfg );
  }

  /**
   * Returns the outCfg iterator.
   *
   * @return the outCfg iterator
   */
  public ModelIterator<OutCfg> outCfgIter() {
    return iterator( ID_OUTCFG, null );
  }

  /**
   * Returns the number of outCfgs.
   *
   * @return the number of outCfgs
   */
  public int outCfgCount() {
    return childCount( ID_OUTCFG );
  }

  /**
   * Returns the outCfg collection.
   *
   * @return the outCfg collection
   */
  public Collection<OutCfg> getOutCfgs() {
    return outCfgs;
  }

  /**
   * Returns the inCfg element identified by the given key.
   *
   * @return the inCfg element identified by the given key
   */
  public InCfg getInCfg( Key inCfgID ) {
    return (InCfg) get( ID_INCFG, inCfgID );
  }

  /**
   * Adds the inCfg element.
   *
   * @param inCfg the inCfg element to add
   */
  public void addInCfg( InCfg inCfg ) {
    add( ID_INCFG, inCfg );
  }

  /**
   * Removes the inCfg element.
   *
   * @param inCfg the inCfg element to remove
   */
  public void removeInCfg( InCfg inCfg ) {
    remove( ID_INCFG, inCfg );
  }

  /**
   * Returns the inCfg iterator.
   *
   * @return the inCfg iterator
   */
  public ModelIterator<InCfg> inCfgIter() {
    return iterator( ID_INCFG, null );
  }

  /**
   * Returns the number of inCfgs.
   *
   * @return the number of inCfgs
   */
  public int inCfgCount() {
    return childCount( ID_INCFG );
  }

  /**
   * Returns the inCfg collection.
   *
   * @return the inCfg collection
   */
  public Collection<InCfg> getInCfgs() {
    return inCfgs;
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen

  public static ComplexType TRANSIENT_TYPE;
  public static final QName ID_LABEL = NAMESPACE.getQName( "label" );

  @Override
  protected void initTransient() {
    super.initTransient();
  }

  @Override
  public ComplexType transientType() {
    if(TRANSIENT_TYPE == null){
      DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "IoCfg_Transient" ), EnumAction.class.getName(), 0, super.transientType() );
      TRANSIENT_TYPE = type;
      type.addAttributeType( ID_LABEL, StringType.getDefString() );
    }
    return TRANSIENT_TYPE;
  }

  @Override
  public Object transientGet( int transientIndex) {
    QName attributeName = transientType().getAttributeName( transientIndex );
    if (attributeName == ID_LABEL) {
      return getLabel();
    }
    else{
      return super.transientGet( transientIndex );
    }
  }

  public String getLabel() {
    return SystemManager.getInstance().getGlossaryString( "connectors" ) ;
  }
}
