// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.rail.model;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.DefaultModel;
import de.pidata.models.tree.ModelCollection;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.util.Collection;
import java.util.Hashtable;

public class FileCfg extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://res.pirail.org/pi-rail.xsd" );

  public static final QName ID_FILEDEF = NAMESPACE.getQName( "fileDef" );
  public static final QName ID_FREE = NAMESPACE.getQName( "free" );

  private final Collection<FileDef> fileDefs = new ModelCollection<>( ID_FILEDEF, this );

  public FileCfg() {
    super( null, PiRailFactory.FILECFG_TYPE, null, null, null );
  }

  public FileCfg( Key key, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, PiRailFactory.FILECFG_TYPE, attributeNames, anyAttribs, childNames );
  }

  protected FileCfg( Key key, ComplexType type, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, type, attributeNames, anyAttribs, childNames );
  }

  /**
   * Returns the attribute free.
   *
   * @return The attribute free
   */
  public Integer getFree() {
    return (Integer) get( ID_FREE );
  }

  /**
   * Set the attribute free.
   *
   * @param free new value for attribute free
   */
  public void setFree( Integer free ) {
    set( ID_FREE, free );
  }

  /**
   * Returns the fileDef element identified by the given key.
   *
   * @return the fileDef element identified by the given key
   */
  public FileDef getFileDef( Key fileDefID ) {
    return (FileDef) get( ID_FILEDEF, fileDefID );
  }

  /**
   * Adds the fileDef element.
   *
   * @param fileDef the fileDef element to add
   */
  public void addFileDef( FileDef fileDef ) {
    add( ID_FILEDEF, fileDef );
  }

  /**
   * Removes the fileDef element.
   *
   * @param fileDef the fileDef element to remove
   */
  public void removeFileDef( FileDef fileDef ) {
    remove( ID_FILEDEF, fileDef );
  }

  /**
   * Returns the fileDef iterator.
   *
   * @return the fileDef iterator
   */
  public ModelIterator<FileDef> fileDefIter() {
    return iterator( ID_FILEDEF, null );
  }

  /**
   * Returns the number of fileDefs.
   *
   * @return the number of fileDefs
   */
  public int fileDefCount() {
    return childCount( ID_FILEDEF );
  }

  /**
   * Returns the fileDef collection.
   *
   * @return the fileDef collection
   */
  public Collection<FileDef> getFileDefs() {
    return fileDefs;
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen
}
