package de.pidata.rail.z21;

import de.pidata.binary.BinaryHelper;
import de.pidata.log.Logger;
import de.pidata.models.types.simple.DecimalObject;
import de.pidata.qnames.QName;
import de.pidata.rail.model.ActionState;
import de.pidata.rail.model.MotorState;
import de.pidata.rail.railway.*;

import java.io.IOException;
import java.net.InetAddress;

/**
 * - Z21 Server - simulates Z21 central
 * - represents the PI-Rail infrastructure via CTC App
 */
public class Z21Processor implements Z21Interface, ModelRailwayListener {

  public static final boolean LOG_SPEED = false;
  public static final boolean LOG_NOTIFY = false;

  private static final int DEFAULT_CAN_ID = 0x0001;
  private int z21CANNetId = DEFAULT_CAN_ID;

  private int[] z21SerialNumber = new int[4];
  private int[] z21HWType = new int[4];
  private int[] z21FWVersion = new int[4];

  private Z21Comm z21Comm;
  private ModelRailway modelRailway;

  public Z21Processor( ModelRailway modelRailway ) {
    this.modelRailway = modelRailway;
    this.z21Comm = new Z21Comm( this );

    z21SerialNumber[0] = 0x11; // TODO: same as HWType?
    z21SerialNumber[1] = 0x02;
    z21SerialNumber[2] = 0x00;
    z21SerialNumber[3] = 0x00;

    z21HWType[0] = 0x11; // D_HWT_Z21_XL
    z21HWType[1] = 0x02;
    z21HWType[2] = 0x00;
    z21HWType[3] = 0x00;

    z21FWVersion[0] = 0x01; // TODO for BROADCAST: max. >= 1.41
    z21FWVersion[1] = 0x42;
    z21FWVersion[2] = 0x00;
    z21FWVersion[3] = 0x00;

    modelRailway.addRailwayListener( this );
  }

  public Z21Comm getZ21Comm() {
    return z21Comm;
  }

  public boolean start() {
    return this.z21Comm.start();
  }

  public void stop() {
    z21Comm.stop();
  }

  @Override
  public int[] getSerialNumber() {
    return z21SerialNumber;
  }

  @Override
  public int[] getHWType() {
    return z21HWType;
  }

  @Override
  public int[] getFWVersion() {
    return z21FWVersion;
  }

  @Override
  public void notifyz21RailPower( int state ) {
    StringBuilder powerMsg = new StringBuilder( "notifyz21RailPower ");
    if (state == Z21Comm.csEmergencyStop) {
      powerMsg.append( "csEmergencyStop to all Locomotives" );
      Logger.info( powerMsg.toString() );
      for (Locomotive locomotive : modelRailway.locoIter()) {
        locomotive.emergencyStop();
      }
      // TODO: direct notification back to iTrain? -> LAN_X_BC_STOPPED
      //z21Comm.send(???);
      return;
    }

    // others: just log
    switch (state) {
      case Z21Comm.csNormal: {
        // no action - everything is fine
        powerMsg.append( "csNormal, no action" );
        break;
      }
      case Z21Comm.csTrackVoltageOff: {
        // no action - just information
        powerMsg.append( "csTrackVoltageOff, no action" );
        break;
      }
      case Z21Comm.csShortCircuit: {
        // no action
        powerMsg.append( "csShortCircuit, no action" );
        break;
      }
      case Z21Comm.csServiceMode: {
        // no action - just information
        powerMsg.append( "csServiceMode, no action" );
        break;
      }
      default: {
        powerMsg.append( state + "-ignored" );
      }
    }
    Logger.info( powerMsg.toString() );
  }

  @Override
  public void notifyz21EthSend( InetAddress client, int sendPort, int[] data ) {
    byte[] msgBytes = new byte[data.length];
    for (int i = 0; i < data.length; i++) {
      msgBytes[i] = (byte) data[i];
    }
    try {
      if (sendPort <= 0) {
        sendPort = Z21Comm.Z21_PORT;
      }
      z21Comm.send( client, sendPort, msgBytes );
    }
    catch (IOException e) {
      Logger.error( "Z21 send error", e );
    }
  }

  @Override
  public void notifyz21S88Data( int data ) {
    Logger.info( "-----notifyz21S88Data [" + data + "]" );
    //z21.setS88Data (datasend);  //Send back state of S88 Feedback
  }

  @Override
  /**
   * @return int-array: int Steps[0], int Speed[1], int F0[2], int F1[3], int F2[4], int F3[5]
   */
  public int[] requestZ21LocoState( int adr ) {
    Locomotive loco = modelRailway.getLoco( adr );
    int[] ldata = new int[6];
    ldata[0] = Z21Comm.DCCSTEP128; // TODO: might be different - get from?
    if (loco != null) {
      if (LOG_SPEED) {
        Logger.info( "   target percent=" + loco.getTargetSpeedPercent() );
      }
      int steps = 126; //default: DCC 128 steps minus 2x stops
      if (ldata[0] == Z21Comm.DCCSTEP28) {
        steps = 28;
      }
      else if (ldata[0] == Z21Comm.DCCSTEP14) {
        steps = 14;
      }
      ldata[1] = (int) Math.round( loco.getTargetSpeedPercent().doubleValue() * steps / 100 );
      if (LOG_SPEED) {
        Logger.info( "   int=" + ldata[1] + " dir=" + loco.getTargetDir() );
      }
      if (loco.getTargetDirChar() == MotorState.CHAR_FORWARD) {
        ldata[1] = BinaryHelper.bitSet( ldata[1], 7 ); // set to identify direction
      }
      if (loco.getDCCFunctionState( 0 )) {
        ldata[2] = ldata[2] | 0x10;
      }
      int bit = 1;
      for (int i = 1; i <= 4; i++) {
        if (loco.getDCCFunctionState( i )) {
          ldata[2] = ldata[2] | bit;
        }
        bit = bit << 1;
      }
      bit = 1;
      for (int i = 5; i <= 12; i++) {
        if (loco.getDCCFunctionState( i )) {
          ldata[3] = ldata[3] | bit;
        }
        bit = bit << 1;
      }
      bit = 1;
      for (int i = 13; i <= 20; i++) {
        if (loco.getDCCFunctionState( i )) {
          ldata[4] = ldata[4] | bit;
        }
        bit = bit << 1;
      }
      bit = 1;
      for (int i = 21; i <= 28; i++) {
        if (loco.getDCCFunctionState( i )) {
          ldata[5] = ldata[5] | bit;
        }
        bit = bit << 1;
      }
    }
    return ldata;
  }

  @Override
  public void setZ21LocoSpeed( int adr, int speed, int steps ) {
    Locomotive loco = modelRailway.getLoco( adr );
    if (loco == null) {
      Logger.info( "setZ21LocoSpeed loco for addr=" + adr + " not found" );
    }
    else {
      boolean isFwd = BinaryHelper.bitRead( speed, 7 );
      if (isFwd) {
        loco.setTargetDir( MotorState.DIR_FORWARD );
        speed = BinaryHelper.bitClear( speed, 7 ); // clear to get the positive speed value
      }
      else {
        loco.setTargetDir( MotorState.DIR_BACK );
      }
      Logger.info( "setZ21LocoSpeed addr=" + adr + ",fwd=" + isFwd + ", speed=" + speed + ", steps=" + steps );
      double speedPercent = ((double) speed) / steps * 100;
      loco.setTargetSpeedPercent( new DecimalObject( speedPercent, 1 ) );
      if (LOG_SPEED) {
        Logger.info( "   percent=" + speedPercent );
      }
    }
  }

  //--------------------------------------------------------------------------------------------

  /**
   *
   * @param adr  DCC loco address
   * @param type Type (0=AUS / 1=EIN / 2=UM)
   * @param fkt  function number (0-63)
   */
  @Override
  public void setZ21LocoFkt( int adr, int type, int fkt ) {
    Logger.info( "setZ21LocoFkt addr=" + adr + ", type=" + type + ", fkt=" + fkt );
    Locomotive loco = modelRailway.getLoco( adr );
    if (loco != null) {
      boolean newValue;
      if (type == 2) {
        newValue = !loco.getDCCFunctionState( fkt );
      }
      else {
        newValue = (type == 1);
      }
      loco.setDCCFunctionState( fkt, newValue );
    }
  }

  //--------------------------------------------------------------------------------------------

  @Override
  public void notifyz21Accessory( int addr, boolean state, boolean active ) {
    Logger.info( "notifyz21Accessory, adr=" + addr + ", state=" + state + ", active=" + active );
    RailAction railAction = modelRailway.getRailAction( addr );
    if ((railAction != null) && active) {
      railAction.setValueDCC( addr, state );
    }
  }

  @Override
  public boolean notifyz21AccessoryInfo( int addr ) {
    RailAction railAction = modelRailway.getRailAction( addr );
    if (railAction == null) {
      Logger.info( "notifyz21AccessoryInfo, addr="+addr+" unknown" );
      return false;
    }
    else {
      boolean value = railAction.getValueDCC( addr );
      Logger.info( "notifyz21AccessoryInfo, addr="+addr+", value="+value );
      return value;
    }
  }

  @Override
  public int notifyz21LNdispatch( int Adr2, int Adr ) {
    Logger.info( "TODO Z21: notifyz21LNdispatch" );
    //return the Slot that was dispatched, 0xFF at error!
    return 0xFF;
  }

  @Override
  public void notifyz21LNSendPacket( int[] data, int length ) {
    StringBuilder sb = new StringBuilder ( "TODO notifyz21LNSendPacket" );

    if (LOG_NOTIFY) {
      for (int i = 0; i < data.length; i++) {
        sb.append( Integer.toHexString( data[i] ) );
        sb.append( " " );
      }
      Logger.info( "to send [" + length + "]: " + sb.toString() + System.lineSeparator() );
    }

    // TODO: extract Sensor by Id, ignore?
  }

  @Override
  public void notifyz21CVREAD( int cvAdrMSB, int cvAdrLSB ) {
    Logger.info( "TODO Z21: notifyz21CVREAD" );
  }

  @Override
  public void notifyz21CVWRITE( int cvAdrMSB, int cvAdrLSB, int value ) {
    Logger.info( "TODO Z21: notifyz21CVWRITE" );
  }

  @Override
  public void notifyz21CVPOMWRITEBYTE( int adr, int cvAdr, int value ) {
    Logger.info( "TODO Z21: notifyz21CVPOMWRITEBYTE" );
  }

  /*@Override
  public int notifyz21ClientHash( InetAddress client ) {
    Logger.info( "TODO Z21: notifyz21ClientHash" );
    return 0;
  }*/

  @Override
  public void notifyz21CVPOMREADBYTE( int adr, int cvAdr ) {
    Logger.info( "TODO Z21: notifyz21CVPOMREADBYTE" );
  }

  @Override
  public void notifyz21CVPOMWRITEBIT( int adr, int cvAdr, int value ) {
    Logger.info( "TODO Z21: notifyz21CVPOMWRITEBIT" );
  }

  @Override
  public void notifyz21UpdateConf() {
    Logger.info( "TODO Z21: notifyz21UpdateConf" );
  }

  @Override
  public void notifyz21CANdetector( InetAddress client, int belegtmelderTyp, int canNetworkid ) {
    if (LOG_NOTIFY) {
      Logger.info( "notifyz21CANdetector " + client.toString() + " " + belegtmelderTyp + " " + BinaryHelper.binByte( canNetworkid ) );
    }

    if (canNetworkid == 0xd000 || canNetworkid == z21CANNetId) {
      // send for all or for our own
      for (ActionGroup actionGroup : modelRailway.actionGroupIter()) {
        for (RailAction railAction : actionGroup.railActionIter()) {
          if (railAction instanceof RailBlock) {
            RailBlock railBlock = (RailBlock) railAction;

            Locomotive locomotive = railBlock.getCurrentLoco();

            ActionState blockActionState = railBlock.getActionState();
            if (blockActionState != null) {
              RailAction railMessage = modelRailway.getRailMessage( blockActionState.getSrcID() );

              if (railMessage != null) {
                doReturnSensorStateChanged( z21CANNetId, locomotive, railMessage );
              }
            }
          }
        }
      }
    }
    else {
      Logger.info( "unknown canNetworkId: " + canNetworkid );
    }
  }

  @Override
  public void notifyz21LNdetector( InetAddress client, int lnType, int addr ) {
    Logger.info( "TODO Z21: notifyz21LNdetector lnType=" + lnType + " addr=" + addr );

    // TODO: lnType 0x80 0x81 0x82
  }

  @Override
  public int notifyz21Railcom() {
    Logger.info( "TODO Z21: notifyz21Railcom" );
    return 0;
  }

  @Override
  public void notifyz21getSystemInfo( InetAddress client, int replyPort ) {
    // values currently not available
    z21Comm.sendSystemInfo( client, replyPort, 1, 15, 40  );
  }

  @Override
  public void notifyz21LocoFktExt( int word, int b, int b1 ) {
    Logger.info( "TODO Z21: notifyz21LocoFktExt word=" + word + ", b=" + b + ", b1=" + b1 );
  }

  /**
   *
   * @param addr
   * @param fktGroup F68 F67 F66 F65 F64 F63 F62 F61
   */@Override
  public void notifyz21LocoFkt61to68( int addr, int fktGroup ) {
    Logger.info( "notifyz21LocoFkt61to68 addr=" + addr + ", fktGroup=" + fktGroup );
    Locomotive locomotive = modelRailway.getLoco( addr );
    if ((locomotive != null)) {
      doNotifyLocoFkt( locomotive, fktGroup, 61, 8 );
    }
  }

  /**
   *
   * @param addr
   * @param fktGroup F60 F59 F58 F57 F56 F55 F54 F53
   */@Override
  public void notifyz21LocoFkt53to60( int addr, int fktGroup ) {
    Logger.info( "notifyz21LocoFkt53to60 addr=" + addr + ", fktGroup=" + fktGroup );
    Locomotive locomotive = modelRailway.getLoco( addr );
    if ((locomotive != null)) {
      doNotifyLocoFkt( locomotive, fktGroup, 53, 8 );
    }
  }

  /**
   *
   * @param addr
   * @param fktGroup F52 F51 F50 F49 F48 F47 F46 F45
   */@Override
  public void notifyz21LocoFkt45to52( int addr, int fktGroup ) {
    Logger.info( "notifyz21LocoFkt45to52 addr=" + addr + ", fktGroup=" + fktGroup );
    Locomotive locomotive = modelRailway.getLoco( addr );
    if ((locomotive != null)) {
      doNotifyLocoFkt( locomotive, fktGroup, 45, 8 );
    }
  }

  /**
   *
   * @param addr
   * @param fktGroup F44 F43 F42 F41 F40 F39 F38 F37
   */@Override
  public void notifyz21LocoFkt37to44( int addr, int fktGroup ) {
    Logger.info( "notifyz21LocoFkt37to44 addr=" + addr + ", fktGroup=" + fktGroup );
    Locomotive locomotive = modelRailway.getLoco( addr );
    if ((locomotive != null)) {
      doNotifyLocoFkt( locomotive, fktGroup, 37, 8 );
    }
  }

  /**
   *
   * @param addr
   * @param fktGroup F36 F35 F34 F33 F32 F31 F30 F29
   */@Override
  public void notifyz21LocoFkt29to36( int addr, int fktGroup ) {
    Logger.info( "notifyz21LocoFkt29to36 addr=" + addr + ", fktGroup=" + fktGroup );
    Locomotive locomotive = modelRailway.getLoco( addr );
    if ((locomotive != null)) {
      doNotifyLocoFkt( locomotive, fktGroup, 29, 8 );
    }
  }

  /**
   *
   * @param addr
   * @param fktGroup F28 F27 F26 F25 F24 F23 F22 F21
   */@Override
  public void notifyz21LocoFkt21to28( int addr, int fktGroup ) {
    Logger.info( "notifyz21LocoFkt21to28 addr=" + addr + ", fktGroup=" + fktGroup );
    Locomotive locomotive = modelRailway.getLoco( addr );
    if ((locomotive != null)) {
      doNotifyLocoFkt( locomotive, fktGroup, 21, 8 );
    }
  }

  /**
   *
   * @param addr
   * @param fktGroup F20 F19 F18 F17 F16 F15 F14 F13
   */ @Override
  public void notifyz21LocoFkt13to20( int addr, int fktGroup ) {
    Logger.info( "notifyz21LocoFkt13to20 addr=" + addr + ", fktGroup=" + fktGroup );
    Locomotive locomotive = modelRailway.getLoco( addr );
    if ((locomotive != null)) {
      doNotifyLocoFkt( locomotive, fktGroup, 13, 8 );
    }
  }

  /**
   *
   * @param addr
   * @param fktGroup 0 0 0 0 F12 F11 F10 F9
   */
  @Override
  public void notifyz21LocoFkt9to12( int addr, int fktGroup ) {
    Logger.info( "notifyz21LocoFkt9to12 addr=" + addr + ", fktGroup=" + fktGroup );
    Locomotive locomotive = modelRailway.getLoco( addr );
    if ((locomotive != null)) {
      doNotifyLocoFkt( locomotive, fktGroup, 9, 4 );
    }
  }

  /**
   *
   * @param addr
   * @param fktGroup 0 0 0 0 F8 F7 F6 F5
   */
  @Override
  public void notifyz21LocoFkt5to8( int addr, int fktGroup ) {
    Logger.info( "notifyz21LocoFkt5to8 addr=" + addr + ", fktGroup=" + fktGroup );
    Locomotive locomotive = modelRailway.getLoco( addr );
    if ((locomotive != null)) {
      doNotifyLocoFkt( locomotive, fktGroup, 5, 4 );
    }
  }

  /**
   *
   * @param addr
   * @param fktGroup 0 0 0 F0 F4 F3 F2 F1
   */
  @Override
  public void notifyz21LocoFkt0to4( int addr, int fktGroup ) {
    Logger.info( "notifyz21LocoFkt0to4 adr=" + addr + ", fktGroup=" + BinaryHelper.binByte( fktGroup ) );
    Locomotive locomotive = modelRailway.getLoco( addr );
    if ((locomotive != null)) {

      // convention: F0 typically is Light
      boolean isLight = BinaryHelper.bitRead( fktGroup, 4 );
      locomotive.setDCCFunctionState( 0, isLight );

      doNotifyLocoFkt( locomotive, fktGroup, 1, 4 );
    }
  }

  /**
   *
   * @param locomotive
   * @param fktGroup
   * @param fktStart
   * @param fktCount group F0-F4, F5-F8, F9-F12 12 have the first 4 bits  unused
   */
  private static void doNotifyLocoFkt( Locomotive locomotive, int fktGroup, int fktStart, int fktCount ) {
    for (int i = 0; i < fktCount; i++) {
      boolean isSet = BinaryHelper.bitRead( fktGroup, i );
      int fktNum = i + fktStart;
      locomotive.setDCCFunctionState( fktNum, isSet );
    }
  }

  @Override
  public void notifyz21ExtAccessory( int i, int b ) {
    Logger.info( "TODO Z21: notifyz21ExtAccessory" );
  }

  @Override
  public void notifyz21CVPOMACCREADBYTE( int adr, int cvAdr ) {
    Logger.info( "TODO Z21: notifyz21CVPOMACCREADBYTE" );
  }

  @Override
  public void notifyz21CVPOMACCWRITEBIT( int adr, int cvAdr, int value ) {
    Logger.info( "TODO Z21: notifyz21CVPOMACCWRITEBIT" );
  }

  @Override
  public void notifyz21CVPOMACCWRITEBYTE( int adr, int cvAdr, int value ) {
    Logger.info( "TODO Z21: notifyz21CVPOMACCWRITEBYTE" );
  }

  public static void main( String[] args ) {
    Z21Processor z21 = null;
    try {
      z21 = new Z21Processor( new ModelRailway() );
      z21.getZ21Comm().start();
      while (true) {
        Thread.sleep( 1000 );
      }
    }
    catch (Exception ex) {
      Logger.error( "Error in Z21", ex );
    }
    finally {
      if (z21 != null) {
        z21.getZ21Comm().stop();
      }
    }
  }

  @Override
  public void stateChanged( RailAction changedAction, ActionState newState ) {
    RailDevice railDevice = changedAction.getRailDevice();

    // loco motor
    if (newState instanceof MotorState) {
      if (railDevice != null && railDevice instanceof Locomotive) {
        Locomotive locomotive = (Locomotive) railDevice;
        if (LOG_NOTIFY) {
          Logger.info( "loco: " + locomotive.getDisplayName() + " DCCAddr:" + locomotive.getDCCAddr() + " newState:" + newState );
        }
        int dccAddr = locomotive.getDCCAddr();
        z21Comm.returnLocoStateFull( dccAddr );
        return;
      }
    }

    // loco function or accessory
    if (changedAction instanceof RailFunction) {
      RailFunction railFunction = (RailFunction) changedAction;
      int dccAddr = railFunction.getDCCAddr();
      if (dccAddr > 0) {
        // function represents an accessory, e.g. Weiche
        if (LOG_NOTIFY) {
          Logger.info( "accessory: " + railFunction.getType() + " DCCAddr:" + railFunction.getDCCAddr() + " target:" + railFunction.getTarget() + " state:" + railFunction.getState() + " value:" + railFunction.getValue() + " newState:" + newState );
        }
        // TODO??? dccAddr += adjustAccessoryAddr;
        z21Comm.returnAccessoryStateChanged( dccAddr );
        return;
      }
      else {
        if (railDevice != null && railDevice instanceof Locomotive) {
          Locomotive locomotive = (Locomotive) railDevice;
          // function within a loco, e.g. Licht
          dccAddr = locomotive.getDCCAddr();
          if (LOG_NOTIFY) {
            Logger.info( "function: " + railFunction.getType() + " DCCAddr:" + locomotive.getDCCAddr() + " target:" + railFunction.getTarget() + " state:" + railFunction.getState() + " value:" + railFunction.getValue() + " newState:" + newState );
          }
          z21Comm.returnLocoStateFull( dccAddr );
          return;
        }
      }
    }

    // Sensor notification
    /*if (changedAction instanceof RailSensor) {
      RailSensor railSensor = (RailSensor) changedAction;
      QName srcID = newState.getSrcID(); // TODO: > 2 Zeichen
      if (srcID != null) {
        RailAction railMessage = modelRailway.getRailMessage( srcID );

        // no loco indicates a reset of the prior Balise
        Locomotive locomotive = null;
        if (railDevice != null && railDevice instanceof Locomotive) {
          locomotive = (Locomotive) railDevice;
        }

        if (LOG_NOTIFY) {
          if (locomotive == null) {
            // reset last
            Logger.info( "release last from sensor: Id:" + railSensor.getId() + " message DCCAddr:" + railMessage.getDCCAddr() + " newState:" + newState );
          }
          else {
            Logger.info( "sensor: Id:" + railSensor.getId() + " message DCCAddr:" + railMessage.getDCCAddr() + " loco DCCAddr:" + locomotive.getDCCAddr() + " newState:" + newState );
          }
        }
        doReturnSensorStateChanged( z21CANNetId, locomotive, railMessage );
      }
      return;
    }   */

    //--- NFC-Balise
    if (changedAction instanceof RailMessage) {
      RailMessage railMessage = (RailMessage) changedAction;
      QName msgID = railMessage.getPosID();
      if (msgID != null) {
        Locomotive locomotive = modelRailway.getLoco( railMessage.getLastReceivedBy() );
        if (LOG_NOTIFY) {
          if (locomotive == null) {
            // reset last
            Logger.info( "release last from message: Id:" + railMessage.getId() + " message DCCAddr:" + railMessage.getDCCAddr() + " newState:" + newState );
          }
          else {
            Logger.info( "sensor: Id:" + msgID + " message DCCAddr:" + railMessage.getDCCAddr() + " loco DCCAddr:" + locomotive.getDCCAddr() + " newState:" + newState );
          }
        }
        doReturnSensorStateChanged( z21CANNetId, locomotive, railMessage );
      }
      return;
    }

    //--- IR-Balise
    if (changedAction instanceof RailTimer) {
      RailTimer railMessage = (RailTimer) changedAction;
      QName msgID = railMessage.getId();
      Locomotive locomotive = modelRailway.getLoco( railMessage.getReceivedByID() );
      if (LOG_NOTIFY) {
        if (locomotive == null) {
          // reset last
          Logger.info( "release last from message: Id:" + railMessage.getId() + " message DCCAddr:" + railMessage.getDCCAddr() + " newState:" + newState );
        }
        else {
          Logger.info( "sensor: Id:" + msgID + " message DCCAddr:" + railMessage.getDCCAddr() + " loco DCCAddr:" + locomotive.getDCCAddr() + " newState:" + newState );
        }
      }
      doReturnSensorStateChanged( z21CANNetId, locomotive, railMessage );
      return;
    }

    // no recognised combination
    if (LOG_NOTIFY) {
      Logger.info( "other: " + changedAction + " id:" + changedAction.getId() + " railDevice: " + changedAction.getRailDevice() + " newState: " + newState );
    }
  }

  private void doReturnSensorStateChanged( int canNetId, Locomotive locomotive, RailAction railMessage ) {
    //--- iTrain calculates sensorAddr = messageAddr * 8 + messagePort + 1
    //    see Mail by Xander from 2023-09-15
    int sensorAddr = railMessage.getDCCAddr() - 1;
    int sensorModuleAddr = sensorAddr / 8;
    int sensorPort = sensorAddr % 8;

    int locoDCCAddr;
    char locoDirChar;
    if (locomotive != null) {
      locoDCCAddr = locomotive.getDCCAddr();
      locoDirChar = locomotive.getClockDir();
    }
    else {
      locoDCCAddr = 0;
      locoDirChar = MotorState.CHAR_UNKNOWN;
    }

    Z21LocoDir z21ClockDir;
    if (locoDirChar == MotorState.CHAR_FORWARD) {
      z21ClockDir = Z21LocoDir.forward;
    }
    else if (locoDirChar == MotorState.CHAR_BACK) {
      z21ClockDir = Z21LocoDir.backward;
    }
    else {
      z21ClockDir = Z21LocoDir.unknown;
    }

    z21Comm.returnSensorStateChanged( canNetId, sensorModuleAddr, sensorPort, locoDCCAddr, z21ClockDir );
  }

}