package de.pidata.rail.z21;

import java.net.InetAddress;

public class TypeActIP {
  public InetAddress client; //Byte client
  public int clientPort;     //replyPort
  public int bcFlag;         //BoadCastFlag - see Z21type.h
  public byte time;          //Zeit
  public int adr;	     //Loco control Adr
}
