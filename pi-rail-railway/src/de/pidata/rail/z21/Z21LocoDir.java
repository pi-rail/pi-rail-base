package de.pidata.rail.z21;

public enum Z21LocoDir {
  forward,
  backward,
  unknown
}
