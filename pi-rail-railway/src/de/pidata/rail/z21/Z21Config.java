package de.pidata.rail.z21;

import de.pidata.qnames.QName;
import de.pidata.rail.model.PiRailFactory;

/**
 * - current configured Z21 characteristics
 * - EEPROM
 */
public class Z21Config {

  public static final QName Z21_EXTCFG_ID = PiRailFactory.NAMESPACE.getQName( "Z21" );

  public int read( int valueID ) {
    return 0;
  }

  public void FSTORAGEMODE( int i, long bcFlag ) {

  }
}
