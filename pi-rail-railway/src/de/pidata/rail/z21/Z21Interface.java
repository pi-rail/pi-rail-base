package de.pidata.rail.z21;

import java.net.InetAddress;

public interface Z21Interface {
  void notifyz21RailPower( int state );

  void notifyz21EthSend( InetAddress client, int sendPort, int[] data );

  void notifyz21S88Data( int data );

  void setZ21LocoFkt( int adr, int type, int fkt );

  void setZ21LocoSpeed( int adr, int speed, int steps );

  void notifyz21Accessory( int adr, boolean state, boolean active );

  boolean notifyz21AccessoryInfo( int Adr );

  int notifyz21LNdispatch( int Adr2, int Adr );

  void notifyz21LNSendPacket( int[] data, int length );

  void notifyz21CVREAD( int cvAdrMSB, int cvAdrLSB );

  void notifyz21CVWRITE( int cvAdrMSB, int cvAdrLSB, int value );

  void notifyz21CVPOMWRITEBYTE( int adr, int cvAdr, int value );

  //int notifyz21ClientHash( InetAddress client );

  void notifyz21CVPOMREADBYTE( int adr, int cvAdr );

  void notifyz21CVPOMWRITEBIT( int adr, int cvAdr, int value );

  void notifyz21UpdateConf();

  void notifyz21CANdetector( InetAddress client, int b, int word );

  void notifyz21LNdetector( InetAddress client, int b, int word );

  int notifyz21Railcom();

  void notifyz21getSystemInfo( InetAddress client, int replyPort );

  void notifyz21LocoFktExt( int word, int b, int b1 );

  void notifyz21LocoFkt61to68( int word, int b );

  void notifyz21LocoFkt53to60( int word, int b );

  void notifyz21LocoFkt45to52( int word, int b );

  void notifyz21LocoFkt37to44( int word, int b );

  void notifyz21LocoFkt29to36( int word, int b );

  void notifyz21LocoFkt21to28( int word, int b );

  void notifyz21LocoFkt13to20( int word, int b );

  void notifyz21LocoFkt9to12( int word, int i );

  void notifyz21LocoFkt5to8( int word, int i );

  void notifyz21LocoFkt0to4( int word, int i );

  void notifyz21ExtAccessory( int i, int b );

  void notifyz21CVPOMACCREADBYTE( int adr, int cvAdr );

  void notifyz21CVPOMACCWRITEBIT( int adr, int cvAdr, int value );

  void notifyz21CVPOMACCWRITEBYTE( int adr, int cvAdr, int value );

  int[] requestZ21LocoState( int adr );

  int[] getSerialNumber();

  int[] getHWType();

  int[] getFWVersion();
}
