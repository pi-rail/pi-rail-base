package de.pidata.rail.z21;

import de.pidata.binary.BinaryHelper;
import de.pidata.connect.udp.InetReceiveListener;
import de.pidata.connect.udp.UdpConnection;
import de.pidata.log.Logger;
import de.pidata.rail.comm.PiRailComm;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.InterfaceAddress;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*
 Converted from C++
 z21.cpp - library for Roco Z21 LAN protocoll
 Copyright (c) 2013-2022 Philipp Gahtow  All right reserved.
 */

/**
 * - Z21 LAN protocol
 * - communicates with Z21 Client. e.g iTrain, Roco WLANMaus, ...
 */
public class Z21Comm implements Runnable, InetReceiveListener {

  public static final boolean LOG_RCV = false;
  public static final boolean LOG_SND = false;
  public static final boolean LOG_SND_REPEAT = false;
  public static final boolean LOG_OTHER = false;
  private Z21Interface z21Interface;

  byte[] last_rcv_packet = new byte[1]; // init with some dummy value to spare null checks
  int[] last_snd_packet = new int[1]; // init with some dummy value to spare null checks

  public static final int Z21_PORT = 21105;
  
  //**************************************************************
//Z21 LAN Protokoll Spezifikation:
  public static final byte LAN_X_Header = 0x40;  //not in Spezifikation!
  public static final byte LAN_GET_SERIAL_NUMBER = 0x10;
  public static final byte LAN_GET_CODE = 0x18;        //SW Feature-Umfang der Z21
  public static final byte LAN_LOGOFF = 0x30;
  public static final byte LAN_X_GET_SETTING = 0x21;
  public static final byte LAN_X_BC_TRACK_POWER = 0x61;
  public static final byte LAN_X_UNKNOWN_COMMAND = 0x61;
  public static final byte LAN_X_STATUS_CHANGED = 0x62;
  public static final byte LAN_X_GET_VERSION = 0x63;        //AW: X-Bus Version 090040006321301260
  public static final int LAN_X_SET_STOP = 0x80;  //AW: LAN_X_BC_STOPPED
  public static final int LAN_X_BC_STOPPED = 0x81;
  public static final int LAN_X_GET_FIRMWARE_VERSION = 0xF1;  //AW: 0xF3
  public static final byte LAN_SET_BROADCASTFLAGS = 0x50;
  public static final byte LAN_GET_BROADCASTFLAGS = 0x51;
  public static final int LAN_SYSTEMSTATE_DATACHANGED = 0x84;
  public static final int LAN_SYSTEMSTATE_GETDATA = 0x85;  //AW: LAN_SYSTEMSTATE_DATACHANGED
  public static final byte LAN_GET_HWINFO = 0x1A;
  public static final byte LAN_GET_LOCOMODE = 0x60;
  public static final byte LAN_SET_LOCOMODE = 0x61;
  public static final byte LAN_GET_TURNOUTMODE = 0x70;
  public static final byte LAN_SET_TURNOUTMODE = 0x71;
  public static final int LAN_X_GET_LOCO_INFO = 0xE3;
  public static final int LAN_X_SET_LOCO = 0xE4;  //X-Header
  public static final int LAN_X_SET_LOCO_FUNCTION = 0xF8;  //DB0
  public static final int LAN_X_SET_LOCO_BINARY_STATE = 0xE5;  //X-Header
  public static final int LAN_X_LOCO_INFO = 0xEF;
  public static final byte LAN_X_GET_TURNOUT_INFO = 0x43;
  public static final byte LAN_X_SET_TURNOUT = 0x53;
  public static final byte LAN_X_TURNOUT_INFO = 0x43;
  public static final byte LAN_X_SET_EXT_ACCESSORY = 0x54;        //new: 1.10
  public static final byte LAN_X_GET_EXT_ACCESSORY_INFO = 0x44;        //new: 1.10
  public static final byte LAN_X_CV_READ = 0x23;
  public static final byte LAN_X_CV_WRITE = 0x24;
  public static final byte LAN_X_CV_NACK_SC = 0x61;
  public static final byte LAN_X_CV_NACK = 0x61;
  public static final byte LAN_X_CV_RESULT = 0x64;
  public static final int LAN_RMBUS_DATACHANGED = 0x80;
  public static final int LAN_RMBUS_GETDATA = 0x81;
  public static final int LAN_RMBUS_PROGRAMMODULE = 0x82;

  public static final int LAN_RAILCOM_DATACHANGED = 0x88;
  public static final int LAN_RAILCOM_GETDATA = 0x89;

  public static final int LAN_LOCONET_Z21_RX = 0xA0;
  public static final int LAN_LOCONET_Z21_TX = 0xA1;
  public static final int LAN_LOCONET_FROM_LAN = 0xA2;
  public static final int LAN_LOCONET_DISPATCH_ADDR = 0xA3;
  public static final int LAN_LOCONET_DETECTOR = 0xA4;

  public static final int LAN_CAN_DETECTOR = 0xC4;

  public static final int LAN_X_CV_POM = 0xE6;  //X-Header
  public static final int LAN_X_CV_POM_WRITE_BYTE = 0xEC;  //DB3 Option
  public static final int LAN_X_CV_POM_WRITE_BIT = 0xE8;  //DB3 Option
  public static final int LAN_X_CV_POM_READ_BYTE = 0xE4;  //DB3 Option
  public static final int LAN_X_CV_POM_ACCESSORY_WRITE_BYTE = 0xEC;        //DB3 Option
  public static final int LAN_X_CV_POM_ACCESSORY_WRITE_BIT = 0xE8;        //DB3 Option
  public static final int LAN_X_CV_POM_ACCESSORY_READ_BYTE = 0xE4;        //DB3 Option

  //ab Z21 FW Version 1.23
  public static final byte LAN_X_MM_WRITE_BYTE = 0x24;

  //ab Z21 FW Version 1.25
  public static final byte LAN_X_DCC_READ_REGISTER = 0x22;
  public static final byte LAN_X_DCC_WRITE_REGISTER = 0x23;

  //**************************************************************
//Z21 BC Flags
  public static final int Z21bcNone = 0b00000000;
  public static final int Z21bcAll = 0x00000001;
  public static final int Z21bcAll_s = 0b00000001;
  public static final int Z21bcRBus = 0x00000002;
  public static final int Z21bcRBus_s = 0b00000010;
  public static final int Z21bcRailcom = 0x00000004;    //RailCom-Daten für Abo Loks
  public static final int Z21bcRailcom_s = 0x100;

  public static final int Z21bcSystemInfo = 0x00000100;        //LAN_SYSTEMSTATE_DATACHANGED
  public static final int Z21bcSystemInfo_s = 0b00000100;

  //ab FW Version 1.20:
  public static final int Z21bcNetAll = 0x00010000; // Alles, auch alle Loks ohne vorher die Lokadresse abonnieren zu müssen (für PC Steuerung)
  public static final int Z21bcNetAll_s = 0b00001000;

  public static final int Z21bcLocoNet = 0x01000000; // LocoNet Meldungen an LAN Client weiterleiten (ohne Loks und Weichen)
  public static final int Z21bcLocoNet_s = 0b00010000;
  public static final int Z21bcLocoNetLocos = 0x02000000; // Lok-spezifische LocoNet Meldungen an LAN Client weiterleiten
  public static final int Z21bcLocoNetLocos_s = 0b00110000;
  public static final int Z21bcLocoNetSwitches = 0x04000000; // Weichen-spezifische LocoNet Meldungen an LAN Client weiterleiten
  public static final int Z21bcLocoNetSwitches_s = 0b01010000;

  //ab FW Version 1.22:
  public static final int Z21bcLocoNetGBM = 0x08000000;  //Status-Meldungen von Gleisbesetztmeldern am LocoNet-Bus
  public static final int Z21bcLocoNetGBM_s = 0b10010000;

  //ab FW Version 1.29:
  public static final int Z21bcRailComAll = 0x00040000; //alles: Änderungen bei RailCom-Daten ohne Lok Abo! -> LAN_RAILCOM_DATACHANGED
  public static final int Z21bcRailComAll_s = 0b10000000;

  //ab FW Version 1.30:
  public static final int Z21bcCANDetector = 0x00080000;        //Meldungen vom Gleisbesetztmeldern am CAN-Bus
  public static final int Z21bcCANDetector_s = 0b11000000;

  //**************************************************************
  //Firmware-Version der Z21: moved to Z21Processor class
  // get from z21Interface: public static final int z21FWVersionMSB = 0x01;
  // get from z21Interface: public static final int z21FWVersionLSB = 0x42;
  /*
  HwType:
  public static final int D_HWT_Z21_OLD = 0x00000200L; // schwarze Z21 (Hardware-Variante ab 2012)
  public static final int D_HWT_Z21_NEW = 0x00000201L; // schwarze Z21(Hardware-Variante ab 2013)
  public static final int D_HWT_SMARTRAIL = 0x00000202L; // SmartRail (ab 2012)
  public static final int D_HWT_z21_SMALL = 0x00000203L; // weiße z21 Starterset-Variante (ab 2013)
  public static final int D_HWT_z21_START = 0x00000204L; // z21 start Starterset-Variante (ab 2016)
  public static final int D_HWT_Z21_XL = 0x00000211L; // 10870 Z21 XL Series (ab 2020)
  public static final int D_HWT_SINGLE_BOOSTER = 0x00000205L; // 10806 Z21 Single Booster (zLink)
  public static final int D_HWT_DUAL_BOOSTER = 0x00000206L; // 10807 Z21 Dual Booster (zLink)
  public static final int D_HWT_Z21_SWITCH_DECODER = 0x00000301L; // 10836 Z21 SwitchDecoder (zLink)
  public static final int D_HWT_Z21_SIGNAL_DECODER = 0x00000302L; // 10836 Z21 SignalDecoder (zLink)
  */
  //Hardware-Typ: 0x00000211 // 10870 Z21 XL Series (ab 2020)
  // get from z21Interface: public static final int z21HWTypeMSB = 0x02;
  // get from z21Interface: public static final int z21HWTypeLSB = 0x11;
  //Seriennummer inside EEPROM:
  //public static final int CONFz21SnMSB = 0;                //0x01
  //public static final int CONFz21SnLSB = 1;                //0xE8
  //**************************************************************
//Store Z21 configuration inside EEPROM:
  public static final int CONF1STORE = 50;        //(10x Byte)	- Prog, RailCom, etc.
  public static final int CONF2STORE = 60;        //(15x Byte)	- Voltage: Prog, Rail, etc.
  public static final int CLIENTHASHSTORE = 0x200;                //512 Start where Client-Hash is stored

  //--------------------------------------------------------------
//certain global XPressnet status indicators:
  public static final int csNormal = 0x00;                        // Normal Operation Resumed ist eingeschaltet
  public static final int csEmergencyStop = 0x01;        // Der Nothalt ist eingeschaltet
  public static final int csTrackVoltageOff = 0x02;  // Die Gleisspannung ist abgeschaltet
  public static final int csShortCircuit = 0x04;        // Kurzschluss
  public static final int csServiceMode = 0x08;                // Der Programmiermodus ist aktiv - Service Mode
  //Bitmask CentralStateEx:
  public static final int cseHighTemperature = 0x01; // zu hohe Temperatur
  public static final int csePowerLost = 0x02; // zu geringe Eingangsspannung
  public static final int cseShortCircuitExternal = 0x04; // am externen Booster-Ausgang
  public static final int cseShortCircuitInternal = 0x08; // am Hauptgleis oder Programmiergleis

  //--------------------------------------------------------------
  public static final int z21ActTimeIP = 20;    //Aktivhaltung einer IP für (sec./2)
  public static final int z21IPinterval = 2000;   //interval at milliseconds

  //DCC Speed Steps
  public static final int DCCSTEP14 = 0x01;
  public static final int DCCSTEP28 = 0x02;
  public static final int DCCSTEP128 = 0x03;

  //Variables:
  private int railpower;                                //state of the railpower
  private long z21IPpreviousMillis;        // will store last time of IP decount updated  
  private List<TypeActIP> actIPList = new ArrayList<>();    //Speicherarray für IPs

  private int lastExtaccMsg = 0x00;                //for LAN_X_GET_EXT_ACCESSORY_INFO
  private boolean lastExtaccReceived = false;        //already had any EXTACC Message?

  private Z21Config z21Config = new Z21Config();
  private UdpConnection z21Connection;
  private boolean active = false;
  private Thread z21Thread;
  private boolean notifyz21CVREAD = true;
  private boolean notifyz21RailPower = true;
  private boolean notifyz21CVWRITE = true;
  private boolean notifyz21CVPOMWRITEBYTE = true;
  private boolean notifyz21CVPOMWRITEBIT = true;
  private boolean notifyz21CVPOMREADBYTE = true;
  private boolean notifyz21CVPOMACCWRITEBYTE = true;
  private boolean notifyz21CVPOMACCWRITEBIT = true;
  private boolean notifyz21CVPOMACCREADBYTE = true;
  private boolean notifyz21Accessory = true;
  private boolean notifyz21AccessoryInfo = true;
  private boolean notifyz21ExtAccessory = true;
  private boolean notifyz21LocoSpeed = true;
  private boolean notifyz21LocoFkt = true;
  private boolean notifyz21LocoFkt0to4 = true;
  private boolean notifyz21LocoFkt5to8 = true;
  private boolean notifyz21LocoFkt9to12 = true;
  private boolean notifyz21LocoFkt13to20 = true;
  private boolean notifyz21LocoFkt21to28 = true;
  private boolean notifyz21LocoFkt29to36 = true;
  private boolean notifyz21LocoFkt37to44 = true;
  private boolean notifyz21LocoFkt45to52 = true;
  private boolean notifyz21LocoFkt53to60 = true;
  private boolean notifyz21LocoFkt61to68 = true;
  private boolean notifyz21LocoFktExt = true;
  private boolean notifyz21S88Data = true;
  private boolean notifyz21getSystemInfo = true;
  private boolean notifyz21Railcom = true;
  private boolean notifyz21LNSendPacket = true;
  private boolean notifyz21LNdispatch = true;
  private boolean notifyz21LNdetector = true;
  private boolean notifyz21CANdetector = true;
  private boolean notifyz21UpdateConf = true;
  private boolean notifyz21LocoState = true;
  private boolean notifyz21EthSend = true;
  private boolean notifyz21ClientHash = true;


// Constructor /////////////////////////////////////////////////////////////////
// Function that handles the creation and setup of instances

  public Z21Comm( Z21Interface z21Interface ) {
    this.z21Interface = z21Interface;
    // initialize this instance's variables
    z21IPpreviousMillis = 0;
    railpower = csTrackVoltageOff;
    clearIPSlots();
  }

  /**
   * @deprecated use {@link BinaryHelper#hexByte(byte)} directly
   * @param value
   * @return
   */
  @Deprecated
  private String hexByte( byte value ) {
    return BinaryHelper.hexByte( value );
  }

  /**
   * @deprecated use {@link BinaryHelper#hexByte(byte)} directly
   * @param value
   * @return
   */
  @Deprecated
  private String hexByte( int value ) {
    return BinaryHelper.hexByte( value );
  }

  /**
   * @deprecated use {@link BinaryHelper#word(int, int)} directly
   * @param high
   * @param low
   * @return
   */
  @Deprecated
  private int word( int high, int low ) {
    return BinaryHelper.word( high, low );
  }

  /**
   * @deprecated use {@link BinaryHelper#word(int, int)} directly
   * @param high
   * @param low
   * @return
   */
  @Deprecated
  private int word( int high, byte low ) {
    return BinaryHelper.word( high, low );
  }

  /**
   * @deprecated use {@link BinaryHelper#lowByte(int)} directly
   * @param value
   * @return
   */
  @Deprecated
  private int lowByte( int value ) {
    return BinaryHelper.lowByte( value );
  }

  /**
   * @deprecated use {@link BinaryHelper#highByte(int)} directly
   * @param value
   * @return
   */
  @Deprecated
  private int highByte( int value ) {
    return BinaryHelper.highByte( value );
  }

  // Public Methods //////////////////////////////////////////////////////////////
  // Functions available in Wiring sketches, this library, and other libraries

  //*********************************************************************************************
  //Daten ermitteln und Auswerten
  public void receivedData( InetAddress client, byte[] packet, InetSocketAddress myAddress, int replyPort ) throws IOException {
    addIPToSlot( client, replyPort, 0 );
    // send a reply, to the IP address and port that sent us the packet we received
    int header = (Byte.toUnsignedInt( packet[3] ) << 8) + Byte.toUnsignedInt( packet[2] );
    int[] data = new int[16];                        //z21 send storage

    boolean rcv_packet_is_different = checkIfPacketIsDifferent( last_rcv_packet, packet );

    if (rcv_packet_is_different) {
      last_rcv_packet = packet; // remember packet for next time
    }

    if (LOG_RCV && rcv_packet_is_different) { // log packet only when it differs from last one
      StringBuilder builder = new StringBuilder( "Received from " );
      builder.append( client );
      builder.append( ": " );
      for (int x = 0; x < packet[0]; x++) {
        builder.append( hexByte( packet[x] ) );
        builder.append( " " );
      }
      Logger.info( builder.toString() );
    }

    switch (header) {
      case LAN_GET_SERIAL_NUMBER: {
        if (LOG_RCV && rcv_packet_is_different) {
          Logger.info( "GET_SERIAL_NUMBER" );
        }
        int[] z21SerialNumber = z21Interface.getSerialNumber();
        data[0] = z21SerialNumber[0];
        data[1] = z21SerialNumber[1];
        data[2] = z21SerialNumber[2];
        data[3] = z21SerialNumber[3];
        ethSend( client, replyPort, 0x08, LAN_GET_SERIAL_NUMBER, data, false, Z21bcNone ); //Seriennummer 32 Bit (little endian)
        break;
      }
      case LAN_GET_HWINFO: {
        if (LOG_RCV && rcv_packet_is_different) {
          Logger.info( "GET_HWINFO" );
        }
        int[] z21HWType = z21Interface.getHWType();
        int[] z21FWVersion = z21Interface.getFWVersion();
        data[0] = z21HWType[0]; // z21HWTypeLSB;  //HwType 32 Bit
        data[1] = z21HWType[1]; // z21HWTypeMSB;
        data[2] = z21HWType[2];
        data[3] = z21HWType[3];

        data[4] = z21FWVersion[0]; // z21FWVersionLSB = 0x42; //FW Version 32 Bit
        data[5] = z21FWVersion[1]; // z21FWVersionMSB = 0x01;
        data[6] = z21FWVersion[2];
        data[7] = z21FWVersion[3];
        ethSend( client, replyPort, 0x0C, LAN_GET_HWINFO, data, false, Z21bcNone );
        break;
      }
      case LAN_LOGOFF: {
        if (LOG_RCV && rcv_packet_is_different) {
          Logger.info( "LOGOFF" );
        }
        clearIPSlot( client );
        //Antwort von Z21: keine
        break;
      }
      case LAN_GET_CODE: { //SW Feature-Umfang der Z21   
		  /*public static final int Z21_NO_LOCK        0x00  // keine Features gesperrt 
			public static final int z21_START_LOCKED   0x01  // z21 start: Fahren und Schalten per LAN gesperrt 
			public static final int z21_START_UNLOCKED 0x02  // z21 start: alle Feature-Sperren aufgehoben */
        data[0] = 0x00; //keine Features gesperrt
        ethSend( client, replyPort, 0x05, LAN_GET_CODE, data, false, Z21bcNone );
        break;
      }
      case (LAN_X_Header): {
        //---------------------- LAN X-Header BEGIN ---------------------------	
        switch (Byte.toUnsignedInt( packet[4] )) { //X-Header
          case LAN_X_GET_SETTING: {
            //---------------------- Switch BD0 BEGIN ---------------------------	
            switch (Byte.toUnsignedInt( packet[5] )) {  //DB0
              case 0x21: {
                if (LOG_RCV && rcv_packet_is_different) {
                  Logger.info( "X_GET_VERSION" );
                }
                data[0] = LAN_X_GET_VERSION;        //X-Header: 0x63
                data[1] = 0x21;        //DB0
                data[2] = 0x30;   //X-Bus Version
                data[3] = 0x12;  //ID der Zentrale
                ethSend( client, replyPort, 0x09, LAN_X_Header, data, true, Z21bcNone );
                break;
              }
              case 0x24: {
                data[0] = LAN_X_STATUS_CHANGED;        //X-Header: 0x62
                data[1] = 0x22;                        //DB0
                data[2] = railpower;                //DB1: Status
                //Logger.info("X_GET_STATUS "); 
                //csEmergencyStop  0x01 // Der Nothalt ist eingeschaltet 
                //csTrackVoltageOff  0x02 // Die Gleisspannung ist abgeschaltet 
                //csShortCircuit  0x04 // Kurzschluss 
                //csProgrammingModeActive 0x20 // Der Programmiermodus ist aktiv 
                ethSend( client, replyPort, 0x08, LAN_X_Header, data, true, Z21bcNone );
                break;
              }
              case 0x80: {
                if (LOG_RCV && rcv_packet_is_different) {
                  Logger.info( "X_SET_TRACK_POWER_OFF" );

                  data[0] = LAN_X_BC_TRACK_POWER;
                  data[1] = 0x00;
                  ethSend( client, replyPort, 0x07, LAN_X_Header, data, true, Z21bcNone );

                }
                if (notifyz21RailPower) {
                  z21Interface.notifyz21RailPower( csTrackVoltageOff );
                }
                break;
              }
              case 0x81: {
                if (LOG_RCV && rcv_packet_is_different) {
                  Logger.info( "X_SET_TRACK_POWER_ON" );
                }

                data[0] = LAN_X_BC_TRACK_POWER;
                data[1] = 0x01;
                ethSend( client, replyPort, 0x07, LAN_X_Header, data, true, Z21bcNone );

                if (notifyz21RailPower)
                  z21Interface.notifyz21RailPower( csNormal );

                break;
              }
            }
            //---------------------- Switch DB0 ENDE ---------------------------	
            break;  //ENDE DB0
          }
          case LAN_X_DCC_READ_REGISTER: {
            if (packet[5] == 0x15) {  //DB0	- SPECIAL: WLANMaus CV Read!
              if (notifyz21CVREAD)
                z21Interface.notifyz21CVREAD( 0, Byte.toUnsignedInt( packet[6] ) - 1 ); //CV_MSB, CV_LSB
            }
            break;
          }
          case LAN_X_CV_READ: {
            if (packet[5] == 0x11) {  //DB0
              if (LOG_RCV && rcv_packet_is_different) {
                Logger.info( "X_CV_READ" );
              }
              if (notifyz21CVREAD)
                z21Interface.notifyz21CVREAD( Byte.toUnsignedInt( packet[6] ), Byte.toUnsignedInt( packet[7] ) ); //CV_MSB, CV_LSB
            }
            if (packet[5] == 0x16) {  //DB0	- SPECIAL: WLANMaus CV Write!
              if (notifyz21CVWRITE)
                z21Interface.notifyz21CVWRITE( 0, Byte.toUnsignedInt(  packet[6] ) - 1, Byte.toUnsignedInt( packet[7] ) ); //CV_MSB, CV_LSB, value
            }
            break;
          }
          case LAN_X_CV_WRITE: {
            if (packet[5] == 0x12) {  //DB0
              if (LOG_RCV && rcv_packet_is_different) {
                Logger.info( "X_CV_WRITE" );
              }
              if (notifyz21CVWRITE)
                z21Interface.notifyz21CVWRITE( Byte.toUnsignedInt( packet[6] ), Byte.toUnsignedInt( packet[7] ), Byte.toUnsignedInt( packet[8] ) ); //CV_MSB, CV_LSB, value
            }
            break;
          }
          case LAN_X_CV_POM: {        //X-Header = 0xE6
            int CVAdr = (((Byte.toUnsignedInt( packet[8] ) & 0xB11) << 8) + Byte.toUnsignedInt( packet[9] ));
            int value = packet[10];
            if (packet[5] == 0x30) {  //DB0 = LAN_X_CV_POM
              int Adr = getAddrLoco( packet[6], packet[7] );
              if ((packet[8] & 0xFC) == LAN_X_CV_POM_WRITE_BYTE) {                //DB3 Option 0xEC
                if (LOG_RCV && rcv_packet_is_different) {
                  Logger.info( "LAN_X_CV_POM_WRITE_BYTE" );
                }
                if (notifyz21CVPOMWRITEBYTE)
                  z21Interface.notifyz21CVPOMWRITEBYTE( Adr, CVAdr, value );  //set Byte
              }
              else if ((packet[8] & 0xFC) == LAN_X_CV_POM_WRITE_BIT) {        //DB3 Option 0xE8
                if (LOG_RCV && rcv_packet_is_different) {
                  Logger.info( "LAN_X_CV_POM_WRITE_BIT" );
                }
                if (notifyz21CVPOMWRITEBIT)
                  z21Interface.notifyz21CVPOMWRITEBIT( Adr, CVAdr, value );  //set Bit
              }
              else if ((packet[8] & 0xFC) == LAN_X_CV_POM_READ_BYTE) {        //DB3 Option 0xE4
                if (LOG_RCV && rcv_packet_is_different) {
                  Logger.info( "LAN_X_CV_POM_READ_BYTE" );
                }
                if (notifyz21CVPOMREADBYTE)
                  z21Interface.notifyz21CVPOMREADBYTE( Adr, CVAdr );  //read byte
              }
            }
            else if (packet[5] == 0x31) {  //DB0 = LAN_X_CV_POM_ACCESSORY
              int Adr = ((Byte.toUnsignedInt( packet[6] ) & 0x1F) << 8) + Byte.toUnsignedInt( packet[7] );
              if ((packet[8] & 0xFC) == LAN_X_CV_POM_ACCESSORY_WRITE_BYTE) {                //DB3 Option 0xEC
                if (LOG_RCV && rcv_packet_is_different) {
                  Logger.info( "LAN_X_CV_POM_ACCESSORY_WRITE_BYTE" );
                }
                if (notifyz21CVPOMACCWRITEBYTE)
                  z21Interface.notifyz21CVPOMACCWRITEBYTE( Adr, CVAdr, value );  //set Byte
              }
              else if ((packet[8] & 0xFC) == LAN_X_CV_POM_ACCESSORY_WRITE_BIT) {        //DB3 Option 0xE8
                if (LOG_RCV && rcv_packet_is_different) {
                  Logger.info( "LAN_X_CV_POM_ACCESSORY_WRITE_BIT" );
                }
                if (notifyz21CVPOMACCWRITEBIT)
                  z21Interface.notifyz21CVPOMACCWRITEBIT( Adr, CVAdr, value );  //set Bit
              }
              else if ((packet[8] & 0xFC) == LAN_X_CV_POM_ACCESSORY_READ_BYTE) {        //DB3 Option 0xE4
                if (LOG_RCV && rcv_packet_is_different) {
                  Logger.info( "LAN_X_CV_POM_ACCESSORY_READ_BYTE" );
                }
                if (notifyz21CVPOMACCREADBYTE)
                  z21Interface.notifyz21CVPOMACCREADBYTE( Adr, CVAdr );  //read byte
              }
            }
            break;
          }
          case LAN_X_SET_TURNOUT: {  //and notify other Clients with LAN_X_GET_TURNOUT_INFO!
            if (LOG_RCV && rcv_packet_is_different) {
              StringBuilder builder = new StringBuilder( "X_SET_TURNOUT Adr.:" );
              builder.append( getAddrAccessory( packet[5], packet[6] ) );
              builder.append( ":" );
              builder.append( BinaryHelper.bitRead( packet[7], 0 ) );
              builder.append( "-" );
              builder.append( BinaryHelper.bitRead( packet[7], 3 ) );
              Logger.info( builder.toString() );
            }
            //boolean TurnOnOff = bitRead(packet[7],3);  //Spule EIN/AUS
            if (notifyz21Accessory) {
              z21Interface.notifyz21Accessory( getAddrAccessory( packet[5], packet[6] ), BinaryHelper.bitRead( packet[7], 0 ), BinaryHelper.bitRead( packet[7], 3 ) );
            }                                                //	Addresse					Links/Rechts			Spule EIN/AUS
            //Check if Broadcast Flag is correct set up?
            boolean BCset = true;
            for (TypeActIP actIP : actIPList) {
              if ((actIP.client == client) && (actIP.bcFlag == 0)) {
                BCset = false;
                break;
              }
            }
            //Fall to next if no BCFlag is set!
            if (BCset) {
              break;
            }
          }
          case LAN_X_GET_TURNOUT_INFO: {
            if (LOG_RCV && rcv_packet_is_different) {
              Logger.info( "X_GET_TURNOUT_INFO " );
            }
            if (notifyz21AccessoryInfo) {
              data[0] = 0x43;  //X-HEADER
              data[1] = Byte.toUnsignedInt( packet[5] ); //High
              data[2] = Byte.toUnsignedInt( packet[6] ); //Low
              if (z21Interface.notifyz21AccessoryInfo( getAddrAccessory( packet[5], packet[6] ) )) {
                data[3] = 0x02;  //active
              }
              else {
                data[3] = 0x01;  //inactive
              }
              ethSend( client, replyPort, 0x09, LAN_X_Header, data, true, Z21bcNone );    //BC new 23.04. !!!(old = 0)
            }
            break;
          }
          case LAN_X_SET_EXT_ACCESSORY: {
            //Schalten erweiterten Zubehördecoder
            if (LOG_RCV && rcv_packet_is_different) {
              StringBuilder builder = new StringBuilder( "X_SET_EXT_ACCESSORY RAdr.:" );
              builder.append( getAddrAccessory( packet[5], packet[6] ));
              builder.append( ":" );
              builder.append( hexByte( packet[7] ) );
              Logger.info( builder.toString() );
            }
            if (notifyz21ExtAccessory) {
              z21Interface.notifyz21ExtAccessory( getAddrAccessory( packet[5], packet[6] ), Byte.toUnsignedInt( packet[7] ));
            }
            lastExtaccMsg = Byte.toUnsignedInt( packet[7] );        //speichere letztes Kommando!
            lastExtaccReceived = true;        //wir haben eine EXTACC empfangen!
            setExtACCInfo( getAddrAccessory( packet[5], packet[6] ), Byte.toUnsignedInt( packet[7] ), 0 );
            break;
          }
          case LAN_X_GET_EXT_ACCESSORY_INFO: {
            //kann mit folgendem Kommando der letzte an einen erweiterten Zubehördecoder übertragene Befehl abgefragt werden.
            if (LOG_RCV && rcv_packet_is_different) {
              StringBuilder builder = new StringBuilder( "X_EXT_ACCESSORY_INFO RAdr.:" );
              builder.append( getAddrAccessory( packet[5], packet[6] ) );
              builder.append( ":" );
              builder.append( hexByte( packet[7] ) );        //DB2 reserviert für zukünftige Erweiterungen
              Logger.info( builder.toString() );
            }
            if (lastExtaccReceived)
              setExtACCInfo( getAddrAccessory( packet[5], packet[6] ), lastExtaccMsg, 0 );        //0x00  Data Valid;
            else setExtACCInfo( getAddrAccessory( packet[5], packet[6] ), lastExtaccMsg, 0xFF );        //0xFF  Data Unknown
            break;
          }
          case LAN_X_SET_STOP: {
            if (LOG_RCV && rcv_packet_is_different) {
              Logger.info( "X_SET_STOP" );
            }
            if (notifyz21RailPower)
              z21Interface.notifyz21RailPower( csEmergencyStop );
            break;
          }
          case LAN_X_GET_LOCO_INFO: {
            if (LOG_RCV && rcv_packet_is_different) {
              Logger.info( "X_GET_LOCO_INFO" );
            }
            if (Byte.toUnsignedInt( packet[5]) == 0xF0) {  //DB0
              //Antwort: LAN_X_LOCO_INFO  Adr_MSB - Adr_LSB
              returnLocoStateFull( client, replyPort, getAddrLoco( packet[6], packet[7] ), false );
            }
            break;
          }
          case LAN_X_SET_LOCO: {
            if (LOG_RCV && rcv_packet_is_different) {
              Logger.info( "LAN_X_SET_LOCO replyPort:" + replyPort );
            }
            //setLocoBusy:
            addBusySlot( client, getAddrLoco( packet[6], packet[7] ) );

            if ((packet[5] & 0xF0) == 0x10) {  //DB0 => 0x1x = LAN_X_SET_LOCO_DRIVE
              //Logger.info("X_SET_LOCO_DRIVE ");
              int steps = 126;              //default value S=3; DCC 128 Fahrstufen minus 2x Stops
              if (packet[5] == 0x12) {      //S=2; DCC 28 Fahrstufen
                steps = 28;
              }
              else if (packet[5] == 0x10) { //S=0; DCC 14 Fahrstufen
                steps = 14;
              }
              if (notifyz21LocoSpeed) {
                z21Interface.setZ21LocoSpeed( getAddrLoco( packet[6], packet[7] ), Byte.toUnsignedInt( packet[8] ), steps );
              }
            }
            else if (Byte.toUnsignedInt( packet[5] ) == LAN_X_SET_LOCO_FUNCTION) {  //DB0 = 0xF8
              //LAN_X_SET_LOCO_FUNCTION  Adr_MSB        Adr_LSB            Type (00=AUS/01=EIN/10=UM)      Funktion
              if (notifyz21LocoFkt) {
                z21Interface.setZ21LocoFkt( getAddrLoco( packet[6], packet[7] ), Byte.toUnsignedInt( packet[8] ) >> 6, packet[8] & 0b00111111 );
              }
              //int Adr, int type, int fkt
            }
            //LAN_X_SET_LOCO_FUNCTION_GROUP:
            else if ((packet[5] == 0x20) && notifyz21LocoFkt0to4) {
              z21Interface.notifyz21LocoFkt0to4( getAddrLoco( packet[6], packet[7] ), packet[8] & 0x1F );                   //0 0 0 F0 F4 F3 F2 F1
            }
            else if ((packet[5] == 0x21) && notifyz21LocoFkt5to8) {
              z21Interface.notifyz21LocoFkt5to8( getAddrLoco( packet[6], packet[7] ), packet[8] & 0x0F );                   //0 0 0 0 F8 F7 F6 F5
            }
            else if ((packet[5] == 0x22) && notifyz21LocoFkt9to12) {
              z21Interface.notifyz21LocoFkt9to12( getAddrLoco( packet[6], packet[7] ), packet[8] & 0x1F );                  //0 0 0 0 F12 F11 F10 F9
            }
            else if ((packet[5] == 0x23) && notifyz21LocoFkt13to20) {
              z21Interface.notifyz21LocoFkt13to20( getAddrLoco( packet[6], packet[7] ), Byte.toUnsignedInt( packet[8] ) );    //F20 F19 F18 F17 F16 F15 F14 F13
            }
            else if ((packet[5] == 0x28) && notifyz21LocoFkt21to28) {
              z21Interface.notifyz21LocoFkt21to28( getAddrLoco( packet[6], packet[7] ), Byte.toUnsignedInt( packet[8] ) );    //F28 F27 F26 F25 F24 F23 F22 F21
            }
            else if ((packet[5] == 0x29) && notifyz21LocoFkt29to36) {
              z21Interface.notifyz21LocoFkt29to36( getAddrLoco( packet[6], packet[7] ), Byte.toUnsignedInt( packet[8] ) );    //F36 F35 F34 F33 F32 F31 F30 F29
            }
            else if (packet[5] == 0x2A) {        // F44 - F37
              if (notifyz21LocoFkt37to44) {
                z21Interface.notifyz21LocoFkt37to44( getAddrLoco( packet[6], packet[7] ), Byte.toUnsignedInt( packet[8] ) );  //F44 F43 F42 F41 F40 F39 F38 F37
              }
              return;        //keine Rückmeldung an die LAN-Clients
            }
            else if (packet[5] == 0x2B) {        // F52 - F45
              if (notifyz21LocoFkt45to52) {
                z21Interface.notifyz21LocoFkt45to52( getAddrLoco( packet[6], packet[7] ), Byte.toUnsignedInt( packet[8] ) );  //F52 F51 F50 F49 F48 F47 F46 F45
              }
              return;        //keine Rückmeldung an die LAN-Clients
            }
            else if (packet[5] == 0x50) {        // F60 - F53
              if (notifyz21LocoFkt53to60) {
                z21Interface.notifyz21LocoFkt53to60( getAddrLoco( packet[6], packet[7] ), Byte.toUnsignedInt( packet[8] ) );  //F60 F59 F58 F57 F56 F55 F54 F53
              }
              return;        //keine Rückmeldung an die LAN-Clients
            }
            else if (packet[5] == 0x51) {        // F68 - F61
              if (notifyz21LocoFkt61to68) {
                z21Interface.notifyz21LocoFkt61to68( getAddrLoco( packet[6], packet[7] ), Byte.toUnsignedInt( packet[8] ) );  //F68 F67 F66 F65 F64 F63 F62 F61
              }
              return;        //keine Rückmeldung an die LAN-Clients
            }
            //TODO notify erst wenn Loco bestätigt hat -- siehe auch Locomotive.setDCCFunctionState
            returnLocoStateFull( client, replyPort, getAddrLoco( packet[6], packet[7] ), true );        //Rückmeldung an die LAN-Clients!
            break;
          }
          case LAN_X_SET_LOCO_BINARY_STATE: {
            if (LOG_RCV && rcv_packet_is_different) {
              Logger.info( "LAN_X_SET_LOCO_BINARY_STATE" );
            }
            if (packet[5] == 0x5F) {        //DB0 = Binary State
              if (notifyz21LocoFktExt)
                z21Interface.notifyz21LocoFktExt( getAddrLoco( packet[6], packet[7] ), Byte.toUnsignedInt( packet[8] ), Byte.toUnsignedInt( packet[9] ) );
            }
            break;
          }
          case LAN_X_GET_FIRMWARE_VERSION: {
            if (LOG_RCV && rcv_packet_is_different) {
              Logger.info( "X_GET_FIRMWARE_VERSION" );
            }
            int[] z21FWVersion = z21Interface.getFWVersion();
            data[0] = 0xF3;                //identify Firmware (not change)
            data[1] = 0x0A;                //identify Firmware (not change)
            data[2] = z21FWVersion[1]; // z21FWVersionMSB = 0x01; //V_MSB
            data[3] = z21FWVersion[0]; // z21FWVersionLSB = 0x42; //V_LSB
            ethSend( client, replyPort, 0x09, LAN_X_Header, data, true, Z21bcNone );
            break;
          }
          case 0x73: {
            //LAN_X_??? WLANmaus periodische Abfrage: 
            //0x09 0x00 0x40 0x00 0x73 0x00 0xFF 0xFF 0x00
            //length X-Header	XNet-Msg			  speed?
            if (LOG_RCV && rcv_packet_is_different) {
              Logger.info( "LAN-X_WLANmaus" );
            }
            //set Broadcastflags for WLANmaus:
            if (addIPToSlot( client, replyPort, 0x00 ) == 0)
              addIPToSlot( client, replyPort, Z21bcAll );
            break;
          }
          default: {
            if (LOG_RCV && rcv_packet_is_different) {
              StringBuilder builder = new StringBuilder( "UNKNOWN_LAN-X_COMMAND" );
              for (int i = 0; i < packet[0]; i++) {
              	builder.append( " " );
              	builder.append( hexByte( packet[i] ) );
              }
              Logger.info( builder.toString() );
            }
            /*data[0] = 0x61;
            data[1] = 0x82;
            ethSend( client, 0x07, LAN_X_Header, data, true, Z21bcNone );
            */
          }
        }
        //---------------------- LAN X-Header ENDE ---------------------------	
        break;
      }
      case (LAN_SET_BROADCASTFLAGS): {
        int bcflag = Byte.toUnsignedInt( packet[7] );
        bcflag = packet[6] | (bcflag << 8);
        bcflag = packet[5] | (bcflag << 8);
        bcflag = packet[4] | (bcflag << 8);
        int effectiveBcFlag = addIPToSlot( client, replyPort, getLocalBcFlag( bcflag ) );
        if (LOG_RCV && rcv_packet_is_different) {
          StringBuilder builder = new StringBuilder( "LAN_SET_BROADCASTFLAGS: " );
          builder.append( BinaryHelper.hexByte( packet[7] ) );
          builder.append( "-" );
          builder.append( BinaryHelper.hexByte( packet[6] ) );
          builder.append( "-" );
          builder.append( BinaryHelper.hexByte( packet[5] ) );
          builder.append( "-" );
          builder.append( BinaryHelper.hexByte( packet[4] ) );
          builder.append( " effectiveBcFlag: " + BinaryHelper.hexByte( effectiveBcFlag ) );
          // 1=BC Power, Loco INFO, Trnt INFO; 2=BC Änderungen der Rückmelder am R-Bus
          // 4=BC Änderungen bei RailCom, 8=LocoNet
          Logger.info( builder.toString() );
        }
        break;
      }
      case (LAN_GET_BROADCASTFLAGS): {
        int flag = getz21BcFlag( addIPToSlot( client, replyPort, 0x00 ) );
        data[0] = flag;
        data[1] = (flag >> 8);
        data[2] = (flag >> 16);
        data[3] = (flag >> 24);
        if (LOG_RCV && rcv_packet_is_different) {
          Logger.info( "LAN_GET_BROADCASTFLAGS: " + Integer.toBinaryString( flag ) );
        }
        ethSend( client, replyPort, 0x08, LAN_GET_BROADCASTFLAGS, data, false, Z21bcNone );
        break;
      }
      case (LAN_GET_LOCOMODE): {
			/*
			In der Z21 kann das Ausgabeformat (DCC, MM) pro Lok-Adresse persistent gespeichert werden. 
			Es können maximal 256 verschiedene Lok-Adressen abgelegt werden. Jede Adresse >= 256 ist automatisch DCC.
			*/
        data[0] = Byte.toUnsignedInt( packet[4] );
        data[1] = Byte.toUnsignedInt( packet[5] );
        data[2] = 0;        //0=DCC Format; 1=MM Format
        ethSend( client, replyPort, 0x07, LAN_GET_LOCOMODE, data, false, Z21bcNone );
        break;
      }
      case (LAN_SET_LOCOMODE): {
        if (LOG_RCV) {
          Logger.info( "LAN_SET_LOCOMODE" );
        }
        //nothing to replay all DCC Format
        break;
      }
      case (LAN_GET_TURNOUTMODE): {
			/*
			In der Z21 kann das Ausgabeformat (DCC, MM) pro Funktionsdecoder-Adresse persistent gespeichert werden. 
			Es können maximal 256 verschiedene Funktionsdecoder -Adressen gespeichert werden. Jede Adresse >= 256 ist automatisch DCC.
			*/
        data[0] = Byte.toUnsignedInt( packet[4] );
        data[1] = Byte.toUnsignedInt( packet[5] );
        data[2] = 0;        //0=DCC Format; 1=MM Format
        ethSend( client, replyPort, 0x07, LAN_GET_LOCOMODE, data, false, Z21bcNone );
        break;
      }
      case (LAN_SET_TURNOUTMODE): {
        if (LOG_RCV) {
          Logger.info( "LAN_SET_TURNOUTMODE" );
        }
        //nothing to replay all DCC Format
        break;
      }
      case (LAN_RMBUS_GETDATA): {
        if (notifyz21S88Data) {
          if (LOG_RCV && rcv_packet_is_different) {
            Logger.info( "RMBUS_GETDATA" );
          }
          //ask for group state 'Gruppenindex'
          z21Interface.notifyz21S88Data( Byte.toUnsignedInt( packet[4] ) );        //normal Antwort hier nur an den anfragenden Client! (Antwort geht hier an alle!)
        }
        break;
      }
      case (LAN_RMBUS_PROGRAMMODULE):
        break;
      case (LAN_SYSTEMSTATE_GETDATA): {        //System state
        if (LOG_RCV && rcv_packet_is_different) {
          Logger.info( "LAN_SYSTEMSTATE_GETDATA" );
        }
        if (notifyz21getSystemInfo)
          z21Interface.notifyz21getSystemInfo( client, replyPort );
        break;
      }
      case (LAN_RAILCOM_GETDATA): {
        int Adr = 0;
        if (packet[4] == 0x01) {        //RailCom-Daten für die gegebene Lokadresse anfordern
          Adr = word( Byte.toUnsignedInt( packet[6] ), packet[5] );
        }
        if (notifyz21Railcom)
          Adr = z21Interface.notifyz21Railcom();        //return global Railcom Adr
        data[0] = (Adr >> 8);        //LocoAddress
        data[1] = (Adr & 0xFF);        //LocoAddress
        data[2] = 0x00;        //UINT32 ReceiveCounter Empfangszähler in Z21 
        data[3] = 0x00;
        data[4] = 0x00;
        data[5] = 0x00;
        data[6] = 0x00;        //UINT32 ErrorCounter Empfangsfehlerzähler in Z21
        data[7] = 0x00;
        data[8] = 0x00;
        data[9] = 0x00;
			  /*
			  data[10] = 0x00;	//UINT8 Reserved1 experimentell, siehe Anmerkung 
			  data[11] = 0x00;	//UINT8 Reserved2 experimentell, siehe Anmerkung 
			  data[12] = 0x00;	//UINT8 Reserved3 experimentell, siehe Anmerkung 
			  */
        ethSend( client, replyPort, 0x0E, LAN_RAILCOM_DATACHANGED, data, false, Z21bcNone );
        break;
      }
      case (LAN_LOCONET_FROM_LAN): {
        if (LOG_RCV && rcv_packet_is_different) {
          Logger.info( "LAN_LOCONET_FROM_LAN" );
        }

        int[] LNdata = new int[ packet[0] - 0x04 ];  //n Bytes
        for (int i = 0; i < (packet[0] - 0x04); i++)
          LNdata[i] = Byte.toUnsignedInt( packet[0x04 + i] );
        if (notifyz21LNSendPacket) {
          z21Interface.notifyz21LNSendPacket( LNdata, Byte.toUnsignedInt( packet[0] ) - 0x04 );
        }
        //Melden an andere LAN-Client das Meldung auf LocoNet-Bus geschrieben wurde
        ethSend( client, replyPort, Byte.toUnsignedInt( packet[0] ), LAN_LOCONET_FROM_LAN, LNdata, false, Z21bcLocoNet_s );  //LAN_LOCONET_FROM_LAN not to the client!

        break;
      }
      case (LAN_LOCONET_DISPATCH_ADDR): {
        if (notifyz21LNdispatch) {
          data[0] = Byte.toUnsignedInt( packet[4] );
          data[1] = Byte.toUnsignedInt( packet[5] );
          data[2] = z21Interface.notifyz21LNdispatch( word( Byte.toUnsignedInt( packet[5] ), packet[4] ), 0 );        //dispatchSlot
          if (LOG_RCV && rcv_packet_is_different) {
            Logger.info( "LOCONET_DISPATCH_ADDR " + word( Byte.toUnsignedInt( packet[5] ), packet[4] ) + "," + data[2] );
          }
          ethSend( client, replyPort, 0x07, LAN_LOCONET_DISPATCH_ADDR, data, false, Z21bcNone );
        }
        break;
      }
      case (LAN_LOCONET_DETECTOR): {
        if (notifyz21LNdetector) {
          if (LOG_RCV && rcv_packet_is_different) {
            Logger.info( "LAN_LOCONET_DETECTOR Abfrage" );
          }
          z21Interface.notifyz21LNdetector( client, Byte.toUnsignedInt( packet[4] ), word( Byte.toUnsignedInt( packet[6] ), packet[5] ) );        //Anforderung Typ & Reportadresse
        }
        break;
      }
      case (LAN_CAN_DETECTOR): {
        if (notifyz21CANdetector) {
          if (LOG_RCV && rcv_packet_is_different) {
            Logger.info( "LAN_CAN_DETECTOR" );
          }
          z21Interface.notifyz21CANdetector( client, Byte.toUnsignedInt( packet[4] ), word( Byte.toUnsignedInt( packet[6] ), packet[5] ) );        //Anforderung Typ & CAN-ID
        }
        break;
      }
      case (0x12): {       //configuration read
        // <-- 04 00 12 00 	
        // 0e 00 12 00 01 00 01 03 01 00 03 00 00 00
        for (int i = 0; i < 10; i++) {
          data[i] = z21Config.read( CONF1STORE + i );
        }
        ethSend( client, 0x0e, replyPort, 0x12, data, false, Z21bcNone );
        if (LOG_RCV && rcv_packet_is_different) {
          StringBuilder builder = new StringBuilder( "Z21 Eins(read) " );
          builder.append( "RailCom: " );
          builder.append( hexByte( data[0] ) );
          builder.append( ", PWR-Button: " );
          builder.append( hexByte( data[2] ) );
          builder.append( ", ProgRead: " );
          switch (data[3]) {
            case 0x00:
              builder.append( "nothing" );
              break;
            case 0x01:
              builder.append( "Bit" );
              break;
            case 0x02:
              builder.append( "Byte" );
              break;
            case 0x03:
              builder.append( "both" );
              break;
          }
          Logger.info( builder.toString() );
        }
        break;
      }
      case (0x13): {        //configuration write
        //<-- 0e 00 13 00 01 00 01 03 01 00 03 00 00 00 
        //0x0e = Length; 0x12 = Header
			/* Daten:
			(0x01) RailCom: 0=aus/off, 1=ein/on
			(0x00)
			(0x01) Power-Button: 0=Gleisspannung aus, 1=Nothalt
			(0x03) Auslese-Modus: 0=Nichts, 1=Bit, 2=Byte, 3=Beides
			*/
        if (LOG_RCV && rcv_packet_is_different) {
          StringBuilder builder = new StringBuilder( "Z21 Eins(write) ");
          builder.append( "RailCom: " );
          builder.append( hexByte( packet[4] ) );
          builder.append( ", PWR-Button: " );
          builder.append( hexByte( packet[6] ) );
          builder.append( ", ProgRead: " );
          switch (packet[7]) {
            case 0x00:
              builder.append( "nothing" );
              break;
            case 0x01:
              builder.append( "Bit" );
              break;
            case 0x02:
              builder.append( "Byte" );
              break;
            case 0x03:
              builder.append( "both" );
              break;
          }
          Logger.info( builder.toString() );
        }

        for (int i = 0; i < 10; i++) {
          z21Config.FSTORAGEMODE( CONF1STORE + i, Byte.toUnsignedInt( packet[4 + i] ) );
        }

        //Request DCC to change
        if (notifyz21UpdateConf)
          z21Interface.notifyz21UpdateConf();
        break;
      }
      case (0x16): { //configuration read
        //<-- 04 00 16 00 
        //14 00 16 00 19 06 07 01 05 14 88 13 10 27 32 00 50 46 20 4e 

        for (int i = 0; i < 16; i++) {
          data[i] = z21Config.read( CONF2STORE + i );
        }


        //check range of MainV:
        if ((word( data[13], data[12] ) > 0x59D8) || (word( data[13], data[12] ) < 0x2A8F)) {
          //set to 20V default:
          data[13] = highByte( 0x4e20 );
          data[12] = lowByte( 0x4e20 );
        }
        //check range of ProgV:
        if ((word( data[15], data[14] ) > 0x59D8) || (word( data[15], data[14] ) < 0x2A8F)) {
          //set to 20V default:
          data[15] = highByte( 0x4e20 );
          data[14] = lowByte( 0x4e20 );
        }

        ethSend( client, replyPort, 0x14, 0x16, data, false, Z21bcNone );
        if (LOG_RCV && rcv_packet_is_different) {
          StringBuilder builder = new StringBuilder( "Z21 Eins(read) " );
          builder.append( "RstP(s): " );
          builder.append( data[0] );                //EEPROM Adr 60
          builder.append( ", RstP(f): " );
          builder.append( data[1] );                //EEPROM Adr 61
          builder.append( ", ProgP: " );
          builder.append( data[2] );                //EEPROM Adr 62
          builder.append( ", MainV: " );
          builder.append( word( data[13], data[12] ) );   //Value only: 11000 - 23000
          builder.append( ", ProgV: " );
          builder.append( word( data[15], data[14] ) );   //Value only: 11000=0x2A8F - 23000=0x59D8
          Logger.info( builder.toString() );
        }
        break;
      }
      case (0x17): {        //configuration write
        //<-- 14 00 17 00 19 06 07 01 05 14 88 13 10 27 32 00 50 46 20 4e 
        //0x14 = Length; 0x16 = Header(read), 0x17 = Header(write)
			/* Daten:
			(0x19) Reset Packet (starten) (25-255)
			(0x06) Reset Packet (fortsetzen) (6-64)
			(0x07) Programmier-Packete (7-64)
			(0x01) ?
			(0x05) ?
			(0x14) ?
			(0x88) ?
			(0x13) ?
			(0x10) ?
			(0x27) ?
			(0x32) ?
			(0x00) ?
			(0x50) Hauptgleis (LSB) (11-23V)
			(0x46) Hauptgleis (MSB)
			(0x20) Programmiergleis (LSB) (11-23V): 20V=0x4e20, 21V=0x5208, 22V=0x55F0
			(0x4e) Programmiergleis (MSB)
			*/
        if (LOG_RCV && rcv_packet_is_different) {
          StringBuilder builder = new StringBuilder( "Z21 Eins(write) " );
          builder.append( "RstP(s): " );
          builder.append( Byte.toUnsignedInt( packet[4] ) );                //EEPROM Adr 60
          builder.append( ", RstP(f): " );
          builder.append( Byte.toUnsignedInt( packet[5] ) );                //EEPROM Adr 61
          builder.append( ", ProgP: " );
          builder.append( Byte.toUnsignedInt( packet[6] ) );                //EEPROM Adr 62
          builder.append( ", MainV: " );
          builder.append( word( Byte.toUnsignedInt( packet[17] ), packet[16] ) );
          builder.append( ", ProgV: " );
          builder.append( word( Byte.toUnsignedInt( packet[19] ), packet[18] ) );
          Logger.info( builder.toString() );
        }
        for (int i = 0; i < 16; i++) {
          z21Config.FSTORAGEMODE( CONF2STORE + i, Byte.toUnsignedInt( packet[4 + i] ) );
        }
			/*
			#if defined(ESP8266) || defined(ESP32)
			z21Config.commit();
			}
			*/
        //Request DCC to change
        if (notifyz21UpdateConf)
          z21Interface.notifyz21UpdateConf();
        break;
      }
      default: {
        if (LOG_RCV && rcv_packet_is_different) {
          StringBuilder builder = new StringBuilder( "UNKNOWN_COMMAND" );
          for (int i = 0; i < packet[0]; i++) {
            builder.append(" ");
            builder.append( hexByte( packet[i] ) );
          }
          Logger.info( builder.toString() );
        }
        data[0] = 0x61;
        data[1] = 0x82;
        ethSend( client, replyPort, 0x07, LAN_X_Header, data, true, Z21bcNone );
      }
    }


    //---------------------------------------------------------------------------------------
    //check if IP is still used:
    long currentMillis = System.currentTimeMillis();
    if ((currentMillis - z21IPpreviousMillis) > z21IPinterval) {
      z21IPpreviousMillis = currentMillis;
      for (int i = actIPList.size()-1; i >= 0; i--) {
        TypeActIP actIP = actIPList.get( i );
        if (actIP.time > 0) {
          actIP.time--;    //Zeit herrunterrechnen
        }
        else {
          actIPList.remove( i );        //clear IP DATA
          //send MESSAGE clear Client
        }
      }
    }
  }

  private static int getAddrLoco( byte highByte, byte lowByte ) {
    return ((Byte.toUnsignedInt( highByte ) & 0x3F) << 8) + Byte.toUnsignedInt( lowByte );
  }

  private static int getAddrAccessory( byte highByte, byte lowByte ) {
    return (Byte.toUnsignedInt( highByte ) << 8) + Byte.toUnsignedInt( lowByte ) + 1;
  }

  private boolean checkIfPacketIsDifferent( byte[] last_packet, byte[] current_packet ) {
    if (last_packet.length != current_packet.length) {
      return true;
    }

    for (int i = 0; i < current_packet.length; i ++ ) {
      if ( last_packet[i] != current_packet[i] ) {
        return true;
      }
    }

    return false;
  }

  private boolean checkIfPacketIsDifferent( int[] last_packet, int[] current_packet ) {
    if (last_packet.length != current_packet.length) {
      return true;
    }

    for (int i = 0; i < current_packet.length; i ++ ) {
      if ( last_packet[i] != current_packet[i] ) {
        return true;
      }
    }

    return false;
  }

  //--------------------------------------------------------------------------------------------
//Zustand der Gleisversorgung setzten
  public void setPower( int state ) {
    int[] data = {LAN_X_BC_TRACK_POWER, 0x00};
    railpower = state;
    switch (state) {
      case csNormal: {
        data[1] = 0x01;
        break;
      }
      case csTrackVoltageOff: {
        data[1] = 0x00;
        break;
      }
      case csServiceMode: {
        data[1] = 0x02;
        break;
      }
      case csShortCircuit: {
        data[1] = 0x08;
        break;
      }
      case csEmergencyStop: {
        data[0] = 0x81;
        data[1] = 0x00;
        break;
      }
    }
    ethSend( null, Z21_PORT, 0x07, LAN_X_Header, data, true, Z21bcAll_s );
    if (LOG_OTHER) {
      Logger.info( "set_X_BC_TRACK_POWER " + hexByte( state ) );
    }
  }

  //--------------------------------------------------------------------------------------------
//Abfrage letzte Meldung über Gleispannungszustand
  public int getPower() {
    return railpower;
  }

  //--------------------------------------------------------------------------------------------
//return request for POM read byte
  public void setCVPOMBYTE( int cvAddr, int value ) {
    int[] data = new int[5];
    data[0] = 0x64; //X-Header
    data[1] = 0x14; //DB0
    data[2] = ((cvAddr >> 8) & 0x3F);  //CV_MSB;
    data[3] = (cvAddr & 0xFF); //CV_LSB;
    data[4] = value;
    ethSend( null, Z21_PORT, 0x0A, LAN_X_Header, data, true, Z21bcAll_s );
  }


  //--------------------------------------------------------------------------------------------
//Zustand Rückmeldung non - Z21 device - Busy!
  public void setLocoStateExt( int Adr ) {
/*	int ldata[6];
	if (notifyz21LocoState)
		notifyz21LocoState(Adr, ldata); //int Steps[0], int Speed[1], int F0[2], int F1[3], int F2[4], int F3[5]
	
	int[] data = new int[10];
	data[0] = LAN_X_LOCO_INFO;  //0xEF X-HEADER
	data[1] = (Adr >> 8) & 0x3F;
	data[2] = Adr & 0xFF;
	// Fahrstufeninformation: 0=14, 2=28, 4=128 
	if ((ldata[0] & 0x03) == DCCSTEP14)
		data[3] = 0;	// 14 steps
	if ((ldata[0] & 0x03) == DCCSTEP28)
		data[3] = 2;	// 28 steps
	if ((ldata[0] & 0x03) == DCCSTEP128)		
		data[3] = 4;	// 128 steps
	data[3] = data[3] | 0x08; //BUSY!
		
	data[4] = (char) ldata[1];	//DSSS SSSS
	data[5] = (char) ldata[2] & 0x1F;    //F0, F4, F3, F2, F1
	data[6] = (char) ldata[3];    //F5 - F12; Funktion F5 ist bit0 (LSB)
	data[7] = (char) ldata[4];  //F13-F20
	data[8] = (char) ldata[5];  //F21-F28
	data[9] = (char) ldata[8] >> 7;	//F31-F29 only
*/
    reqLocoBusy( Adr );

    returnLocoStateFull( null, Z21_PORT, Adr, true );

    //EthSend(0, 15, LAN_X_Header, data, true, Z21bcAll_s | Z21bcNetAll_s);  //Send Loco Status und Funktions to all active Apps 
  }

  //--------------------------------------------------------------------------------------------

  /**
   * Returns the current locomotive status to requester.
   * Retrieves the managed locomotive action based on the DCC address.
   *
   * @param addr the DCC address of the locomotive to get the state from
   */
  public void returnLocoStateFull( int addr ) {
    for (TypeActIP typeActIP : actIPList) {
      if (typeActIP.adr == addr) {
        returnLocoStateFull( typeActIP.client, typeActIP.clientPort, addr, false );
      }
    }
  }

  /**
   * Returns the current locomotive status to requester.
   *
   * @param client
   * @param sendPort
   * @param addr
   * @param bc true => to inform also other client over the change; false => just ask about the accessory state
   */
  public void returnLocoStateFull( InetAddress client, int sendPort, int addr, boolean bc ) {
    if (addr <= 0) {
      //Not a valid loco adr!
      return;
    }

    int[] ldata;
    if (notifyz21LocoState) {
      ldata = z21Interface.requestZ21LocoState( addr ); //int Steps[0], int Speed[1], int F0[2], int F1[3], int F2[4], int F3[5]
    }
    else {
      ldata = new int[6];
    }

    int[] data = new int[10];
    data[0] = (LAN_X_LOCO_INFO);  //0xEF X-HEADER
    data[1] = ((addr >> 8) & 0x3F);
    data[2] = (addr & 0xFF);
    // Fahrstufeninformation: 0=14, 2=28, 4=128 
    if ((ldata[0] & 0x03) == DCCSTEP14)
      data[3] = 0;        // 14 steps
    if ((ldata[0] & 0x03) == DCCSTEP28)
      data[3] = 2;        // 28 steps
    if ((ldata[0] & 0x03) == DCCSTEP128)
      data[3] = 4;        // 128 steps
    data[3] = (data[3] | 0x08); //BUSY!

    data[4] = ldata[1];        //DSSS SSSS
    data[5] = (ldata[2] & 0x1F);  //F0, F4, F3, F2, F1
    data[6] = ldata[3];  //F5 - F12; Funktion F5 ist bit0 (LSB)
    data[7] = ldata[4];  //F13-F20
    data[8] = ldata[5];  //F21-F28
    data[9] = (ldata[2] >> 7);        //F31-F29

    //Info to all:
    for (TypeActIP actIP : actIPList) {
      if (actIP.client != client) {
        if ((actIP.bcFlag & (Z21bcAll_s | Z21bcNetAll_s)) > 0) {
          if (bc) {
            ethSend( actIP.client, actIP.clientPort, 15, LAN_X_Header, data, true, Z21bcNone );  //Send Loco status und Funktions to BC Apps
          }
        }
      }
      else { //Info to client that ask:
        if (actIP.adr == addr) {
          data[3] = (data[3] & 0b111);        //clear busy flag!
        }
        ethSend( client, sendPort, 15, LAN_X_Header, data, true, Z21bcNone );  //Send Loco status und Funktions to request App
        data[3] = (data[3] | 0x08); //BUSY!
      }
    }
  }

  //--------------------------------------------------------------------------------------------
//return state of S88 sensors
  public void setS88Data( int[] data ) {
    ethSend( null, Z21_PORT, 0x0F, LAN_RMBUS_DATACHANGED, data, false, Z21bcRBus_s ); //RMBUS_DATACHANED
  }

  //--------------------------------------------------------------------------------------------
//return state from LN detector

  public void returnSensorStateChanged( int feedbackAddr, int locoAddr, Z21LocoDir locoDir ) {
    returnSensorStateChanged( null, -1, feedbackAddr, locoAddr, locoDir, true );
  }

  /**
   * TODO:
   * - implement using LISSY!
   * - create const
   *
   * @param client
   * @param sendPort
   * @param feedbackAddr
   * @param locoAddr
   * @param bc
   */
  public void returnSensorStateChanged( InetAddress client, int sendPort, int feedbackAddr, int locoAddr, Z21LocoDir locoDir, boolean bc ) {
    if (feedbackAddr <= 0) {
      //Not a valid loco adr!
      return;
    }

    // TODO?
    int internalAddr = locoAddr - 1;

    // TODO: prepared for implementing - not yet working!
    int dataLen;
    int[] data;
    if (true) {
      dataLen = 3 + 3; // 3 for addressing LISSY, 3 for info
      data = new int[dataLen];

      data[0] = 0x10; // LISSY Lokadresse
      data[1] = BinaryHelper.lowByte( feedbackAddr ); // message addr
      data[2] = BinaryHelper.highByte( feedbackAddr );

      data[3] = BinaryHelper.lowByte( internalAddr ); // loco addr
      data[4] = BinaryHelper.highByte( internalAddr );
      data[5] = 0x0; // 0 DIR1 DIR0 0 K3 K2 K1 K0
      data[5] = BinaryHelper.bitSet( data[5], 6 );
      if (locoDir == Z21LocoDir.backward) { // DIR0=0 ist vorwärts, DIR0=1 ist rückwärts
        data[5] = BinaryHelper.bitSet( data[5], 5 );
      }
    }
    else {
      dataLen = 3; // request status
      data = new int[dataLen];

      data[0] = 0x82; // LISSY ab Z21 FW Version 1.23
      data[1] = BinaryHelper.lowByte( feedbackAddr ); // message addr
      data[2] = BinaryHelper.highByte( feedbackAddr );
    }

    if(LOG_SND) {
      StringBuilder sb = new StringBuilder( "to be sent: " );
      sb.append( BinaryHelper.hexStr( data ) );
      Logger.info( sb.toString() );
    }

    //Info to all:
    if (client != null) {
      ethSend( client, sendPort, 0x04 + dataLen, LAN_LOCONET_DETECTOR, data, false, Z21bcNone );  //LAN_LOCONET_DETECTOR
    }
    else {
      ethSend( null, sendPort, 0x04 + dataLen, LAN_LOCONET_DETECTOR, data, false, Z21bcLocoNet_s );  //LAN_LOCONET_DETECTOR
    }
  }

  //--------------------------------------------------------------------------------------------
//LN Meldungen weiterleiten
  boolean setLNMessage( int[] data, int dataLen, int bcType, boolean TX ) {
    if (dataLen > 20)        //Z21 LocoNet tunnel DATA has max 20 Byte!
      return false;
    if (TX) {  //Send by Z21 or Receive a Packet?
      ethSend( null, Z21_PORT, 0x04 + dataLen, LAN_LOCONET_Z21_TX, data, false, bcType );  //LAN_LOCONET_Z21_TX
    }
    else {
      ethSend( null, Z21_PORT, 0x04 + dataLen, LAN_LOCONET_Z21_RX, data, false, bcType );  //LAN_LOCONET_Z21_RX
    }
    return true;
  }

  //--------------------------------------------------------------------------------------------
//return state from CAN detector

  /**
   * Notify clients via CAN.
   *
   * @param nid e.g. from 2 char Block id
   * @param addr sensor/message/feedback DCC addr; will be adjusted by -1 according to specification
   * @param port currently always 0
   * @param locoAddr
   * @param clockDir locomotive alignment on the track
   */
  void returnSensorStateChanged( int nid, int addr, int port, int locoAddr, Z21LocoDir clockDir ) {

    // encode direction in highest 2 bits
    int internalLocoAddr = locoAddr;
    if (clockDir == Z21LocoDir.forward) { // 10 Fahrzeug ist vorwärts auf das Gleis gestellt worden
      internalLocoAddr = BinaryHelper.bitSet( internalLocoAddr, 15 );
      internalLocoAddr = BinaryHelper.bitClear( internalLocoAddr, 14 );
    }
    else if(clockDir == Z21LocoDir.backward) { // 11 Fahrzeug ist rückwärts auf das Gleis gestellt worden
      internalLocoAddr = BinaryHelper.bitSet( internalLocoAddr, 15 );
      internalLocoAddr = BinaryHelper.bitSet( internalLocoAddr, 14 );
    }
    else {
      internalLocoAddr &= 0x3fff; // 0x
    }
    int typ;
    int v1;
    int v2;
    if (locoAddr == 0) {
      typ = 0x01;
      v1 = 0x0100;
      v2 = 0;
    }
    else {
      typ = 0x11;
      v1 = internalLocoAddr;
      v2 = 0; // always only 1 loco
    }
    setCANDetector( nid, addr, port, typ, v1, v2 );
  }

  void setCANDetector( int nid, int addr, int port, int typ, int v1, int v2 ) {
    int[] data = new int[10];
    data[0] = BinaryHelper.lowByte( nid );
    data[1] = BinaryHelper.highByte( nid );
    data[2] = BinaryHelper.lowByte( addr );
    data[3] = BinaryHelper.highByte( addr );
    data[4] = (port);
    data[5] = (typ);
    data[6] = BinaryHelper.lowByte( v1 );
    data[7] = BinaryHelper.highByte( v1 );
    data[8] = BinaryHelper.lowByte( v2 );
    data[9] = BinaryHelper.highByte( v2 );
    ethSend( null, Z21_PORT, 0x0E, LAN_CAN_DETECTOR, data, false, Z21bcCANDetector_s );  //CAN_DETECTOR
  }

  //--------------------------------------------------------------------------------------------

  /**
   * Returns the current accessory state change.
   *
   * @param addr the DCC address of the accessory to get the state from
   */
  public void returnAccessoryStateChanged( int addr ) {
    returnAccessoryStateChanged( null, -1, addr, true );
  }

  /**
   * Returns the current accessory state change to requester.
   *
   * @param client
   * @param sendPort
   * @param addr
   * @param bc true => to inform also other client over the change; false => just ask about the accessory state
   */
  private void returnAccessoryStateChanged( InetAddress client, int sendPort, int addr, boolean bc ) {
    if (addr <= 0) {
      //Not a valid accessory adr!
      return;
    }

    // TODO?
    int internalAddr = addr - 1;

    int[] data = new int[4];
    data[0] = (LAN_X_TURNOUT_INFO);  //0x43 X-HEADER
    data[1] = BinaryHelper.highByte( internalAddr );   //High
    data[2] = BinaryHelper.lowByte( internalAddr ); //Low

    // accessory state
    if (z21Interface.notifyz21AccessoryInfo( addr )) {
      data[3] = 0x02;  //active
    }
    else {
      data[3] = 0x01;  //inactive
    }
    data[3] = (data[3] | 0x08); //BUSY!

    //Info to all:
    for (TypeActIP actIP : actIPList) {
      if (actIP.client != client) {
        if ((actIP.bcFlag & (Z21bcAll_s | Z21bcNetAll_s)) > 0) {
          if (bc) {
            ethSend( actIP.client, actIP.clientPort, 0x09, LAN_X_Header, data, true, Z21bcNone );  //Send Loco status und Funktions to BC Apps
          }
        }
      }
      else { //Info to client that ask:
        if (actIP.adr == addr) {
          data[3] = (data[3] & 0b111);        //clear busy flag!
        }
        ethSend( client, sendPort, 0x09, LAN_X_Header, data, true, Z21bcNone );  //Send Loco status und Funktions to request App
        data[3] = (data[3] | 0x08); //BUSY!
      }
    }
  }

  //--------------------------------------------------------------------------------------------
//Return EXT accessory info
  void setExtACCInfo( int Adr, int State, int Status ) {
    int[] data = new int[5];
    data[0] = LAN_X_GET_EXT_ACCESSORY_INFO;  //0x44 X-HEADER
    data[1] = (Adr >> 8);   //High
    data[2] = (Adr & 0xFF); //Low
    data[3] = State;
    data[4] = Status;  //0x00  Data Valid; 0xFF  Data Unknown
    ethSend( null, Z21_PORT, 0x0A, LAN_X_Header, data, true, Z21bcAll_s );
  }

  //--------------------------------------------------------------------------------------------
//Return CV Value for Programming
  void setCVReturn( int CV, int value ) {
    int[] data = new int[5];
    data[0] = LAN_X_CV_RESULT;   //0x64 X-Header
    data[1] = 0x14; //DB0
    data[2] = (CV >> 8);  //CV_MSB;
    data[3] = (CV & 0xFF); //CV_LSB;
    data[4] = (value);
    ethSend( null, Z21_PORT, 0x0A, LAN_X_Header, data, true, Z21bcAll_s );
  }

  //--------------------------------------------------------------------------------------------
//Return no ACK from Decoder
  void setCVNack() {
    int[] data = new int[2];
    data[0] = LAN_X_CV_NACK;  //0x61 X-Header
    data[1] = 0x13; //DB0
    ethSend( null, Z21_PORT, 0x07, LAN_X_Header, data, true, Z21bcAll_s );
  }

  //--------------------------------------------------------------------------------------------
//Return Short while Programming
  void setCVNackSC() {
    int[] data = new int[2];
    data[0] = LAN_X_CV_NACK_SC;   //0x61 X-Header
    data[1] = 0x12; //DB0
    ethSend( null, Z21_PORT, 0x07, LAN_X_Header, data, true, Z21bcAll_s );
  }

  //--------------------------------------------------------------------------------------------
//Send Changing of SystemInfo
  void sendSystemInfo( InetAddress client, int sendPort, int maincurrent, int mainvoltage, int temp ) {
    int[] data = new int[16];
    data[0] = (maincurrent & 0xFF);  //MainCurrent mA
    data[1] = (maincurrent >> 8);  //MainCurrent mA
    data[2] = data[0];  //ProgCurrent mA
    data[3] = data[1];  //ProgCurrent mA        
    data[4] = data[0];  //FilteredMainCurrent
    data[5] = data[1];  //FilteredMainCurrent
    data[6] = (temp & 0xFF);  //Temperature
    data[7] = (temp >> 8);  //Temperature
    data[8] = (mainvoltage & 0xFF);  //SupplyVoltage
    data[9] = (mainvoltage >> 8);  //SupplyVoltage
    data[10] = data[8];  //VCCVoltage
    data[11] = data[9];  //VCCVoltage
    data[12] = railpower;  //CentralState
    if (data[12] == csServiceMode)
      data[12] = 0x20;
/*Bitmasken für CentralState: 
	public static final int csEmergencyStop  0x01 // Der Nothalt ist eingeschaltet 
	public static final int csTrackVoltageOff  0x02 // Die Gleisspannung ist abgeschaltet 
	public static final int csShortCircuit  0x04 // Kurzschluss 
	public static final int csProgrammingModeActive 0x20 // Der Programmiermodus ist aktiv 	
*/
    data[13] = 0x00;  //CentralStateEx
/* Bitmasken für CentralStateEx: 
	public static final int cseHighTemperature  0x01 // zu hohe Temperatur 
	public static final int csePowerLost  0x02 // zu geringe Eingangsspannung 
	public static final int cseShortCircuitExternal 0x04 // am externen Booster-Ausgang 
	public static final int cseShortCircuitInternal 0x08 // am Hauptgleis oder Programmiergleis 
	public static final int cseRCN213 0x20 // Weichenadressierung gem. RCN213	
*/
    data[14] = 0x00;  //reserved
    data[15] = 0x01;  //Capabilitie DCC only
    if (z21Config.read( CONF1STORE ) == 0x01)        //RailCom
      data[15] |= 0x08;        //RailCom aktiv!
    data[15] |= 0x10 | 0x20 | 0x40;                //LAN-Befehle
/*	
	public static final int capDCC 0x01 // beherrscht DCC
	public static final int capMM 0x02 // beherrscht MM
	//public static final int capReserved 0x04 // reserviert für zukünftige Erweiterungen
	public static final int capRailCom 0x08 // RailCom ist aktiviert
	public static final int capLocoCmds 0x10 // akzeptiert LAN-Befehle für Lokdecoder
	public static final int capAccessoryCmds 0x20 // akzeptiert LAN-Befehle für Zubehördecoder
	public static final int capDetectorCmds 0x40 // akzeptiert LAN-Befehle für Belegtmelder
	public static final int capNeedsUnlockCode 0x80 // benötigt Freischaltcode (z21start)
*/
    //only to the request client if or if client = 0 to all that select this message (Abo)!
    if (client != null) {
      ethSend( client, sendPort, 0x14, LAN_SYSTEMSTATE_DATACHANGED, data, false, Z21bcNone );
    }
    else {
      ethSend( null, Z21_PORT, 0x14, LAN_SYSTEMSTATE_DATACHANGED, data, false, Z21bcSystemInfo_s );
    }
  }

// Private Methods ///////////////////////////////////////////////////////////////////////////////////////////////////
// Functions only available to other functions in this library *******************************************************

  //--------------------------------------------------------------------------------------------
  void ethSend( InetAddress client, int sendPort, int dataLen, int header, int[] dataString, boolean withXOR, int bc ) {
    int[] data = new int[dataLen];                        //z21 send storage

    //--------------------------------------------        
    //XOR bestimmen:
    data[0] = (dataLen & 0xFF);
    data[1] = (dataLen >> 8);
    data[2] = (header & 0xFF);
    data[3] = (header >> 8);
    data[dataLen - 1] = 0;        //XOR

    int count;
    if (withXOR) {
      count = dataLen - 5;
    }
    else {
      count = dataLen - 4;
    }
    int pos = 0;
    for (int i = 0; i < count; i++) { //Ohne Length und Header und XOR
      if (withXOR) {
        data[dataLen - 1] = data[dataLen - 1] ^ dataString[pos];
      }
      data[i + 4] = dataString[pos];
      pos++;
    }

    final boolean snd_packet_is_different = checkIfPacketIsDifferent( last_snd_packet, data );
    if (snd_packet_is_different) {
      last_snd_packet = data;
    }

    //--------------------------------------------
    if ((client != null) && (bc == Z21bcNone)) {
      if (notifyz21EthSend) {
        z21Interface.notifyz21EthSend( client, sendPort, data );
      }
      if (LOG_SND) {
        if (snd_packet_is_different) {
          StringBuilder builder = new StringBuilder( "--> Sent CTX " );
          builder.append( client );
          builder.append( ": " );
          builder.append( BinaryHelper.hexStr( data ) );
          Logger.info( builder.toString() );
        }
        else if (LOG_SND_REPEAT) {
          Logger.info( "--> Sent CTX (same as before) " );
        }
      }
    }
    else {
      InetAddress clientOut = null; //client;
      for (TypeActIP actIP : actIPList) {
        if ((actIP.time > 0) && ((bc & actIP.bcFlag) > 0)) {    //Boradcast & Noch aktiv
          int clientPort = actIP.clientPort;
          if (bc != 0) {
            if (bc == Z21bcAll_s) {
              clientOut = null;        //ALL
            }
            else {
              clientOut = actIP.client;
            }
          }

          if ((clientOut != client) || (clientOut == null)) {        //wenn client > 0 und nicht Z21bcNone, sende an alle außer den client!

            //--------------------------------------------
            if (notifyz21EthSend) {
              z21Interface.notifyz21EthSend( clientOut, clientPort, data );
            }
            if (LOG_SND) {
              if (snd_packet_is_different) {
                StringBuilder builder = new StringBuilder( "--> Sent BTX " );
                builder.append( clientOut ).append( ":" ).append( clientPort );
                builder.append( " BC:" );
                builder.append( Integer.toBinaryString( bc & actIP.bcFlag ) );
                builder.append( " : " );
                builder.append( BinaryHelper.hexStr( data ) );
                Logger.info( builder.toString() );
              }
              else if (LOG_SND_REPEAT) {
                Logger.info( "--> Sent BTX (same as before) " );
              }
            }
            if (clientOut == null)
              return;
          }
        }
      }
    }
  }

  //--------------------------------------------------------------------------------------------
//Convert local stored flag back into a Z21 Flag
  public int getz21BcFlag( int flag ) {
    int outFlag = 0;
    if ((flag & Z21bcAll_s) != 0)
      outFlag |= Z21bcAll;
    if ((flag & Z21bcRBus_s) != 0)
      outFlag |= Z21bcRBus;
    if ((flag & Z21bcSystemInfo_s) != 0)
      outFlag |= Z21bcSystemInfo;
    if ((flag & Z21bcNetAll_s) != 0)
      outFlag |= Z21bcNetAll;
    if ((flag & Z21bcLocoNet_s) != 0)
      outFlag |= Z21bcLocoNet;
    if ((flag & Z21bcLocoNetLocos_s) != 0)
      outFlag |= Z21bcLocoNetLocos;
    if ((flag & Z21bcLocoNetSwitches_s) != 0)
      outFlag |= Z21bcLocoNetSwitches;
    if ((flag & Z21bcLocoNetGBM_s) != 0)
      outFlag |= Z21bcLocoNetGBM;
    return outFlag;
  }

  //--------------------------------------------------------------------------------------------
//Convert Z21 LAN BC flag to local stored flag
  int getLocalBcFlag( int flag ) {
    int outFlag = 0;
    if ((flag & Z21bcAll) != 0)
      outFlag |= Z21bcAll_s;
    if ((flag & Z21bcRBus) != 0)
      outFlag |= Z21bcRBus_s;
    if ((flag & Z21bcSystemInfo) != 0)
      outFlag |= Z21bcSystemInfo_s;
    if ((flag & Z21bcNetAll) != 0)
      outFlag |= Z21bcNetAll_s;
    if ((flag & Z21bcLocoNet) != 0)
      outFlag |= Z21bcLocoNet_s;
    if ((flag & Z21bcLocoNetLocos) != 0)
      outFlag |= Z21bcLocoNetLocos_s;
    if ((flag & Z21bcLocoNetSwitches) != 0)
      outFlag |= Z21bcLocoNetSwitches_s;
    if ((flag & Z21bcLocoNetGBM) != 0)
      outFlag |= Z21bcLocoNetGBM_s;
    if ((flag & Z21bcCANDetector) != 0)
      outFlag |= Z21bcCANDetector_s;
    return outFlag;
  }

  //--------------------------------------------------------------------------------------------
  void clearIPSlots() {
    actIPList.clear();
  }

  //--------------------------------------------------------------------------------------------
  void clearIPSlot( InetAddress client ) {
    for (int i = actIPList.size()-1; i >= 0; i--) {
      TypeActIP actIP = actIPList.get( i );
      if (actIP.client == client) {
        actIPList.remove( i );
        return;
      }
    }
  }

  //--------------------------------------------------------------------------------------------
//speichern des BCFlag im EEPROM
  void setEEPROMBCFlag( int IPHash, int bcFlag ) {
    z21Config.FSTORAGEMODE( CLIENTHASHSTORE | IPHash, bcFlag );
    if (LOG_OTHER) {
      Logger.info( ""+ (CLIENTHASHSTORE | IPHash) + " write: " + Integer.toBinaryString( bcFlag ));
    }
  }

  //--------------------------------------------------------------------------------------------
//lesen des BCFlag im EEPROM
  public int findEEPROMBCFlag( int IPHash ) {
    int flag = z21Config.read( CLIENTHASHSTORE | IPHash );
    if (LOG_OTHER) {
      Logger.info( "" + (CLIENTHASHSTORE | IPHash) + "read: " + Integer.toBinaryString( flag ) );
    }
    //wurde BC im EEPROM bereits erfasst?
    if (flag == 0xFF)
      return 0x00;        //not found!
    return flag;
  }

  //--------------------------------------------------------------------------------------------

  /**
   * Returns the current effective broadcast flags.
   *
   * @param client
   * @param replyPort
   * @param bcFlag
   * @return
   */
  int addIPToSlot( InetAddress client, int replyPort, int bcFlag ) {
    //int slot = z21clientMAX;

    for (TypeActIP actIP : actIPList) {
      if (actIP.client == client) {
        actIP.time = z21ActTimeIP;
        if (replyPort != 0 && replyPort != actIP.clientPort) {
          Logger.info( "  ...replyPort changed from " + actIP.clientPort + " to " + replyPort );
          actIP.clientPort = replyPort;
        }
        if (bcFlag != 0) {   //Falls BC Flag übertragen wurde diesen hinzufügen!
          actIP.bcFlag = bcFlag;
          /*if (notifyz21ClientHash)
            setEEPROMBCFlag( z21Interface.notifyz21ClientHash( client ), bcFlag );
           */
        }
        return actIP.bcFlag;    //BC Flag 4. Byte Rückmelden
      }
    }
    TypeActIP actIP = new TypeActIP();
    actIP.client = client;
    actIP.clientPort = replyPort;
    actIP.bcFlag = 0;
    actIP.time = z21ActTimeIP;
    actIP.adr = 0;
    actIPList.add( actIP );
    setPower( railpower );                //inform the client with last power state

    //read out last BCFlag from EEPROM:
    /*if (notifyz21ClientHash)
      actIP.bcFlag = findEEPROMBCFlag( z21Interface.notifyz21ClientHash( client ) );
    */
    return actIP.bcFlag;   //BC Flag 4. Byte Rückmelden
  }

  //--------------------------------------------------------------------------------------------
//check if there are slots with the same loco, set them to busy
  void setOtherSlotBusy( TypeActIP currentActIP ) {
    for (TypeActIP actIP : actIPList) {
      if ((actIP != currentActIP) && (currentActIP.adr == actIP.adr)) { //if in other Slot -> set busy
        actIP.adr = 0; //clean slot that informed as busy & let it activ
        //Inform with busy message:
        //not used!
      }
    }
  }

  //--------------------------------------------------------------------------------------------
//Add loco to slot. 
  void addBusySlot( InetAddress client, int adr ) {
    for (TypeActIP actIP : actIPList) {
      if (actIP.client == client) {
        if (actIP.adr != adr) {        //skip is already used by this client
          actIP.adr = adr;                //store loco that is used
          setOtherSlotBusy( actIP );        //make other busy
        }
        break;
      }
    }
  }

  //--------------------------------------------------------------------------------------------
//used by non Z21 client
  void reqLocoBusy( int adr ) {
    for (TypeActIP actIP : actIPList) {
      if (adr == actIP.adr) {
        actIP.adr = 0;        //clear
      }
    }
  }

  public boolean start() {
    long timeout = System.currentTimeMillis() + 5000;
    z21Connection = new UdpConnection( Z21_PORT, this );
    while (!z21Connection.isConnected()) {
      try {
        Thread.sleep( 50 );
      }
      catch (InterruptedException e) {
        // do nothing
      }
      if (System.currentTimeMillis() > timeout) {
        z21Connection.close();
        z21Connection = null;
        return false;
      }
    }
    z21Thread = new Thread( this, "Z21-Comm" );
    z21Thread.start();
    return true;
  }

  public void stop() {
    active = false;
    z21Thread.interrupt();
  }

  public void run() {
    Logger.info( "PI-Rail Z21Comm starting..." );
    active = true;
    setPower(csNormal);
    while (active) {
      try {
        Thread.sleep( 1000 );
      }
      catch (InterruptedException e) {
        // do nothing
      }
    }
  }

  public void send( InetAddress client, int z21Port, byte[] msgBytes ) throws IOException {
    if (client == null) {
      Set<InetAddress> broadcastIPs = new HashSet<>();
      for (InterfaceAddress myAdress : PiRailComm.getMyAddressList()) {
        InetAddress broadcastAddr = myAdress.getBroadcast();
        if (broadcastAddr != null) {
          broadcastIPs.add( broadcastAddr );
        }
      }
      for (InetAddress addr : broadcastIPs) {
        try {
          z21Connection.send( addr, z21Port, msgBytes );
        }
        catch (IOException e) {
          // ignore sync exceptions - interface may be offline
        }
      }
    }
    else {
      z21Connection.send( client, z21Port, msgBytes );
    }
  }
}
