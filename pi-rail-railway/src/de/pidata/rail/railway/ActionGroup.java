// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.rail.railway;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.DefaultModel;
import de.pidata.models.tree.ModelCollection;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.rail.model.Action;

import java.util.Collection;
import java.util.Hashtable;

public class ActionGroup extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://res.pirail.org/railway.xsd" );

  public static final QName ID_ID = NAMESPACE.getQName( "id" );
  public static final QName ID_RAILACTION = NAMESPACE.getQName( "railAction" );

  private final Collection<RailAction> railActions = new ModelCollection<>( ID_RAILACTION, this );

  public ActionGroup( Key id ) {
    super( id, RailwayFactory.ACTIONGROUP_TYPE, null, null, null );
  }

  public ActionGroup( Key key, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, RailwayFactory.ACTIONGROUP_TYPE, attributeNames, anyAttribs, childNames );
  }

  protected ActionGroup( Key key, ComplexType type, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, type, attributeNames, anyAttribs, childNames );
  }

  /**
   * Returns the attribute id.
   *
   * @return The attribute id
   */
  public QName getId() {
    return (QName) get( ID_ID );
  }

  /**
   * Returns the railAction element identified by the given key.
   *
   * @return the railAction element identified by the given key
   */
  public RailAction getRailAction( Key railActionID ) {
    return (RailAction) get( ID_RAILACTION, railActionID );
  }

  /**
   * Adds the railAction element.
   *
   * @param railAction the railAction element to add
   */
  public void addRailAction( RailAction railAction ) {
    add( ID_RAILACTION, railAction );
  }

  /**
   * Removes the railAction element.
   *
   * @param railAction the railAction element to remove
   */
  public void removeRailAction( RailAction railAction ) {
    remove( ID_RAILACTION, railAction );
  }

  /**
   * Returns the railAction iterator.
   *
   * @return the railAction iterator
   */
  public ModelIterator<RailAction> railActionIter() {
    return iterator( ID_RAILACTION, null );
  }

  /**
   * Returns the number of railActions.
   *
   * @return the number of railActions
   */
  public int railActionCount() {
    return childCount( ID_RAILACTION );
  }

  /**
   * Returns the railAction collection.
   *
   * @return the railAction collection
   */
  public Collection<RailAction> getRailActions() {
    return railActions;
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen

  @Override
  public String toString() {
    return "ActionGroup "+getId();
  }
}
