// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.rail.railway;

import de.pidata.models.tree.ChildList;
import de.pidata.models.tree.ModelCollection;
import de.pidata.models.tree.ModelIterator;
import de.pidata.models.tree.SimpleKey;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.rail.model.*;
import de.pidata.system.base.SystemManager;

import java.lang.String;
import java.util.Collection;
import java.util.Hashtable;

public class ProductCfg extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://res.pirail.org/railway.xsd" );

  public static final QName ID_DESCRIPTION = NAMESPACE.getQName( "description" );
  public static final QName ID_FUNC = NAMESPACE.getQName( "func" );
  public static final QName ID_ID = NAMESPACE.getQName( "id" );
  public static final QName ID_LOCALNAME = NAMESPACE.getQName( "localName" );
  public static final QName ID_MOTOR = NAMESPACE.getQName( "motor" );
  public static final QName ID_NAME = NAMESPACE.getQName( "name" );
  public static final QName ID_RANGE = NAMESPACE.getQName( "range" );
  public static final QName ID_SENSOR = NAMESPACE.getQName( "sensor" );
  public static final QName ID_TIMER = NAMESPACE.getQName( "timer" );
  public static final QName ID_TRIGGER = NAMESPACE.getQName( "trigger" );

  private final Collection<LocalName> localNames = new ModelCollection<>( ID_LOCALNAME, this );
  private final Collection<TimerAction> timers = new ModelCollection<>( ID_TIMER, this );
  private final Collection<MotorAction> motors = new ModelCollection<>( ID_MOTOR, this );
  private final Collection<EnumAction> funcs = new ModelCollection<>( ID_FUNC, this );
  private final Collection<RangeAction> ranges = new ModelCollection<>( ID_RANGE, this );
  private final Collection<TriggerAction> triggers = new ModelCollection<>( ID_TRIGGER, this );
  private final Collection<SensorAction> sensors = new ModelCollection<>( ID_SENSOR, this );

  public ProductCfg( Key id ) {
    super( id, RailwayFactory.PRODUCTCFG_TYPE, null, null, null );
  }

  public ProductCfg( Key key, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, RailwayFactory.PRODUCTCFG_TYPE, attributeNames, anyAttribs, childNames );
  }

  protected ProductCfg( Key key, ComplexType type, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, type, attributeNames, anyAttribs, childNames );
  }

  /**
   * Returns the attribute id.
   *
   * @return The attribute id
   */
  public QName getId() {
    return (QName) get( ID_ID );
  }

  /**
   * Returns the attribute name.
   *
   * @return The attribute name
   */
  public String getName() {
    return (String) get( ID_NAME );
  }

  /**
   * Set the attribute name.
   *
   * @param name new value for attribute name
   */
  public void setName( String name ) {
    set( ID_NAME, name );
  }

  /**
   * Returns the attribute description.
   *
   * @return The attribute description
   */
  public String getDescription() {
    return (String) get( ID_DESCRIPTION );
  }

  /**
   * Set the attribute description.
   *
   * @param description new value for attribute description
   */
  public void setDescription( String description ) {
    set( ID_DESCRIPTION, description );
  }

  /**
   * Returns the localName element identified by the given key.
   *
   * @return the localName element identified by the given key
   */
  public LocalName getLocalName( Key localNameID ) {
    return (LocalName) get( ID_LOCALNAME, localNameID );
  }

  /**
   * Adds the localName element.
   *
   * @param localName the localName element to add
   */
  public void addLocalName( LocalName localName ) {
    add( ID_LOCALNAME, localName );
  }

  /**
   * Removes the localName element.
   *
   * @param localName the localName element to remove
   */
  public void removeLocalName( LocalName localName ) {
    remove( ID_LOCALNAME, localName );
  }

  /**
   * Returns the localName iterator.
   *
   * @return the localName iterator
   */
  public ModelIterator<LocalName> localNameIter() {
    return iterator( ID_LOCALNAME, null );
  }

  /**
   * Returns the number of localNames.
   *
   * @return the number of localNames
   */
  public int localNameCount() {
    return childCount( ID_LOCALNAME );
  }

  /**
   * Returns the localName collection.
   *
   * @return the localName collection
   */
  public Collection<LocalName> getLocalNames() {
    return localNames;
  }

  /**
   * Returns the timer element identified by the given key.
   *
   * @return the timer element identified by the given key
   */
  public TimerAction getTimer( Key timerID ) {
    return (TimerAction) get( ID_TIMER, timerID );
  }

  /**
   * Adds the timer element.
   *
   * @param timer the timer element to add
   */
  public void addTimer( TimerAction timer ) {
    add( ID_TIMER, timer );
  }

  /**
   * Removes the timer element.
   *
   * @param timer the timer element to remove
   */
  public void removeTimer( TimerAction timer ) {
    remove( ID_TIMER, timer );
  }

  /**
   * Returns the timer iterator.
   *
   * @return the timer iterator
   */
  public ModelIterator<TimerAction> timerIter() {
    return iterator( ID_TIMER, null );
  }

  /**
   * Returns the number of timers.
   *
   * @return the number of timers
   */
  public int timerCount() {
    return childCount( ID_TIMER );
  }

  /**
   * Returns the timer collection.
   *
   * @return the timer collection
   */
  public Collection<TimerAction> getTimers() {
    return timers;
  }

  /**
   * Returns the motor element identified by the given key.
   *
   * @return the motor element identified by the given key
   */
  public MotorAction getMotor( Key motorID ) {
    return (MotorAction) get( ID_MOTOR, motorID );
  }

  /**
   * Adds the motor element.
   *
   * @param motor the motor element to add
   */
  public void addMotor( MotorAction motor ) {
    add( ID_MOTOR, motor );
  }

  /**
   * Removes the motor element.
   *
   * @param motor the motor element to remove
   */
  public void removeMotor( MotorAction motor ) {
    remove( ID_MOTOR, motor );
  }

  /**
   * Returns the motor iterator.
   *
   * @return the motor iterator
   */
  public ModelIterator<MotorAction> motorIter() {
    return iterator( ID_MOTOR, null );
  }

  /**
   * Returns the number of motors.
   *
   * @return the number of motors
   */
  public int motorCount() {
    return childCount( ID_MOTOR );
  }

  /**
   * Returns the motor collection.
   *
   * @return the motor collection
   */
  public Collection<MotorAction> getMotors() {
    return motors;
  }

  /**
   * Returns the func element identified by the given key.
   *
   * @return the func element identified by the given key
   */
  public EnumAction getFunc( Key funcID ) {
    return (EnumAction) get( ID_FUNC, funcID );
  }

  /**
   * Adds the func element.
   *
   * @param func the func element to add
   */
  public void addFunc( EnumAction func ) {
    add( ID_FUNC, func );
  }

  /**
   * Removes the func element.
   *
   * @param func the func element to remove
   */
  public void removeFunc( EnumAction func ) {
    remove( ID_FUNC, func );
  }

  /**
   * Returns the func iterator.
   *
   * @return the func iterator
   */
  public ModelIterator<EnumAction> funcIter() {
    return iterator( ID_FUNC, null );
  }

  /**
   * Returns the number of funcs.
   *
   * @return the number of funcs
   */
  public int funcCount() {
    return childCount( ID_FUNC );
  }

  /**
   * Returns the func collection.
   *
   * @return the func collection
   */
  public Collection<EnumAction> getFuncs() {
    return funcs;
  }

  /**
   * Returns the range element identified by the given key.
   *
   * @return the range element identified by the given key
   */
  public RangeAction getRange( Key rangeID ) {
    return (RangeAction) get( ID_RANGE, rangeID );
  }

  /**
   * Adds the range element.
   *
   * @param range the range element to add
   */
  public void addRange( RangeAction range ) {
    add( ID_RANGE, range );
  }

  /**
   * Removes the range element.
   *
   * @param range the range element to remove
   */
  public void removeRange( RangeAction range ) {
    remove( ID_RANGE, range );
  }

  /**
   * Returns the range iterator.
   *
   * @return the range iterator
   */
  public ModelIterator<RangeAction> rangeIter() {
    return iterator( ID_RANGE, null );
  }

  /**
   * Returns the number of ranges.
   *
   * @return the number of ranges
   */
  public int rangeCount() {
    return childCount( ID_RANGE );
  }

  /**
   * Returns the range collection.
   *
   * @return the range collection
   */
  public Collection<RangeAction> getRanges() {
    return ranges;
  }

  /**
   * Returns the trigger element identified by the given key.
   *
   * @return the trigger element identified by the given key
   */
  public TriggerAction getTrigger( Key triggerID ) {
    return (TriggerAction) get( ID_TRIGGER, triggerID );
  }

  /**
   * Adds the trigger element.
   *
   * @param trigger the trigger element to add
   */
  public void addTrigger( TriggerAction trigger ) {
    add( ID_TRIGGER, trigger );
  }

  /**
   * Removes the trigger element.
   *
   * @param trigger the trigger element to remove
   */
  public void removeTrigger( TriggerAction trigger ) {
    remove( ID_TRIGGER, trigger );
  }

  /**
   * Returns the trigger iterator.
   *
   * @return the trigger iterator
   */
  public ModelIterator<TriggerAction> triggerIter() {
    return iterator( ID_TRIGGER, null );
  }

  /**
   * Returns the number of triggers.
   *
   * @return the number of triggers
   */
  public int triggerCount() {
    return childCount( ID_TRIGGER );
  }

  /**
   * Returns the trigger collection.
   *
   * @return the trigger collection
   */
  public Collection<TriggerAction> getTriggers() {
    return triggers;
  }

  /**
   * Returns the sensor element identified by the given key.
   *
   * @return the sensor element identified by the given key
   */
  public SensorAction getSensor( Key sensorID ) {
    return (SensorAction) get( ID_SENSOR, sensorID );
  }

  /**
   * Adds the sensor element.
   *
   * @param sensor the sensor element to add
   */
  public void addSensor( SensorAction sensor ) {
    add( ID_SENSOR, sensor );
  }

  /**
   * Removes the sensor element.
   *
   * @param sensor the sensor element to remove
   */
  public void removeSensor( SensorAction sensor ) {
    remove( ID_SENSOR, sensor );
  }

  /**
   * Returns the sensor iterator.
   *
   * @return the sensor iterator
   */
  public ModelIterator<SensorAction> sensorIter() {
    return iterator( ID_SENSOR, null );
  }

  /**
   * Returns the number of sensors.
   *
   * @return the number of sensors
   */
  public int sensorCount() {
    return childCount( ID_SENSOR );
  }

  /**
   * Returns the sensor collection.
   *
   * @return the sensor collection
   */
  public Collection<SensorAction> getSensors() {
    return sensors;
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen

  public static final QName ID_IR_BALISE = PiRailFactory.NAMESPACE.getQName( "IR-Balise" );
  public static final QName ID_BLOCK = PiRailFactory.NAMESPACE.getQName( "Block" );

  public static ComplexType TRANSIENT_TYPE;
  public static final QName ID_DISPLAYNAME = NAMESPACE.getQName( "displayName" );

  @Override
  public ComplexType transientType() {
    if (TRANSIENT_TYPE == null) {
      DefaultComplexType type = new DefaultComplexType( NAMESPACE.getQName( "Product_Transient" ), RailDevice.class.getName(), 0, super.transientType() );
      TRANSIENT_TYPE = type;
      type.addAttributeType( ID_DISPLAYNAME, StringType.getDefString() );
    }
    return TRANSIENT_TYPE;
  }

  @Override
  public Object transientGet( int transientIndex ) {
    QName attributeName = transientType().getAttributeName( transientIndex );
    if (attributeName == ID_DISPLAYNAME) {
      String language = SystemManager.getInstance().getLocale().getLanguage();
      LocalName localName = getLocalName( new SimpleKey(language) );
      if (localName == null) {
        return getId();
      }
      else {
        return localName.getName();
      }
    }
    return super.transientGet( transientIndex );
  }
}
