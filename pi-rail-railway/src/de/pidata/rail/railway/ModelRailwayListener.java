package de.pidata.rail.railway;

import de.pidata.rail.model.ActionState;

/**
 * Used to notify attached clients about changes.
 */
public interface ModelRailwayListener {

  void stateChanged( RailAction changedAction, ActionState newState );

}
