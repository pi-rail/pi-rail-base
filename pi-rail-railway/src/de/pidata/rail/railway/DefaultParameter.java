// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.rail.railway;

import de.pidata.models.tree.ChildList;
import de.pidata.models.types.ComplexType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

import java.util.Hashtable;

public class DefaultParameter extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://res.pirail.org/railway.xsd" );

  public static final QName ID_NAME = NAMESPACE.getQName( "name" );

  public DefaultParameter() {
    super( null, RailwayFactory.DEFAULTPARAMETER_TYPE, null, null, null );
  }

  public DefaultParameter( Key key, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, RailwayFactory.DEFAULTPARAMETER_TYPE, attributeNames, anyAttribs, childNames );
  }

  protected DefaultParameter( Key key, ComplexType type, Object[] attributeNames, Hashtable<QName, Object> anyAttribs, ChildList childNames ) {
    super( key, type, attributeNames, anyAttribs, childNames );
  }

  /**
   * Returns the attribute name.
   *
   * @return The attribute name
   */
  public String getName() {
    return (String) get( ID_NAME );
  }

  /**
   * Set the attribute name.
   *
   * @param name new value for attribute name
   */
  public void setName( String name ) {
    set( ID_NAME, name );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen

  public DefaultParameter(String name) {
    this();
    setName( name );
  }
}
