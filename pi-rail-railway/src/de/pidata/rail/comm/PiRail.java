/*
 * This file is part of PI-Rail base (https://gitlab.com/pi-rail/pi-rail-base).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.comm;

import de.pidata.log.Logger;
import de.pidata.models.tree.EventListener;
import de.pidata.models.tree.EventSender;
import de.pidata.models.tree.ModelFactoryTable;
import de.pidata.qnames.QName;
import de.pidata.rail.model.*;
import de.pidata.rail.railway.*;
import de.pidata.rail.track.PiRailTrackFactory;
import de.pidata.rail.z21.Z21Processor;
import de.pidata.string.Helper;
import de.pidata.system.base.SystemManager;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Locale;
import java.util.Properties;

public class PiRail implements EventListener, Runnable {
  private byte nextAdress;

  private ModelRailway modelRailway;
  private boolean refreshing = false;
  private PiRailComm piRailComm;
  private PiRailCommZ21 piRailCommZ21;
  private Z21Processor z21Processor;
  private int displayRefreshInterval = 250;

  private static PiRail instance;

  public static synchronized PiRail getInstance() {
    if (instance == null) {
      instance = new PiRail();
    }
    return instance;
  }

  private PiRail() {
    //--- Check if strings for locale are available, if not switch to EN
    SystemManager sysMan = SystemManager.getInstance();
    String language =  sysMan.getProperty( "language", null );
    Locale locale;
    if (language != null) {
      locale = Locale.forLanguageTag( language );
      sysMan.setLocale( locale );
    }
    else {
      locale = sysMan.getLocale();
      language = locale.getLanguage();
    }
    Properties props = sysMan.getLanguageProps( "messages", language );
    if (props == null) {
      Logger.info( "Missing properties for '" + language + "' - switching to 'en'" );
      sysMan.setLocale( Locale.ENGLISH );
    }

    ModelFactoryTable.getInstance().getOrSetFactory( PiRailFactory.NAMESPACE, PiRailFactory.class );
    ModelFactoryTable.getInstance().getOrSetFactory( PiRailTrackFactory.NAMESPACE, PiRailTrackFactory.class );
    ModelFactoryTable.getInstance().getOrSetFactory( RailwayFactory.NAMESPACE, RailwayFactory.class );
    this.modelRailway = new ModelRailway();
    this.modelRailway.namespaceTable().addNamespace( PiRailFactory.NAMESPACE, "" );
    this.piRailComm = new PiRailComm( modelRailway );
    this.piRailCommZ21 = piRailComm.getPiRailCommZ21();
    try {
      Thread.sleep( 200 );
    }
    catch (InterruptedException e) {
      e.printStackTrace();
    }
    //createDummyModel();

    // Start Display state refresher
    new Thread( this, "Display-Refresher" ).start();
  }

  public PiRailComm getPiRailComm() {
    return piRailComm;
  }

  private RailDeviceAddress createAdress() {
    RailDeviceAddress address = null;
    try {
      address = new RailDeviceAddress( InetAddress.getByAddress( new
          byte[]{(byte) 192, (byte) 168, (byte) 188, nextAdress} ) );
      nextAdress++;
    }
    catch (UnknownHostException e) {
      e.printStackTrace();
    }
    return address;
  }

  public ModelRailway getModelRailway() {
    return modelRailway;
  }

  public boolean startZ21() {
    if (this.z21Processor == null) {
      this.z21Processor = new Z21Processor( modelRailway );
      if (!this.z21Processor.start()) {
        this.z21Processor = null;
        return false;
      }
    }
    return true;
  }

  public void close() {
    refreshing = false;
    piRailComm.stop();
    if (z21Processor != null) {
      z21Processor.stop();
    }
  }

  @Override
  public void run() {
    refreshing = true;
    long nextRefresh = 0;
    while (refreshing) {
      long now = System.currentTimeMillis();
      if (now >= nextRefresh) {
        for (Locomotive locomotive : modelRailway.locoIter()) {
          try {
            locomotive.processStateMsg();
            locomotive.refreshWlanIcon();
          }
          catch (Exception ex) {
            Logger.error( "Error while refreshing loco", ex );
          }
          try {
            // Wait until config files are loaded and loco is not offline
            if (locomotive.isConfigValid() && (locomotive.getWifiState() != WifiState.red)) {
              if ((locomotive.getCurrentSpeed() <= 0) && (locomotive.getSpeedLimitInt() <= 0)) {
                boolean sentMsg = false;
                //----- If loco halted on a track section which has a signal assigned for loco's dir,
                //      then notify waiting loco by sending new signal state to that loco
                RailBlock locoCurrentBlock = locomotive.getCurrentBlock();
                char locoDir = locomotive.getClockDir();
                if ((locoCurrentBlock != null) && (locoDir != MotorState.DIR_UNKNOWN.charAt( 0 ))) {
                  char signalState = locoCurrentBlock.getExitSignalState( locoDir );
                  if (RailFunction.isSignalFreeState( signalState )) {
                    Cmd command = new Cmd();
                    command.setMo( new MoCmd( locomotive.getMainMotor().getId(), signalState, -1, false ) );
                    PiRail.getInstance().sendCmd( locomotive, command );
                    sentMsg = true;
                  }
                }
                //----- If loco stopped at a Balise and that Balise changed it's command
                //      then notify waiting loco by sending new command to that loco (needs firmware >= 20231105)
                if (!sentMsg) {
                  QName posID = locomotive.getCurrentPosID();
                  if (posID != null) {
                    char lastCmd = locomotive.getLastCmd();
                    RailAction railMessage = PiRail.getInstance().getModelRailway().getRailMessage( posID );
                    if (railMessage != null) {
                      ActionState actionState = railMessage.getActionState();
                      if ((actionState != null) && (actionState.getCurChar() != lastCmd)) {
                        Cmd command = new Cmd( null );
                        command.setSet( new SetCmd( posID, actionState.getCur(), actionState.getCurI() ) );
                        PiRail.getInstance().sendCmd( locomotive, command );
                      }
                    }
                  }
                }
              }
            }
          }
          catch (Exception e) {
            Logger.error( "Error checking Loco resume", e );
          }
          try {
            Thread.sleep( 1 ); // allow processing of UDP messages
          }
          catch (InterruptedException e) {
            // do nothing
          }
          if (!refreshing) return;
        }
        for (SwitchBox switchBox : modelRailway.switchBoxIter()) {
          try {
            switchBox.processStateMsg();
            switchBox.refreshWlanIcon();
          }
          catch (Exception ex) {
            Logger.error( "Error while refreshing tower", ex );
          }
          try {
            Thread.sleep( 1 );  // allow processing of UDP messages
          }
          catch (InterruptedException e) {
            // do nothing
          }
          if (!refreshing) return;
        }
        nextRefresh = now + displayRefreshInterval;
      }
      long sleep = nextRefresh -System.currentTimeMillis();
      if (sleep > 0) {
        try {
          Thread.sleep( sleep );
        }
        catch (InterruptedException e) {
          // do nothing
        }
      }
      else {
        long duration = displayRefreshInterval - sleep;
        Logger.info( "Display-Thread took too long, ms="+duration );
      }
    }
  }

  public void addMessage( String message ) {
    Logger.info( message );
    /*if (foregroundActivity != null) {
      foregroundActivity.addStatusMessage( message );
    } */
  }

  //---------------------------------------------------------------------------------------------
  // Interface EventListener
  //---------------------------------------------------------------------------------------------

  /**
   * An event occurred.
   *
   * @param eventSender the sender of this event
   * @param eventID     the id of the event
   * @param source      the instance where the event occurred on, may be a child of eventSender
   * @param modelID     the id of the affected model
   * @param oldValue    the old value of the model
   * @param newValue    the new value of the model
   */
  @Override
  public void eventOccured( EventSender eventSender, int eventID, Object source, QName modelID, Object oldValue, Object newValue ) {
/*    if (source instanceof LocoState) {
      LocoState locoState = (LocoState) source;
      Locomotive loco = (Locomotive) locoState.getParent( false );
      if ((modelID == LocoState.ID_TARGET_SPEED_PERCENT) || (modelID == LocoState.ID_DIRECTION_FWD) || (modelID == LocoState.ID_DRIVING))  {
        xBeeConnector.sendData( loco, new LocoSetSpeed( loco, 0, locoState.calcTargetSpeed() ) );
      }
    }
//    else if (source instanceof LocoFunction) {
//      if (modelID == LocoFunction.ID_FUNCTION_ON) {
//        LocoFunction locoFunction = (LocoFunction) source;
//        Locomotive loco = (Locomotive) locoFunction.getParent( false ).getParent( false );
//        boolean value;
//        if (newValue == null) value = false;
//        else value = ((Boolean) newValue).booleanValue();
//        xBeeConnector.sendData( loco, new LocoSetFunction( loco, locoFunction.getNumber(), value ) );
//      }
//    }
    else{
      throw new RuntimeException( "TODO" );
    }
*/  }

  public QName getMyLockID() {
    return piRailComm.getMyLockID();
  }

  public void sendLockCommand( RailAction railAction, QName lockID, boolean apply ) {
    RailDevice railDevice = railAction.getRailDevice();
    if (railDevice == null) {
      Logger.info( "sendLockCommand (" + apply + ") to action " + railAction.getId().getName() );
      if (apply) {
        railAction.setLockID( lockID );
      }
      else {
        railAction.setLockID( null );
      }
    }
    else {
      Logger.info( "sendLockCommand (" + apply + ") on " + railAction.getRailDevice().getDisplayName() );
      CommType commType = railDevice.getCommType();
      if (commType == CommType.Z21) {
        State stateMsg = railDevice.getState();
        if ( stateMsg == null ) {
          stateMsg = new State();
          stateMsg.setId( railDevice.getId() );
        }
        stateMsg.setLocker( apply ? lockID : null );
        railDevice.setStateMsg( stateMsg );
      }
      else {
        QName itemConnID = railAction.getItemConnID();
        Cmd command = new Cmd( lockID );
        command.setLck( new LockCmd( itemConnID, apply ) );
        piRailComm.sendData( railDevice, command );
      }
    }
  }

  public void sendLockCommand( Locomotive locomotive, boolean apply ) {
    QName lockID = getMyLockID();
    sendLockCommand( locomotive.getMainMotor(), lockID, apply );
    if (apply) {
      RailAction autoDriveAction = locomotive.getAction( Locomotive.ACTION_ID_AUTODRIVE );
      if (autoDriveAction != null) {
        sendSetCommand( autoDriveAction, 'a', -1, null );
      }
    }
  }

  public void sendSetCommand( RailAction railAction, char value, int intValue, QName lockID ) {
    RailDevice railDevice = railAction.getRailDevice();
    if (railDevice == null) {
      // TrackAction: use ScriptRunner for processing in separate Thread
      new ScriptRunner( railAction, value, intValue, lockID );
    }
    else {
      if (lockID == null) {
        lockID = getMyLockID();
      }
      CommType commType = railDevice.getCommType();
      QName id = railAction.getId();
      String name = id.getName();
      Logger.info( "sendSetCommand(\"" + name + "\", \"" + value + "\", " + intValue + ") on " + railDevice.getDisplayName() );
      Cmd command = new Cmd( lockID );
      command.setSet( new SetCmd( railAction.getId(), "" + value, Integer.valueOf( intValue ) ) );

//      System.out.println("new set command");
//      PiRailComm.dumpCmdMessage( command );

      if (commType == CommType.Z21) {
        piRailCommZ21.addCmd( railDevice.getId(), command );
      }
      else {
        piRailComm.sendData( railDevice, command );
      }
    }
  }

  public void sendRangeCommand( RailRange railRange, Integer pos ) {
    RailDevice railDevice = railRange.getRailDevice();
    Cmd command = new Cmd( getMyLockID() );
    command.setSet( new SetCmd( railRange.getId(), null, pos ) );
    piRailComm.sendData( railDevice, command );
  }

  public void sendTriggerCommand( RailTrigger railTrigger, String value, int num ) {
    if (!Helper.isNullOrEmpty( value )) {
      RailDevice railDevice = railTrigger.getRailDevice();
      Cmd command = new Cmd( getMyLockID() );
      command.setSet( new SetCmd( railTrigger.getId(), value, Integer.valueOf( num ) ) );
      piRailComm.sendData( railDevice, command );
    }
  }

  public void sendLocoCommand( Locomotive locomotive, EnumAction function, char value ) {
    Logger.info( "sendLocoCommand(" + locomotive.getDisplayName() + ", " + function + ", " + value + ")" );
    CommType commType = locomotive.getCommType();
    if (commType == CommType.Z21) {
      Logger.info("Z21 ToDo: (sendLocoCommand)");
      // ToDo
    }
    else {
      Cmd command = new Cmd( getMyLockID() );
      command.setSet( new SetCmd( function.getId(), "" + value, null ) );
      piRailComm.sendData( locomotive, command );
    }
  }

  public void sendMotorSpeed( RailRange motor, char targetDir, short targetSpeed, boolean immediate ) {
    RailDevice locomotive = motor.getRailDevice();
    CommType commType = locomotive.getCommType();
    Cmd command = new Cmd( getMyLockID() );
    command.setMo( new MoCmd( motor.getId(), targetDir, targetSpeed, immediate ) );

//    System.out.println("new motor command");
//    PiRailComm.dumpCmdMessage( command );

    if (commType == CommType.Z21) {
      piRailCommZ21.addCmd( locomotive.getId(), command );
    }
    else {
      piRailComm.sendData( locomotive, command );
    }
  }

  public void sendSetCmd( RailDevice railDevice, QName setID, QName varID, Integer value ) {
    Logger.info( "sendSetCmd(" + railDevice.getDisplayName() + ", " + setID.getName() + ", " + varID.getName() + ", " + value + ")" );
    CommType commType = railDevice.getCommType();
    if (commType == CommType.Z21) {
      Logger.info("Z21 ToDo: (sendSetCmd)");
      // ToDo
    }
    else {
      Cmd command = new Cmd( getMyLockID() );
      command.setSet( new SetCmd( setID, varID, null, value ) );
      piRailComm.sendData( railDevice, command );
    }
  }

  public void sendCmd( RailDevice railDevice, Cmd command ) {
    piRailComm.sendData( railDevice, command );
  }
}
