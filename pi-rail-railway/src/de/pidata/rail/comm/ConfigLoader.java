/*
 * This file is part of PI-Rail base (https://gitlab.com/pi-rail/pi-rail-base).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.comm;

import de.pidata.gui.component.base.ComponentBitmap;
import de.pidata.gui.component.base.Platform;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.models.xml.binder.XmlReader;
import de.pidata.qnames.QName;
import de.pidata.rail.model.FileCfg;
import de.pidata.rail.model.FileDef;
import de.pidata.rail.railway.RailDevice;
import de.pidata.rail.railway.RailDeviceAddress;
import de.pidata.rail.track.Depot;
import de.pidata.rail.track.PiRailTrackFactory;
import de.pidata.rail.track.TrackCfg;
import de.pidata.stream.StreamHelper;
import de.pidata.system.base.SystemManager;

import java.io.*;
import java.net.*;

public class ConfigLoader implements Runnable {

  public static final int HTTP_TIMEOUT_MILLIS = 15000;
  
  public static final String CFG_XML = "cfg.xml";
  public static final String FILE_CFG_XML = "fileCfg.xml";
  public static final String IO_CFG_XML = "ioCfg.xml";
  public static final String NET_CFG_XML = "netCfg.xml";
  public static final String TRACK_CFG_XML = "trackCfg.xml";
  public static final String DEPOT_XML = "depot.xml";
  public static final String ICON_JPG = "icon.jpg";

  private boolean active = false;
  private Thread loaderThread;
  private InetAddress railDeviceAddress = null;
  private QName railDeviceID = null;
  private String filename;
  private Model configModel;
  private FileCfg fileCfg;
  private TrackCfg trackConfigModel;
  private Depot depotModel;
  private ComponentBitmap icon;
  private ConfigLoaderListener listener;
  private boolean tryIcon;
  private boolean tryTrackCfg;

  protected ConfigLoader( QName railDeviceId, InetAddress railDeviceAddress, String filename, ConfigLoaderListener listener, boolean tryIcon, boolean tryTrackCfg ) {
    this.railDeviceID = railDeviceId;
    this.railDeviceAddress = railDeviceAddress;
    this.filename = filename;
    this.listener = listener;
    this.tryIcon = tryIcon;
    this.tryTrackCfg = tryTrackCfg;
    this.loaderThread = new Thread( this, "Config-Loader" );
    this.loaderThread.start();
  }

  public InetAddress getRailDeviceAddress() {
    return railDeviceAddress;
  }

  public QName getRailDeviceID() {
    return railDeviceID;
  }

  public Model getConfigModel() {
    return configModel;
  }

  public ComponentBitmap getIcon() {
    return icon;
  }

  public TrackCfg getTrackConfigModel() {
    return trackConfigModel;
  }

  public Depot getDepotModel() {
    return depotModel;
  }

  private byte[] doDownload( RailDevice railDevice, String sourceName ) {
    RailDeviceAddress addr = railDevice.getAddress();
    if (addr != null) {
      InetAddress inetAddress = addr.getInetAddress();
      if (inetAddress != null) {
        String url = "http://" + inetAddress.getHostAddress() + "/" + sourceName;
        URI uri = URI.create( url );
        HttpURLConnection httpConnection = null;
        InputStream inStream = null;
        try {
          XmlReader xmlReader = new XmlReader();
          URL destination = uri.toURL();
          httpConnection = (HttpURLConnection) SystemManager.getInstance().getWifiManager().openWiFiConnection( destination );
          httpConnection.setRequestMethod( "GET" );
          httpConnection.setConnectTimeout( ConfigLoader.HTTP_TIMEOUT_MILLIS );
          httpConnection.setReadTimeout( ConfigLoader.HTTP_TIMEOUT_MILLIS );
          inStream = httpConnection.getInputStream();
          return StreamHelper.toBytes( inStream );
        }
        catch (IOException e) {
          e.printStackTrace();
        }
        finally {
          StreamHelper.close( inStream );
          if (httpConnection != null) {
            httpConnection.disconnect();
          }
        }
      }
    }
    return null;
  }

  public ComponentBitmap loadBitmap( InetAddress inetAddress, String sourceName ) {
    if (inetAddress != null) {
      String url = "http://" + inetAddress.getHostAddress() + "/" + sourceName;
      URI uri = URI.create( url );
      InputStream inStream = null;
      HttpURLConnection httpConnection = null;
      try {
        URL destination = uri.toURL();
        httpConnection = (HttpURLConnection) SystemManager.getInstance().getWifiManager().openWiFiConnection( destination );
        httpConnection.setRequestMethod( "GET" );
        httpConnection.setConnectTimeout( ConfigLoader.HTTP_TIMEOUT_MILLIS );
        httpConnection.setReadTimeout( ConfigLoader.HTTP_TIMEOUT_MILLIS );
        inStream = httpConnection.getInputStream();
        return Platform.getInstance().loadBitmap( inStream );
      }
      catch (FileNotFoundException e) {
        Logger.warn( "File not found, URL="+url );
      }
      catch (Exception e) {
        Logger.error( "Error loading file from URL="+url, e );
      }
      finally {
        StreamHelper.close( inStream );
        if (httpConnection != null) {
          httpConnection.disconnect();
        }
      }
    }
    return null;
  }

  public static int doUpload( InetAddress railDeviceAddress, CharSequence data, String targetFileName ) throws IOException {
    return doUpload( railDeviceAddress, data, null, targetFileName );
  }

  public static int doUpload( InetAddress railDeviceAddress, File file, String targetFileName ) throws IOException {
    return doUpload( railDeviceAddress, null, file, targetFileName );
  }

  protected static int doUpload( InetAddress inetAddress, CharSequence data, File file, String targetFileName ) throws IOException {
    int responseCode = -1;

    if (inetAddress != null && ( data != null || file != null )  && targetFileName != null) {
      String url = "http://" + inetAddress.getHostAddress() + "/fileUpload";

      final String boundary = Long.toHexString( System.currentTimeMillis() ); // Just generate some unique random value.
      final String charset = "UTF-8";
      final String CRLF = "\r\n"; // Line separator required by multipart/form-data.

      HttpURLConnection connection = null;
      OutputStream output = null;
      PrintWriter writer = null;
      try {
        URL destination = new URL( url );
        connection = (HttpURLConnection) SystemManager.getInstance().getWifiManager().openWiFiConnection( destination );
        connection.setDoOutput( true ); // implicitly sets method to POST
        connection.setRequestProperty( "Content-Type", "multipart/form-data; boundary=" + boundary );
        connection.setConnectTimeout( ConfigLoader.HTTP_TIMEOUT_MILLIS );
        connection.setReadTimeout( ConfigLoader.HTTP_TIMEOUT_MILLIS );
        output = connection.getOutputStream();
        writer = new PrintWriter( output );
        // Start of multipart/form-data.
        writer.append( "--" + boundary ).append( CRLF );
        writer.append( "Content-Disposition: form-data; name=\"file\"; filename=\"/" + targetFileName + "\"" ).append( CRLF );
        writer.append( "Content-Type: " + URLConnection.guessContentTypeFromName( targetFileName ) ).append( CRLF );
        if (data != null) {
          writer.append( CRLF ).flush();
          writer.append( data );
        }
        if (file != null) {
          writer.append( "Content-Transfer-Encoding: binary" ).append( CRLF );
          writer.append( CRLF ).flush();
          output.write( StreamHelper.toBytes( new FileInputStream( file ) ) );
        }
        output.flush(); // Important before continuing with writer!
        writer.append( CRLF ).flush(); // CRLF is important! It indicates end of boundary.

        // End of multipart/form-data.
        writer.append( "--" + boundary + "--" ).append( CRLF ).flush();

        // Request is lazily fired whenever you need to obtain information about response.
        responseCode = connection.getResponseCode();
      }
      finally {
        StreamHelper.close( writer );
        StreamHelper.close( output );
        if (connection != null) {
          connection.disconnect();
        }
      }
    }
    return responseCode;
  }

  private Model doLoadConfig( InetAddress inetAddress, String file, boolean optional ) {
    if (railDeviceAddress != null) {
      String url = "http://" + inetAddress.getHostAddress() + "/" + file;
      URI uri = URI.create( url );
      HttpURLConnection httpConnection = null;
      InputStream inStream = null;
      try {
        XmlReader xmlReader = new XmlReader();
        URL destination = uri.toURL();
        httpConnection = (HttpURLConnection) SystemManager.getInstance().getWifiManager().openWiFiConnection( destination );
        httpConnection.setRequestMethod( "GET" );
        httpConnection.setConnectTimeout( ConfigLoader.HTTP_TIMEOUT_MILLIS );
        httpConnection.setReadTimeout( ConfigLoader.HTTP_TIMEOUT_MILLIS );
        inStream = httpConnection.getInputStream();
        return xmlReader.loadData( inStream, null );
      }
      catch (FileNotFoundException fex) {
        if (!optional) {
          Logger.error( "Config file not found at " + url, fex );
        }
      }
      catch (IOException e) {
        Logger.error( "Error loading config from "+url, e );
      }
      finally {
        StreamHelper.close( inStream );
        if (httpConnection != null) {
          httpConnection.disconnect();
        }
      }
    }
    return null;
  }

  public static ConfigLoader loadConfig( QName railDeviceId, InetAddress railDeviceAddress, String file, ConfigLoaderListener listener, boolean  tryIcon, boolean tryTrackCfg ) {
    return new ConfigLoader( railDeviceId, railDeviceAddress, file, listener, tryIcon, tryTrackCfg );
  }

  public void stop() {
    this.active = false;
    this.loaderThread.interrupt();
  }

  @Override
  public void run() {
    active = true;
    try {
      configModel = doLoadConfig( railDeviceAddress, filename, false );
    }
    catch (Exception ex) {
      Logger.error( "Error loading "+filename+" for " + railDeviceAddress, ex );
    }
    boolean success = (configModel != null);
    if (active && success) {
      if (tryIcon) {
        try {
          icon = loadBitmap( railDeviceAddress, ICON_JPG );
        }
        catch (Exception ex) {
          Logger.error( "Error loading icon.jpg for " + railDeviceAddress );
        }
      }
      if (tryTrackCfg) {
        try {
          Model model = doLoadConfig( railDeviceAddress, FILE_CFG_XML, true );
          if (model instanceof FileCfg) {
            fileCfg = (FileCfg) model;
          }
          else if (model != null) {
            Logger.error( "Invalid model type for fileCfg: "+model.getClass().getName() );
          }
        }
        catch (Exception ex) {
          Logger.error( "Error loading fileCfg.xml for " + railDeviceAddress, ex );
        }
        for (FileDef fileDef : fileCfg.fileDefIter()) {
          String fileDefName = fileDef.getName();
          if (fileDefName.startsWith( "/" )) {
            fileDefName = fileDefName.substring( 1 );
          }
          if (fileDefName.equals( TRACK_CFG_XML ) && tryTrackCfg) {
            try {
              Model model = doLoadConfig( railDeviceAddress, TRACK_CFG_XML, true );
              if (model instanceof TrackCfg) {
                trackConfigModel = (TrackCfg) model;
              }
              else if (model != null) {
                Logger.error( "Invalid model type for trackCfg: " + model.getClass().getName() );
              }
            }
            catch (Exception ex) {
              Logger.error( "Error loading trackCfg.xml for " + railDeviceAddress, ex );
            }
          }
          else if (fileDefName.equals( DEPOT_XML ) && tryTrackCfg) {
            try {
              Model model = doLoadConfig( railDeviceAddress, DEPOT_XML, true );
              if (model instanceof Depot) {
                depotModel = (Depot) model;
              }
              else if (model != null) {
                Logger.error( "Invalid class for depot: " + model.getClass().getName() );
              }
            }
            catch (Exception ex) {
              Logger.error( "Error loading depot.xml for " + railDeviceAddress, ex );
            }
          }
          else if (fileDefName.equals( ICON_JPG )) {
            // skip
          }
          else if (fileDefName.toLowerCase().endsWith( ".jpg" ) || fileDefName.toLowerCase().endsWith( ".png" )) {
            try {
              ComponentBitmap bitmap = loadBitmap( railDeviceAddress, fileDefName );
              if (bitmap != null) {
                Platform.getInstance().addToImageCache( PiRailTrackFactory.NAMESPACE.getQName( fileDefName ), bitmap );
                Logger.info( "Loaded bitmap " + fileDefName + " from " + railDeviceAddress );
              }
            }
            catch (Exception ex) {
              Logger.error( "Error loading "+fileDefName+" from " + railDeviceAddress );
            }
          }
        }
      }
    }
    listener.finishedLoading( this, success );
    active = false;
  }
}
