/*
 * This file is part of PI-Rail base (https://gitlab.com/pi-rail/pi-rail-base).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.comm;

import de.pidata.connect.udp.InetReceiveListener;
import de.pidata.connect.udp.UdpConnection;
import de.pidata.gui.component.base.ComponentBitmap;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.models.types.simple.Binary;
import de.pidata.models.xml.binder.XmlReader;
import de.pidata.models.xml.binder.XmlWriter;
import de.pidata.qnames.QName;
import de.pidata.rail.model.*;
import de.pidata.rail.railway.*;
import de.pidata.rail.track.Depot;
import de.pidata.rail.track.TrackCfg;
import de.pidata.stream.CharSequenceInputStream;
import de.pidata.string.Helper;

import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.util.*;

@SuppressWarnings({"FieldMayBeFinal", "FieldCanBeLocal", "unused", "SuspiciousMethodCalls", "PatternVariableCanBeUsed"})
public class PiRailComm implements Runnable, InetReceiveListener, ConfigLoaderListener {

  private static final boolean DEBUG_SYNC = false;
  private static final boolean DEBUG_STATES = false;
  private static final boolean DEBUG_BROADCAST = false;

  public static final int MAX_LOG_LINE_LENGTH = 100;

  public static final short RAIL_CONTROLLER_PORT = 9750;
  public static final short RAIL_DEVICE_PORT = 9751;
  private static final int SYNC_MAX_MSG_STATE = 19;
  // For UDP size see https://stackoverflow.com/questions/14993000/the-most-reliable-and-efficient-udp-packet-size
  private static final int MAX_UDP_MSG_LENGTH = 1460;

  private static final long DEFAULT_BROADCAST_IP_SCAN_INTERVAL = 5 * 1000; // Rescan every minute

  private long broadcastScanInterval = DEFAULT_BROADCAST_IP_SCAN_INTERVAL; // may be adjusted on slow machines
  private long lastBrodcastIPScan = 0;

  private boolean active = false;
  private Thread commThread;
  private Thread z21CommThread;

  private static List<InterfaceAddress> myAddressList = Collections.emptyList();

  //-- Variables for sending sync messages - using snchronized(broadcastIPs) for changing
  private final Set<InetAddress> broadcastIPs = new HashSet<>();
  private String myAddress = null;
  private InetAddress syncDeviceAddr = null;
  private long nextSyncTime = 0;
  private int syncMaxMsgState = SYNC_MAX_MSG_STATE;
  private int slotMillis = 1;
  private int syncBlockMillis = 260;

  private UdpConnection udpDeviceConnection;
  private UdpConnection udpCtrlConnection;

  private final ModelRailway modelRailway;
  private PiRailCommZ21 piRailCommZ21;

  private final List<Cmd> xmlCommandList = new LinkedList<>();
  private final List<RailDevice> xmlDestinationList = new LinkedList<>();
  private final List<RailDevice> configLoadList = new LinkedList<>();
  private final Map<RailDevice,ConfigLoader> configLoaderMap = new HashMap<>();

  private byte[] oldMsgBytes = "<cmd xmlns=\"pi-rail-v1\"><syn startIP=\"0\" endIP=\"254\" msSlotSize=\"1\" msInterval=\"0\"/></cmd>".getBytes();

  public PiRailComm( ModelRailway modelRailway ) {
    this.modelRailway = modelRailway;
    this.piRailCommZ21 = new PiRailCommZ21( this );
    start();
  }

  public PiRailCommZ21 getPiRailCommZ21() {
    return piRailCommZ21;
  }

  public static List<InterfaceAddress> updateMyAddressList() throws SocketException {
    //Logger.info( "Start update myAddressList" );
    List<InterfaceAddress> addressList = new ArrayList<>();
    for (Enumeration<NetworkInterface> netEnum = NetworkInterface.getNetworkInterfaces(); netEnum.hasMoreElements(); ) {
      NetworkInterface net = netEnum.nextElement();
      if ((!net.isLoopback()) && (!net.isVirtual()) && net.isUp()) {
        for (InterfaceAddress ifAddr : net.getInterfaceAddresses()) {
          InetAddress addr = ifAddr.getAddress();
          if ((!addr.isAnyLocalAddress() && (addr.toString().length() <= 20))) {
            if (!addressList.contains( addr )) {
              addressList.add( ifAddr );
            }
          }
        }
      }
    }
    myAddressList = addressList;
    return myAddressList;
  }

  public static List<InterfaceAddress> getMyAddressList() {
    return myAddressList;
  }

  public boolean isMyAddress( InetAddress inetAddress ) throws SocketException {
    for (InterfaceAddress myIfAddr : getMyAddressList()) {
      if (myIfAddr.getAddress().equals( inetAddress )) {
        return true;
      }
    }
    return false;
  }

  public String getMyAddress() {
    if (myAddress == null) {
      try {
        Logger.info( "Start getMyAddress" );
        int indexFound = Integer.MAX_VALUE;
        byte[] myAddr = null;
        for (Enumeration<NetworkInterface> netEnum = NetworkInterface.getNetworkInterfaces(); netEnum.hasMoreElements(); ) {
          NetworkInterface net = netEnum.nextElement();
          if ((!net.isLoopback()) && (!net.isVirtual()) && net.isUp()) {
            byte[] hwAddr = net.getHardwareAddress();
            if ((hwAddr != null) && (net.getIndex() < indexFound)) {
              indexFound = net.getIndex();
              myAddr = hwAddr;
            }
          }
        }
        if (myAddr == null) {
          for (Enumeration<NetworkInterface> netEnum = NetworkInterface.getNetworkInterfaces(); netEnum.hasMoreElements(); ) {
            NetworkInterface net = netEnum.nextElement();
            if ((!net.isLoopback()) && (!net.isVirtual()) && net.isUp()) {
              for (Enumeration<InetAddress> addrEnum = net.getInetAddresses(); addrEnum.hasMoreElements(); ) {
                InetAddress addr = addrEnum.nextElement();
                if (!addr.isAnyLocalAddress() && !addr.isLinkLocalAddress()) {
                  String ipAddr = addr.toString();
                  if (ipAddr.length() <= 17) {
                    myAddress = ipAddr;
                    return myAddress;
                  }
                }
              }
            }
          }
        }
        else {
          StringBuilder builder = new StringBuilder();
          boolean first = true;
          for (byte b : myAddr) {
            if (first) {
              first = false;
            }
            else {
              builder.append( '-' );
            }
            String hexStr = Integer.toHexString( b );
            if (hexStr.length() >= 2) {
              builder.append( hexStr.substring( hexStr.length() - 2 ) );
            }
            else {
              builder.append( '0').append( hexStr );
            }
          }
          myAddress = builder.toString();
        }
      }
      catch (Exception ex) {
        Logger.error( "Error reading my network address", ex );
      }
    }
    return myAddress;
  }

  public QName getMyLockID() {
    String myAddr = getMyAddress();
    if (Helper.isNullOrEmpty( myAddr )) {
      return null;
    }
    else {
      return PiRailFactory.NAMESPACE.getQName( myAddr );
    }
  }

  /**
   * Starts a config loader job for RailDevice if config is not loaded.
   * Job will not be started if RailDevice's nextConfigLoadTime is in the future.
   * Hint: If load job is not successful, RailDevice sets its nextConfigLoadTime.
   * 
   * @param railDevice the railDevice for which config should be loaded
   */
  public void addLoadConfigJob( RailDevice railDevice ) {
    synchronized (configLoaderMap) {
      ConfigLoader loader = configLoaderMap.get( railDevice );
      if (loader == null) {
        if (!configLoadList.contains( railDevice )) {
          if (railDevice.getNextConfigLoadTime() < System.currentTimeMillis()) {
            configLoadList.add( railDevice );
          }
        }
      }
    }
  }

  public void sendData( RailDevice destination, Cmd command ) {
    if (active) {
      if (destination.getAddress() == null || destination.getAddress().getInetAddress() == null) {
        Logger.error("Trying to send data to unreachable device.");
        return;
      }
      synchronized (xmlCommandList) {
        this.xmlCommandList.add( command );
        this.xmlDestinationList.add( destination );
      }
    }
    else {
      Logger.error( "ERROR: PIRailComm not running" );
    }
  }

  @Override
  public void finishedLoading( ConfigLoader configLoader, boolean success ) {
    RailDevice railDevice = modelRailway.getRailDevice( configLoader.getRailDeviceID() );
    synchronized (configLoaderMap) {
      configLoaderMap.remove( railDevice );
    }
    if (success) {
      Model configModel = configLoader.getConfigModel();
      if (configModel instanceof Cfg) {
        railDevice.updateConfig( (Cfg) configModel );
      }
      else if (configModel instanceof NetCfg) {
        modelRailway.setNetCfg( (NetCfg) configModel );
      }
      ComponentBitmap icon = configLoader.getIcon();
      if (icon != null) {
        railDevice.setIcon( icon );
      }
      if (railDevice instanceof Locomotive) {
        ((Locomotive) railDevice).updateWagonImageData();
      }

      TrackCfg trackCfg = configLoader.getTrackConfigModel();
      if (trackCfg != null) {
        trackCfg.setDeviceAddress( configLoader.getRailDeviceAddress() );
        modelRailway.processTrackCfg( trackCfg, railDevice );
        railDevice.setTrackCfg( trackCfg );
        final Collection<Locomotive> locos = modelRailway.getLocos();
        for (Locomotive loco : locos) {
          if (loco.getCommType() == CommType.Z21) {
            final QName locoId = loco.getId();
            if (!piRailCommZ21.hasDevice(locoId)) {
              piRailCommZ21.addDevice( loco );
            }
          }
        }
      }
      Depot depot = configLoader.getDepotModel();
      if (depot != null) {
        modelRailway.processDepot( depot, railDevice );
      }
    }
    else {
      railDevice.configLoadError();
    }
    Logger.info( "Config loader finished success="+success+" for "+railDevice.getDisplayName() +" ("+ railDevice.getFirmwareSketch()+" v"+railDevice.getFirmwareVersion() + ")" );
  }

  public void start() {
    commThread = new Thread( this, "PI-Rail-Comm" );
    commThread.start();
    z21CommThread = new Thread( piRailCommZ21, "Pi-Rail-Comm-Z21" );
    z21CommThread.start();
  }

  public void stop() {
    active = false;
    piRailCommZ21.stop();
    z21CommThread.interrupt();
    commThread.interrupt();
    for (ConfigLoader configLoader : configLoaderMap.values()) {
      configLoader.stop();
    }
    configLoaderMap.clear();
  }

  public void refreshBroadcastIPs( boolean force ) {
    if (force || (lastBrodcastIPScan + broadcastScanInterval) < System.currentTimeMillis()) {
      synchronized (broadcastIPs) {
        long scanStartTime = System.currentTimeMillis();
        Set<InetAddress> newBroadCastIPs = new HashSet<>();
        try {
          updateMyAddressList();
          for (InterfaceAddress myAddress : getMyAddressList()) {
            InetAddress broadcastAddr = myAddress.getBroadcast();
            if (broadcastAddr != null) {
              if (!broadcastIPs.contains( broadcastAddr )) {
                Logger.info( "Adding broadcastIP=" + broadcastAddr );
              }
            }
            newBroadCastIPs.add( broadcastAddr );
          }
          lastBrodcastIPScan = System.currentTimeMillis();
          long scanTime = lastBrodcastIPScan - scanStartTime;
          if (scanTime > 10) {
            long newInterval = 30 * 1000;
            if (broadcastScanInterval < newInterval) {
              broadcastScanInterval = newInterval;
              Logger.info( "Scanning broadcastIPs took very long ms="+scanTime+" new scan interval is "+(broadcastScanInterval/1000)+" s" );
            }
          }
          else if ((scanTime < 5) && (broadcastScanInterval > DEFAULT_BROADCAST_IP_SCAN_INTERVAL)) {
            broadcastScanInterval = DEFAULT_BROADCAST_IP_SCAN_INTERVAL;
            Logger.info( "Scanning broadcastIPs is fast again (ms="+scanTime+") new scan interval is "+(broadcastScanInterval/1000)+" s" );
          }
          
          for (InetAddress addr : broadcastIPs) {
            if (!newBroadCastIPs.contains( addr )) {
              Logger.info( "Removing broadcastIP=" + addr );
            }
          }
          broadcastIPs.clear();
          broadcastIPs.addAll( newBroadCastIPs );
        }
        catch (Exception ex) {
          long scanTime = System.currentTimeMillis() - scanStartTime;
          Logger.error( "Error scanning broadcast IPs after ms="+scanTime );
        }
      }
    }
  }

  private void doSendSync( byte[] msgBytes ) {
    List<InetAddress> errorAddr = new ArrayList<>();
    synchronized (broadcastIPs) {
      refreshBroadcastIPs( false );
      for (InetAddress addr : broadcastIPs) {
        try {
          udpDeviceConnection.send( addr, RAIL_DEVICE_PORT, msgBytes );
          udpDeviceConnection.send( addr, RAIL_DEVICE_PORT, this.oldMsgBytes );
        }
        catch (Exception e) {
          Logger.error( "Error sending to broadcastIP=" + addr );
          // ignore sync exceptions - interface may be offline
          errorAddr.add( addr );
        }
      }
      for (InetAddress addr : errorAddr) {
        broadcastIPs.remove( addr );
        Logger.info( "Removed error broadcast IP="+addr );
      }
    }
  }

  private String doSendXMLData( RailDevice destination, Cmd message ) {
    try {
      XmlWriter xmlWriter = new XmlWriter( null );
      xmlWriter.setPrettyPrint( false );
      String msg = xmlWriter.writeXML( message, false ).toString();
      byte[] msgBytes = msg.getBytes();
      if (msgBytes.length > MAX_UDP_MSG_LENGTH) {
        String errMsg = "ERROR: UDP msg to large, size="+msgBytes.length+" msg="+msg.substring( 0, 100 ) +"...";
        Logger.error( errMsg );
        return errMsg;
      }
      if (destination == null) {
        doSendSync( msgBytes );
      }
      else {
        RailDeviceAddress address = destination.getAddress();
        if (address == null) {
          Logger.error("Cannot send XML data: " + msg);
        }
        else {
          InetAddress addr = address.getInetAddress();
          udpDeviceConnection.send( addr, RAIL_DEVICE_PORT, msgBytes );
          destination.setCmdSent( message );
        }
      }
      return msg;
    }
    catch (IOException e) {
      Logger.error( "Error sending XML cmd", e );
      return "ERROR: "+e.getMessage();
    }
  }

  private void doSendSyncData( Cmd message ) {
    XmlWriter xmlWriter = new XmlWriter( null );
    xmlWriter.setPrettyPrint( false );
    String msg = xmlWriter.writeXML( message, false ).toString();
    byte[] msgBytes = msg.getBytes();
    if (msgBytes.length > MAX_UDP_MSG_LENGTH) {
      syncMaxMsgState--;
      Logger.warn( "WARN: Sync msg to large ("+msgBytes.length+"), reducing syncMaxMsgState to "+syncMaxMsgState );
    }
    else {
      doSendSync( msgBytes );
    }
  }

  public void broadcastXMLState( State message ) {
    XmlWriter xmlWriter = new XmlWriter( null );
    xmlWriter.setPrettyPrint( false );
    String msg = xmlWriter.writeXML( message, false ).toString();
    byte[] msgBytes = msg.getBytes();
    if (DEBUG_BROADCAST) {
      Logger.info( "Z21 broadcastXMLState msg=" + msg );
    }
    synchronized (broadcastIPs) {
      refreshBroadcastIPs( false );
      List<InetAddress> errorAddr = new ArrayList<>();
      for (InetAddress broadcastAddr : broadcastIPs) {
        try {
          udpDeviceConnection.send( broadcastAddr, RAIL_CONTROLLER_PORT, msgBytes );
          if (DEBUG_BROADCAST) {
            Logger.info( "  Z21 sent broadcastXMLState to IP=" + broadcastAddr );
          }
        }
        catch (Exception e) {
          if (DEBUG_BROADCAST) {
            Logger.error( "  Z21 error sending broadcastXMLState to IP="+broadcastAddr, e );
          }
          errorAddr.add( broadcastAddr );
        }
      }
      if (DEBUG_BROADCAST) {
        Logger.info( "Z21 finished broadcastXMLState" );
      }
      for (InetAddress addr : errorAddr) {
        broadcastIPs.remove( errorAddr );
        Logger.info( "Removed error broadcast IP="+errorAddr );
      }
    }
  }

  @Override
  public void run() {
    active = true;
    Logger.info( "PI-Rail-Comm starting..." );
    udpDeviceConnection = new UdpConnection( RAIL_CONTROLLER_PORT, this );
    udpCtrlConnection = new UdpConnection( RAIL_DEVICE_PORT, this );
    while (!udpDeviceConnection.isActive()) {
      try {
        Thread.sleep( 50 );
      }
      catch (InterruptedException e) {
        if (!active) return;
      }
    }
    int nextSyncNum = 0;
    synchronized (broadcastIPs) {
      nextSyncTime = System.currentTimeMillis();
    }
    boolean netCfgNeeded = true;
    while (active) {

      //----- Load configs
      int loading = 0;
      synchronized (configLoaderMap) {
        try {
          while (!configLoadList.isEmpty()) {
            RailDevice railDevice = configLoadList.remove( 0 );
            State deviceState = railDevice.getState();
            if ((deviceState == null) || (deviceState.getType() == DeviceType.locomotive) || (deviceState.getType() == DeviceType.switchBox)) {
              ConfigLoader loader = configLoaderMap.get( railDevice );
              if (loader == null) {
                QName deviceId = railDevice.getId();
                InetAddress ipAddress = railDevice.getAddress().getInetAddress();
                if (railDevice instanceof Locomotive) {
                  loader = ConfigLoader.loadConfig( deviceId,ipAddress, ConfigLoader.CFG_XML, this, true, false );
                  if (netCfgNeeded) {
                    ConfigLoader.loadConfig( deviceId,ipAddress, ConfigLoader.NET_CFG_XML, this, false, false );
                    netCfgNeeded = false;
                  }
                }
                else if (railDevice instanceof SwitchBox) {
                  loader = ConfigLoader.loadConfig( deviceId,ipAddress, ConfigLoader.CFG_XML, this, false, true );
                  if (netCfgNeeded) {
                    ConfigLoader.loadConfig( deviceId,ipAddress, ConfigLoader.NET_CFG_XML, this, false, false );
                    netCfgNeeded = false;
                  }
                }
                else {
                  loader = ConfigLoader.loadConfig( deviceId,ipAddress, ConfigLoader.CFG_XML, this, false, false );
                }
                configLoaderMap.put( railDevice, loader );
              }
            }
            loading++;
          }
        }
        catch (Exception ex) {
          Logger.error( "Error processing config load list", ex );
        }
      }

      if (loading > 0) {
        int delay = 500 + (loading * 50);
        Logger.info( "Loading config count=" + loading + ", delay=" + delay );
        try {
          Thread.sleep( delay );
        }
        catch (InterruptedException e) {
          if (!active) return;
        }
      }

      //----- synchronize
      long time = System.currentTimeMillis();
      if (time >= nextSyncTime) {
        try {
          int syncLenMillis;
          int syncCount;
          short msInterval;
          long syncEndTime;
          boolean syncMaster;
          synchronized (broadcastIPs) {
            syncLenMillis = 2 * syncBlockMillis;
            syncCount = syncBlockMillis / slotMillis;
            msInterval = (short) (2 * syncBlockMillis * (254 / syncCount));
            syncEndTime = nextSyncTime + syncLenMillis;
            syncMaster = this.isSyncMaster();
          }
          //-- Send sync command if no other Sync master is known
          if (syncMaster && udpDeviceConnection.socketActive()) {
            Cmd command = new Cmd();
            int syncEndNum = nextSyncNum + syncCount;
            if (syncEndNum > 254) syncEndNum = 254;
            command.setSyn( new SyncCmd( slotMillis, msInterval, (short) nextSyncNum, (short) syncEndNum ) );
            int msgStateIndex = 0;
            while (msgStateIndex >= 0) {
              msgStateIndex = modelRailway.appendMsgStates( command, msgStateIndex, syncMaxMsgState );
              if (msgStateIndex >= 0) {
                doSendSyncData( command );
                command = new Cmd();
              }
            }
            doSendSyncData( command );
            if (DEBUG_SYNC) Logger.info( "Sent sync command" );
            syncEndTime = System.currentTimeMillis() + syncLenMillis; // recalculate - we may have lost time in synchronized block
            nextSyncNum = syncEndNum + 1;
            if (nextSyncNum > 254) {
              nextSyncNum = 0;
            }
          }
          //-- Wait estimated time to allow expected messages to arrived
          long delay = System.currentTimeMillis() - syncEndTime;
          if (delay > 0) {
            Thread.sleep( delay );
          }
        }
        catch (InterruptedException ie) {
          if (!active) return;
        }
        catch (Exception ex) {
          Logger.error( "Error sending sync message", ex );
        }

        //-- Calculate next sync time
        synchronized (broadcastIPs) {
          if (this.isSyncMaster()) {
            nextSyncTime = time + (4L * syncBlockMillis);
          }
          else {
            nextSyncTime = time + (21L * syncBlockMillis);
            syncDeviceAddr = null;
          }
        }
      }
      try {
        Thread.sleep( 50 );
      }
      catch (InterruptedException e) {
        // do nothing
      }

      //----- Send commands
      Cmd xmlCommand = null;
      RailDevice xmlDestination = null;
      synchronized (xmlCommandList) {
        if (!xmlCommandList.isEmpty()) {
          xmlCommand = xmlCommandList.remove( 0 );
          xmlDestination = xmlDestinationList.remove( 0 );
        }
      }
      if (xmlCommand != null) {
        try {
          String msg = doSendXMLData( xmlDestination, xmlCommand );
          Logger.info( "Sent to device " + xmlDestination + ": " + msg );
        }
        catch (Exception e) {
          Logger.error( "Error sending XML data", e );
        }
      }
    }
  }

  public void processMessage( RailDevice fromDevice, Model xmlMessage, long receiveTime ) {
    if (xmlMessage instanceof State) {
      State deviceState = (State) xmlMessage;
      deviceState.setReceiveTime( receiveTime );
      RailDevice railDevice = modelRailway.getRailDevice( deviceState.getId() );
      if (railDevice instanceof UnknownDevice) {
        modelRailway.removeUnknownDevice( (UnknownDevice) railDevice );
        railDevice = null;
      }
      if (railDevice == null) {
        railDevice = fromDevice;
        if (fromDevice instanceof Locomotive) {
          modelRailway.addLoco( (Locomotive) fromDevice );
          if (deviceState.getType() == DeviceType.locomotive) {
            addLoadConfigJob( railDevice );
          }
        }
        else if (fromDevice instanceof SwitchBox) {
          modelRailway.addSwitchBox( (SwitchBox) fromDevice );
          addLoadConfigJob( railDevice );
        }
        else {
          modelRailway.addUnknownDevice( (UnknownDevice) railDevice );
        }
      }
      else {
        checkConfigVersion( railDevice );
      }
      railDevice.setXmlProtocol( true );
      railDevice.setFirmwareSketch( deviceState.getFw() );
      railDevice.setFirmwareVersion( deviceState.getVer() );
      railDevice.setStateMsg( deviceState );
      if (deviceState.getType() == DeviceType.z21_loco) {
        if ( deviceState.getLocker() != null && deviceState.getLocker() != getMyLockID() ) {
          piRailCommZ21.removeDevice( deviceState.getId() );
        }
        railDevice.processStateMsg();
      }
    }
    else if (xmlMessage instanceof Data) {
      RailDevice railDevice = modelRailway.getRailDevice( fromDevice.getId() );
      if (railDevice != null ) {
        railDevice.processData( (Data) xmlMessage );
      }
    }
  }

  private void checkConfigVersion( RailDevice railDevice ) {
    if (!railDevice.isConfigValid()) {
      addLoadConfigJob( railDevice );
    }
  }

  @Override
  public void receivedData( InetAddress fromAddress, byte[] data, InetSocketAddress mySocketAddress, int replyPort ) throws IOException {
    long now = System.currentTimeMillis();
    String msgString = new String( data, 0, data.length );
    if (DEBUG_SYNC) {
      if (msgString.length() > MAX_LOG_LINE_LENGTH) {
        Logger.info( "-----From " + fromAddress + " msg=" + (msgString.substring( 0, MAX_LOG_LINE_LENGTH )) );
      }
      else {
        Logger.info( "-----From " + fromAddress + " msg=" + msgString );
      }
    }
    if (msgString.startsWith("<cmd xmlns=\"pi-rail-v1\"")) {
      // ignore old sync message
    }
    else if (msgString.startsWith( "<" )) { // begin of XML message
      InputStream in = new CharSequenceInputStream( msgString, 0, msgString.length() );
      Model xmlMessage;
      try {
        XmlReader reader = new XmlReader();
        xmlMessage = reader.loadData( in, null );
      }
      catch (Exception ex) {
        Logger.error( msgString, ex );
        // Parse error, e.g. due to old message format
        // no ID available here
        RailDevice railDevice = modelRailway.getRailDevice( fromAddress );
        if (railDevice == null) {
          QName id = ModelRailway.NAMESPACE.getQName( fromAddress.getHostAddress() );
          if ((msgString.startsWith("<loco")) || (msgString.contains( "ype=\"locomotive\"" ))) {
            Locomotive errorLoco = new Locomotive( id, fromAddress, "XML" );
            errorLoco.setName( State.NAMESPACE.getQName( "Error-Loco" ) );
            int fwPos = msgString.indexOf( "\" fw=\"" );
            if (fwPos > 0) {
              int endPos = msgString.indexOf( '"', fwPos+6 );
              if (endPos > 0) {
                errorLoco.setFirmwareSketch( msgString.substring( fwPos+6, endPos ));
              }
            }
            modelRailway.addLoco( errorLoco );
          }
          else if ((msgString.startsWith("<tower")) || (msgString.contains( "ype=\"switchBox\"" ))) {
            SwitchBox errorTower = new SwitchBox( id, fromAddress, "XML" );
            errorTower.setName( State.NAMESPACE.getQName( "Error-SwitchBox" ) );
            int fwPos = msgString.indexOf( "\" fw=\"" );
            if (fwPos > 0) {
              int endPos = msgString.indexOf( '"', fwPos+6 );
              if (endPos > 0) {
                errorTower.setFirmwareSketch( msgString.substring( fwPos+6, endPos ));
              }
            }
            modelRailway.addSwitchBox( errorTower );
          }
          else if (msgString.startsWith("<data")) {
            // ignore
          }
          else {
            railDevice = new UnknownDevice( id, fromAddress, "unknown" );
            modelRailway.addUnknownDevice( (UnknownDevice) railDevice );
          }
        }
        return;
      }
      if (xmlMessage instanceof Cmd) {
        Cmd cmd = (Cmd) xmlMessage;
        SyncCmd syncCmd = cmd.getSyn();
        if (syncCmd != null) {
          synchronized (broadcastIPs) {
            if (isMyAddress( fromAddress ) ) {
              syncDeviceAddr = null;
            }
            else {
              if (DEBUG_SYNC) {
                Logger.info( "Received SyncCmd from " + fromAddress );
              }
              syncDeviceAddr = fromAddress;
              this.nextSyncTime = now - 5;
              this.slotMillis = syncCmd.getMsSlotSizeInt();
              this.syncBlockMillis = 10 + (syncCmd.getEndIPInt() - syncCmd.getgetStartIPInt()) * slotMillis;
            }
          }
        }
      }
      else {
//        if ( xmlMessage instanceof State deviceState && Boolean.TRUE.equals( deviceState.getBridged() ) ) {
//          DeviceType deviceType = deviceState.getType();
//          QName deviceID = deviceState.getId();
//          Logger.info("State from bridged device: type = " + deviceType + ", ID = " + deviceID );
//          if (deviceType == DeviceType.locomotive) {
//            RailDevice railDevice = modelRailway.getLoco( deviceID );
//            if (railDevice == null) {
//              // ToDo: get device ... but how ???
//            }
//            if (railDevice != null) {
//              railDevice.setStateMsg( deviceState );
//            }
//          }
//        } else {
        RailDevice railDevice = null;
        if (xmlMessage instanceof State) {
          railDevice = modelRailway.getRailDevice( ((State) xmlMessage).getId() );
        }

        if ((railDevice == null) || (railDevice instanceof UnknownDevice)) {
          if (xmlMessage instanceof State) {
            DeviceType deviceType = ((State) xmlMessage).getType();
            QName deviceID = ((State) xmlMessage).getId();
            if (DeviceType.isLocomotive( deviceType )) {
              railDevice = new Locomotive( deviceID, fromAddress, "XML" );
            }
            else if (DeviceType.isSwitchBox( deviceType )) {
              railDevice = new SwitchBox( deviceID, fromAddress, "XML" );
            }
            else {
              Logger.error( "Unknown XML message: " + xmlMessage.getClass() );
              railDevice = new UnknownDevice( deviceID, fromAddress, "Unknown" );
              modelRailway.addUnknownDevice( (UnknownDevice) railDevice );
            }
          }
          else if (xmlMessage instanceof Data) {
            // data is skipped until loco or tower state has been received for fromAddress
            railDevice = modelRailway.getRailDevice( fromAddress );
          }
          else {
            Logger.error( "Unknown XML message: " + xmlMessage.getClass() );
            railDevice = new UnknownDevice( ModelRailway.NAMESPACE.getQName( fromAddress.getHostAddress() ), fromAddress, "Unknown" );
            modelRailway.addUnknownDevice( (UnknownDevice) railDevice );
          }
        }
        if (railDevice != null) {
          processMessage( railDevice, xmlMessage, now );
        }
//        }
      }
    }
    else {
      // not an XML message
      // no ID available here
      RailDevice railDevice = modelRailway.getRailDevice( fromAddress );
      if (railDevice == null) {
        railDevice = new UnknownDevice( ModelRailway.NAMESPACE.getQName( fromAddress.getHostAddress() ), fromAddress, "unknown" );
        modelRailway.addUnknownDevice( (UnknownDevice) railDevice );
      }
      if (railDevice instanceof UnknownDevice) {
        ((UnknownDevice) railDevice).receivedMsg( msgString );
      }
      else {
        Logger.warn( "Received invalid data from "+fromAddress+": "+msgString );
      }
    }
  }

  public boolean isSyncMaster() {
    synchronized (broadcastIPs) {
      return (syncDeviceAddr == null);
    }
  }

  public static State createStateMessageCloneWithTargetsSetToCurrent( State oldState ) {
    State stateMsg = (State) oldState.clone( null, true, false );
    for (MotorState motor : stateMsg.getSps()) {
      motor.setTgt( motor.getCur() );
      motor.setTgtI( motor.getCurI() );
    }
    for (ActionState act : stateMsg.getActs()) {
      act.setTgt( act.getCur() );
      act.setTgtI( act.getCurI() );
    }
    return stateMsg;
  }

  public static State createMotorStateMessage( RailDevice locomotive, RailRange motor, char targetDir, int targetSpeed ) {
    State oldState = locomotive.getState();
    State stateMsg = createStateMessageCloneWithTargetsSetToCurrent(oldState);

    if (DEBUG_STATES) {
      System.out.println();
      System.out.println( "Old state:" );
      dumpStateMessage( oldState );
    }

    final QName id = motor.getId();
    MotorState motorState = getMotorState( stateMsg, id );
    if (motorState != null) {
      if (DEBUG_STATES) {
        System.out.print( "Setting \"targetDir\" to \"" + targetDir + "\" and \"targetSpeed\" to \"" + targetSpeed + "\" ..." );
      }
      motorState.setTgtI( Integer.valueOf( targetSpeed ) );
      motorState.setTgt( String.valueOf( targetDir ) );
      OpMode op = targetSpeed != 0 ? OpMode.Drive : OpMode.Stop;
      motorState.setOp( op );
      if (DEBUG_STATES) {
        System.out.println( " done" );
      }
    }

    if (DEBUG_STATES) {
      System.out.println();
      System.out.println( "New state after changes:" );
      dumpStateMessage( stateMsg );
    }

    return stateMsg;
  }

  public static State createMotorDirMessage( RailDevice locomotive, RailRange motor, char targetDir ) {
    State oldState = locomotive.getState();
    State stateMsg = createStateMessageCloneWithTargetsSetToCurrent(oldState);

    if (DEBUG_STATES) {
      System.out.println();
      System.out.println( "Old state:" );
      dumpStateMessage( oldState );
    }

    final QName id = motor.getId();
    MotorState motorState = getMotorState( stateMsg, id );
    if (motorState != null) {
      if (DEBUG_STATES) {
        System.out.print( "Setting \"targetDir\" to \"" + targetDir + "\" ..." );
      }
      motorState.setTgt( String.valueOf( targetDir ) );
      if (DEBUG_STATES) {
        System.out.println( " done" );
      }
    }

    if (DEBUG_STATES) {
      System.out.println();
      System.out.println( "New state after changes:" );
      dumpStateMessage( stateMsg );
    }

    return stateMsg;
  }

  public static State createFunctionStateMessage( RailDevice locomotive, String actionName, char target ) {
    State oldState = locomotive.getState();
    State stateMsg = createStateMessageCloneWithTargetsSetToCurrent(oldState);

    if (DEBUG_STATES) {
      System.out.println();
      System.out.println( "Old state:" );
      dumpStateMessage( oldState );
    }

    ActionState actionState = getActionState( stateMsg, actionName);
    if (actionState != null) {
      final String targetString = String.valueOf( target );
      if (DEBUG_STATES) {
        System.out.print( "Setting \"" + actionName + "\" to \"" + targetString + "\" ..." );
      }
      actionState.setTgt( targetString );
      actionState.setTgtI( Integer.decode( targetString ) );
      if (DEBUG_STATES) {
        System.out.println( " done" );
      }
    }

    if (DEBUG_STATES) {
      System.out.println();
      System.out.println( "New state after changes:" );
      dumpStateMessage( stateMsg );
    }

    return stateMsg;
  }

  public static MotorState getMotorState( State stateMsg, QName id ) {
    if (stateMsg != null) {
      for (MotorState motorState : stateMsg.getSps()) {
        if (motorState.getId() == id) {
          return motorState;
        }
      }
    }
    return null;
  }

  public static ActionState getActionState( State stateMsg, String actionName ) {
    if (stateMsg != null) {
      Collection<ActionState> stateActs = stateMsg.getActs();
      for (ActionState actionState : stateActs) {
        if (actionName.equals( actionState.getId().getName() )) {
          return actionState;
        }
      }
    }
    return null;
  }

  public static void dumpStateMessage( State stateMsg ) {
    XmlWriter xmlWriter = new XmlWriter( null );
    xmlWriter.setPrettyPrint( true );
    String msg = xmlWriter.writeXML( stateMsg, false ).toString();
    System.out.println( msg );
  }

  public static void dumpCmdMessage( Cmd cmd ) {
    XmlWriter xmlWriter = new XmlWriter( null );
    xmlWriter.setPrettyPrint( true );
    String msg = xmlWriter.writeXML( cmd, false ).toString();
    System.out.println( msg );
  }
}
