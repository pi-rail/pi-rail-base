package de.pidata.rail.comm;

import de.pidata.log.Logger;
import de.pidata.qnames.QName;
import de.pidata.rail.model.*;
import de.pidata.rail.railway.Locomotive;
import de.pidata.rail.railway.RailDevice;
import de.pidata.rail.railway.RailFunction;
import de.pidata.rail.railway.RailRange;
import z21Drive.LocoAddressOutOfRangeException;
import z21Drive.Z21;
import z21Drive.actions.Z21Action;
import z21Drive.actions.Z21ActionGetLocoInfo;
import z21Drive.actions.Z21ActionSetLocoDrive;
import z21Drive.actions.Z21ActionSetLocoFunction;
import z21Drive.broadcasts.BroadcastTypes;
import z21Drive.broadcasts.Z21Broadcast;
import z21Drive.broadcasts.Z21BroadcastLanXLocoInfo;
import z21Drive.broadcasts.Z21BroadcastListener;

import java.net.InetAddress;
import java.util.*;

import static z21Drive.broadcasts.Z21BroadcastLanXLocoInfo.FUNCTION_COUNT;

/**
 * Sends regular status broadcasts for bridged devices.
 */
public class PiRailCommZ21 implements Runnable {
  private static final boolean DEBUG_CMDS = true;
  private static final boolean DEBUG_LOCO_INFO = true;

  private Map<QName, DeviceData> devices = new HashMap<>();
  private PiRailComm piRailComm;
  private boolean running = false;

  private List<InetAddress> initializedZ21Addresses = new ArrayList<>();

  private static class DeviceData {
    RailDevice railDevice;
    List<Cmd> cmdQueue;
    long lastBroadcast;
    int locoAddress;

    public DeviceData( RailDevice railDevice ) {
      this.railDevice = railDevice;
      this.cmdQueue = new ArrayList<>();
      this.lastBroadcast = 0;
      this.locoAddress = getLocoAddress( railDevice );
    }
  }

  public PiRailCommZ21( PiRailComm piRailComm ) {
    this.piRailComm = piRailComm;
  }

  public synchronized void addDevice( RailDevice railDevice ) {
    try {
      Z21 z21 = railDevice.getZ21();
      if (z21 == null) {
        Logger.info( "Cannot init Z21 on " + railDevice.getDisplayName() + " msg=" + railDevice.getZ21Error() );
      }
      else {
        initZ21( z21 );
        devices.put( railDevice.getId(), new DeviceData( railDevice ) );
        if (DEBUG_CMDS) {
          System.out.println( "added device: \"" + railDevice.getDisplayName() + "\"" );
          State currentState = railDevice.getState();
          if (currentState == null) {
            System.out.println( "HAS NO STATE !!!" );
          }
        }
        railDevice.getZ21().sendActionToZ21( new Z21ActionGetLocoInfo( railDevice.getZ21(), getLocoAddress( railDevice ) ) );
      }
    }
    catch (Exception e) {
      Logger.error( "Error sending Z21 commands for \"" + railDevice.getDisplayName() + "\"", e );
    }
  }

  public synchronized boolean hasDevice( QName id ) {
    return devices.get( id ) != null;
  }

  public synchronized void addCmd( QName id, Cmd cmdNew ) {
    DeviceData deviceData = devices.get( id );
    if (deviceData != null) {
      MoCmd moCmdNew = cmdNew.getMo();
      SetCmd setCmdNew = cmdNew.getSet();
      LockCmd lckCmdNew = cmdNew.getLck();

      // let new cmd's kick out corresponding old ones
      Iterator<Cmd> cmdIterator = deviceData.cmdQueue.iterator();
      while (cmdIterator.hasNext()) {
        Cmd cmdOld = cmdIterator.next();
        MoCmd moCmdOld = cmdOld.getMo();
        SetCmd setCmdOld = cmdOld.getSet();
        LockCmd lckCmdOld = cmdOld.getLck();
        if ( (moCmdOld != null && moCmdNew != null && moCmdOld.getId() == moCmdNew.getId())
             ||
             (setCmdOld != null && setCmdNew != null && setCmdOld.getId() == setCmdNew.getId())
             ||
             (lckCmdOld != null && lckCmdNew != null && lckCmdOld.getId() == lckCmdNew.getId()) ) {
          cmdIterator.remove();
        }
      }

      MoCmd moCmd = cmdNew.getMo();
      SetCmd setCmd = cmdNew.getSet();
      LockCmd lckCmd = cmdNew.getLck();

      RailDevice railDevice = deviceData.railDevice;
      State currentState = railDevice.getState();
      boolean doAdd = false;

      if (moCmd != null) {
        RailRange mainMotor = ((Locomotive) railDevice).getMainMotor();
        QName mainMotorId = mainMotor.getId();
        MotorState currentMotorState = PiRailComm.getMotorState( currentState, mainMotorId );
        String motorStateCur = currentMotorState.getCur();
        Integer motorStateCurI = currentMotorState.getCurI();
        Integer cmdSpeed = moCmd.getSpeed();
        String cmdDir = moCmd.getDir();
        if (!Objects.equals( motorStateCur, cmdDir ) || !Objects.equals( motorStateCurI, cmdSpeed )) {
          currentMotorState.setTgt( cmdDir );
          currentMotorState.setTgtI( cmdSpeed );
          doAdd = true;
        }
      }

      if (setCmd != null) {
        String setCmdName = setCmd.getId().getName();
        String cmdValue = setCmd.getValue();
        ActionState actionState = PiRailComm.getActionState( currentState, setCmdName );
        if (!cmdValue.equals( actionState.getCur() )) {
          actionState.setTgt( cmdValue );
          doAdd = true;
        }
      }

      if (lckCmd != null) {
        // ToDo: muss man da überhaupt was machen ?
      }

      if (doAdd) {
        deviceData.cmdQueue.add( cmdNew );
        if (DEBUG_CMDS) {
          System.out.println();
          System.out.println( "Added cmd:" );
          PiRailComm.dumpCmdMessage( cmdNew );
        }
      }
      else {
        if (DEBUG_CMDS) {
          System.out.println();
          System.out.println( "Discarded cmd:" );
          PiRailComm.dumpCmdMessage( cmdNew );
        }
      }
    }
  }

  public synchronized void removeDevice( QName stateToRemoveId ) {
    if (stateToRemoveId != null) {
      devices.remove( stateToRemoveId );
    }
  }

  public void stop() {
    running = false;
  }

  @Override
  public void run() {
    running = true;
    while ( running ) {
      int sleepTime = 100;
      Set<Map.Entry<QName, DeviceData>> entries;
      synchronized (this) {
        entries = devices.entrySet();
      }

      for (Map.Entry<QName, DeviceData> entry : entries) {
        try {
          DeviceData deviceData = entry.getValue();
          RailDevice railDevice = deviceData.railDevice;
          State currentState = railDevice.getState();
          int locoAddress = deviceData.locoAddress;
          List<Z21Action> actionList = new ArrayList<>();
          Z21 railDeviceZ21 = railDevice.getZ21();

          if ((railDeviceZ21 != null) && (locoAddress >= 0) && (!deviceData.cmdQueue.isEmpty())) {
            Iterator<Cmd> cmdIterator = deviceData.cmdQueue.iterator();
            while (cmdIterator.hasNext()) {
              Cmd cmd = cmdIterator.next();
              MoCmd moCmd = cmd.getMo();
              SetCmd setCmd = cmd.getSet();
              LockCmd lckCmd = cmd.getLck();

              if (moCmd != null) {
                int speedStepsID = 3; // ToDo: was ist das und muss man das flexibel setzen ?
                String  dir = moCmd.getDir();
                int tgtInt = moCmd.getSpeedInt();
                boolean z21Direction = ( dir.indexOf(MotorState.CHAR_FORWARD) == 0 );
                int z21TgtSpeed = calcZ21TargetSpeed( tgtInt );
                if (DEBUG_CMDS) {
                  System.out.println( "Creating Z21 <" + railDeviceZ21 + "> action \"set loco drive (" + z21TgtSpeed + ", " + speedStepsID + ", " + z21Direction + ")\", calculated from tgtInt=" + tgtInt + ", dir=" + dir);
                }
                try {
                    Z21ActionSetLocoDrive locoDrive = new Z21ActionSetLocoDrive( railDeviceZ21, locoAddress, z21TgtSpeed, speedStepsID, z21Direction );
                    actionList.add( locoDrive );
                }
                catch (LocoAddressOutOfRangeException e) {
                  Logger.error( "Error setting motor speed and dir on " + railDevice.getDisplayName(), e );
                }
              }

              if (setCmd != null) {
                String setCmdName = setCmd.getId().getName();
                RailFunction railFunction = ((Locomotive) railDevice).getFunction( setCmd.getId() );
                if (railFunction != null) {
                  ItemConn itemConn = railFunction.getItemConn();
                  StateScript stateScript = railFunction.getStateScript( setCmd.getValue().charAt( 0 ) );
                  if (stateScript != null) {
                    for (SetIO setIO : stateScript.setIOIter()) {
                      OutPin outPin = itemConn.getOutPin( setIO.getPinID() );
                      if (outPin != null) {
                        OutCfg outCfg = itemConn.getCfg().findOutCfg( outPin.getIoID() );
                        if (outCfg != null) {
                          boolean on = (setIO.getPinValueInt() != 0);
                          int functionNo = outCfg.getPinInt();
                          if (DEBUG_CMDS) {
                            System.out.println( "Creating Z21 action \"set loco function (" + functionNo + ", " + on + ")\", calculated from value=" + on );
                          }
                          try {
                            Z21ActionSetLocoFunction locoFunction = new Z21ActionSetLocoFunction( railDeviceZ21, locoAddress, functionNo, on );
                            actionList.add( locoFunction );
                          }
                          catch (LocoAddressOutOfRangeException e) {
                            Logger.error( "Error switching \"" + setCmdName + "\" to \"" + on + "\" on " + railDevice.getDisplayName(), e );
                          }
                        }
                      }
                    }
                  }
                }
              }

              if (lckCmd != null) {
                // ToDo: muss man da überhaupt was machen ?
              }
            }
          }

          // check if there is anything to do
          if (actionList.isEmpty()) {
            long now = System.currentTimeMillis();
            if ( deviceData.lastBroadcast == 0 || deviceData.lastBroadcast + 1000 < now ) {
              deviceData.lastBroadcast = now;
              if (currentState != null) {
                piRailComm.broadcastXMLState( (State) currentState.clone( null, true, false ) );
              }
            }
          }
          else {
            try {
              for (Z21Action z21Action : actionList) {
                z21Action.getZ21().sendActionToZ21( z21Action );
              }
              railDeviceZ21.sendActionToZ21( new Z21ActionGetLocoInfo( railDeviceZ21, locoAddress ) );
            }
            catch (LocoAddressOutOfRangeException e) {
              Logger.error( "Error sending Z21 commands for \"" + railDevice.getDisplayName() + "\"", e );
            }

            sleepTime = 350; // wait a bit longer to give network communication a better chance to complete before next loop
            // no status message broadcast this time ... will be done on Z21 reply to GetLocoInfo
          }
        }
        catch (Exception ex) {
          // catch all Exceptions here, because the Thread must be kept running at all costs
          Logger.error( "Exception occurred in Z21 Thread:", ex );
        }
      }

      try {
        Thread.sleep( sleepTime );
      }
      catch (InterruptedException e) {
        // NOP
      }
    }
  }

  private void initZ21( final Z21 z21 ) {
    final InetAddress z21Address = z21.getZ21Address();
    if (initializedZ21Addresses.contains( z21Address )) {
      return;
    }

    initializedZ21Addresses.add( z21Address );
    System.out.println( "Attaching broadcast listener to " + z21 );
    z21.addBroadcastListener( new Z21BroadcastListener() {
      @Override
      public void onBroadCast( BroadcastTypes type, Z21Broadcast broadcast ) {
        if (type == BroadcastTypes.LAN_X_LOCO_INFO) {
          Z21BroadcastLanXLocoInfo bc = (Z21BroadcastLanXLocoInfo) broadcast;
          int locoAddress = bc.getLocoAddress();
          boolean lightOnResponse = bc.getFunctionState(0);
          boolean directionResponse = bc.getDirection();
          int speedResponse = bc.getSpeed();

          if (DEBUG_LOCO_INFO || DEBUG_CMDS) {
            System.out.println();
            System.out.println("Response from " + z21 + ":");
          }
          if (DEBUG_LOCO_INFO) {
            System.out.println( "Loco address: " + locoAddress );
            System.out.println( "Lights: " + lightOnResponse );
            System.out.println( "All Functions: ");
            for (int i = 0; i < FUNCTION_COUNT; i ++) {
              System.out.println( "* F" + i + ": " + bc.getFunctionState(i));
            }
            System.out.println( "Speed steps: " + bc.getSpeedSteps() );
            System.out.println( "Direction: " + directionResponse );
            System.out.println( "Speed: " + speedResponse );
            System.out.println( "Raw data:" );
            for (byte b : bc.getByteRepresentation())
              System.out.print( b + " " );
            System.out.print( "\n" );
            System.out.println( "Array length: " + bc.getByteRepresentation().length );
          }

          final Collection<DeviceData> deviceDataCollection = devices.values();
          if (!deviceDataCollection.isEmpty()) {
            for (DeviceData deviceData : deviceDataCollection) {
              if (deviceData.locoAddress == locoAddress) {
                RailDevice railDevice = deviceData.railDevice;
                // set data in current state to prevent unnecessarily sending command again because network timing of status message broadcast
                final State currentState = railDevice.getState();

                // check and clear command queue: every command with same values as Z21 response can be removed
                Iterator<Cmd> cmdIterator = deviceData.cmdQueue.iterator();
                while (cmdIterator.hasNext()) {
                  Cmd cmd = cmdIterator.next();
                  MoCmd moCmd = cmd.getMo();
                  SetCmd setCmd = cmd.getSet();
                  LockCmd lckCmd = cmd.getLck();

                  if (moCmd != null) {
                    String  cmdDir = moCmd.getDir();
                    int cmdSpeed = moCmd.getSpeedInt();
                    boolean z21Dir = ( cmdDir.indexOf(MotorState.CHAR_FORWARD) == 0 );
                    // calculate again and compare result in order to compensate rounding errors
                    int z21Speed = calcZ21TargetSpeed( cmdSpeed );
                    if (z21Dir == directionResponse && z21Speed == speedResponse) {
                      if (DEBUG_CMDS) {
                        System.out.println("Removing motor cmd from queue.");
                      }
                      cmdIterator.remove();
                      continue;
                    } else {
                      System.out.println("Speed/Direction mismatch: cmdSpeed=" + cmdSpeed
                          + ", z21Speed=" + z21Speed
                          + ", speedResponse=" + speedResponse
                          + ", cmdDir=" + cmdDir
                          + ", z21Dir=" + z21Dir
                          + ", directionResponse=" + directionResponse
                      );
                    }
                  }

                  if (setCmd != null) {
                    String setCmdName = setCmd.getId().getName();
                    if ("Light".equals( setCmdName )) {
                      String value = setCmd.getValue();
                      boolean on = ("1".equals( value ));
                      if (on == lightOnResponse) {
                        if (DEBUG_CMDS) {
                          System.out.println("Removing set cmd from queue.");
                        }
                        cmdIterator.remove();
                        continue;
                      }
                    }
                  }

                  if (lckCmd != null) {
                    // ToDo: muss man da überhaupt was machen ?
                  }
                }

                // update current state for broadcast: Light
                ActionState lightState = PiRailComm.getActionState( currentState, "Light" );
                if (lightOnResponse != (lightState.getCurChar() == '1')) {
                  String curString = lightOnResponse ? "1" : "0";
                  lightState.setCur( curString );
                  if (DEBUG_CMDS) {
                    System.out.println( "Set current Light to " + curString );
                  }
                }

                // update current state for broadcast: Motor
                if (railDevice instanceof Locomotive) {
                  RailRange mainMotor = ((Locomotive) railDevice).getMainMotor();
                  QName mainMotorId = mainMotor.getId();
                  MotorState currentMotorState = PiRailComm.getMotorState( currentState, mainMotorId );
                  if (currentMotorState != null) {
                    String z21Direction = directionResponse ? "+" : "-";
                    currentMotorState.setCur( z21Direction );
                    if (DEBUG_CMDS) {
                      System.out.println( "Set current Motor Dir to " + z21Direction );
                    }
                    int stateTgtSpeed = currentMotorState.getTgtInt();
                    // calculate again and compare result in order to compensate rounding errors
                    int z21Speed = calcZ21TargetSpeed( stateTgtSpeed );
                    System.out.println("Comparing speed: stateTgtSpeed=" + stateTgtSpeed + ", calculated=" + z21Speed + ", response=" + speedResponse);
                    if (z21Speed == speedResponse) {
                      currentMotorState.setCurI( Integer.valueOf( stateTgtSpeed ) );
                      final OpMode opMode = stateTgtSpeed != 0 ? OpMode.Drive : OpMode.Stop;
                      currentMotorState.setOp( opMode );
                      if (DEBUG_CMDS) {
                        System.out.println( "Set current Motor Speed to " + stateTgtSpeed + ", opMode to " + opMode );
                      }
                    }
                    else {
                      // must be a different speed, calculate backwards
                      int curI = calcZ21TargetSpeedBackwards( speedResponse );
                      currentMotorState.setCurI( Integer.valueOf( curI ) );
                      final OpMode opMode = curI != 0 ? OpMode.Drive : OpMode.Stop;
                      currentMotorState.setOp( opMode );
                      if (DEBUG_CMDS) {
                        System.out.println( "Set current Motor Speed to " + curI + " calculated from " + speedResponse+ ", opMode to " + opMode );
                      }
                    }
                  }
                }

                if (DEBUG_CMDS) {
                  System.out.println();
                  System.out.println( "current state after Z21 response:" );
                  PiRailComm.dumpStateMessage( currentState );
                }

                deviceData.lastBroadcast = System.currentTimeMillis();
                piRailComm.broadcastXMLState( (State) currentState.clone( null, true, false ) );

                break;
              }
            }
          }
        }
      }

      @Override
      public BroadcastTypes[] getListenerTypes() {
        return new BroadcastTypes[]{ BroadcastTypes.LAN_X_LOCO_INFO };
      }
    } );
  }

  private static int getLocoAddress( RailDevice railDevice ) {
    Cfg railDeviceConfig = railDevice.getConfig();
    Collection<ExtCfg> extCfgs = railDeviceConfig.getExtCfgs();
    if ( !extCfgs.isEmpty() ) {
      ExtCfg extCfg = extCfgs.iterator().next();
      return extCfg.getBusAddrInt();
    }
    return -1;
  }

  private boolean motorCmdMatchesState(MoCmd moCmd, MotorState currentMotorState) {
    String motorStateCur = currentMotorState.getCur();
    Integer motorStateCurI = currentMotorState.getCurI();
    Integer cmdSpeed = moCmd.getSpeed();
    String dir = moCmd.getDir();
    return Objects.equals( motorStateCur, dir ) && Objects.equals( motorStateCurI, cmdSpeed );
  }

  public static int calcZ21TargetSpeed( int permillage ) {
    return (permillage * 1270 + 5) / 10000;
  }

  public static int calcZ21TargetSpeedBackwards( int speed ) {
    int result = (speed * 1000) / 126;
    return result;
  }

//  public static void main( String[] args ) {
//    for ( int i = 0; i <= 1000; i += 10 ) {
//      int z21Speed = calcZ21TargetSpeed( i );
//      int j = calcZ21TargetSpeedBackwards( z21Speed );
//      if ( i != j ) {
//        System.out.println( "Calc mismatch: i = " + i + ", j = " + j + ", z21=" + z21Speed );
//      }
//    }
//  }
}
