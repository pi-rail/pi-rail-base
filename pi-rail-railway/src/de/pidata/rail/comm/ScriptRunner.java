package de.pidata.rail.comm;

import de.pidata.log.Logger;
import de.pidata.qnames.QName;
import de.pidata.rail.model.*;
import de.pidata.rail.railway.*;
import de.pidata.rail.track.TrackCfg;

import java.util.ArrayList;
import java.util.List;

public class ScriptRunner implements Runnable {

  private static int exeCounter = 0;

  private RailAction mainAction;
  private char mainValue;
  private int mainIntValue;
  private QName lockID;
  private RailAction lockAction = null;
  private FunctionType functionType = FunctionType.Switch;
  private String scriptID;
  private Locomotive currentLoco;
  private RailBlock endBlock;

  public ScriptRunner( RailAction railAction, char value, int intValue, QName lockID ) {
    this.mainAction = railAction;
    this.mainValue = value;
    this.mainIntValue = intValue;
    StateScript stateScript = null;
    if (railAction instanceof RailFunction) {
      EnumAction enumAction = (EnumAction) railAction.getAction();
      this.functionType = enumAction.getType();
    }
    applyLock( railAction, stateScript, lockID );
    int x = exeCounter++;
    this.scriptID = "EX"+x;
    new Thread( this, "Script_"+scriptID+"_"+lockID ).start();
  }

  private boolean applyLock( RailAction railAction, StateScript stateScript, QName lockID ) {
    if ((lockID == null) && (railAction instanceof RailFunction) && (stateScript != null)) {
      EnumAction enumAction = (EnumAction) railAction.getAction();
      if ((enumAction.getStartPos() != null) && (stateScript.getEndPos() != null)) {
        QName startID = enumAction.getStartPos().getPosID();
        QName endID = stateScript.getEndPos();
        if ((startID != null) && (endID != null)) {
          lockID = PiRailFactory.NAMESPACE.getQName( startID.getName() + "~" + endID.getName() );
          RailBlock targetBlock = PiRail.getInstance().getModelRailway().getOrCreateBlock( endID );
          if ((lockID != null) && (!targetBlock.reserveFor( lockID ))) {
            Logger.info( "Cannot execute Script: Target Block "+endID.getName()+" is locked" );
            return false;
          }
          else {
            this.lockID = lockID;
            this.lockAction = railAction;
            railAction.setLockID( lockID );
          }
        }
      }
    }
    else {
      this.lockID = lockID;
    }
    return true;
  }

  @Override
  public void run() {
    try {
      String indent = scriptID + ": ";
      Logger.info( indent + "-------" + mainAction + "( " + mainValue + ", " + mainIntValue + " )------" );
      mainAction.execute( this, this.mainValue, this.mainIntValue, indent );
    }
    catch (Exception ex) {
      Logger.error( "Error running script", ex );
    }
  }

  private boolean processPosIDs( StateScript stateScript, String indent ) {
    PiRail piRail = PiRail.getInstance();
    Cmd scheduleCmd = null;
    QName startPos = null;
    QName endPos = null;
    for (Object obj : stateScript.iterator( null, null )) {
      Command cmd = (Command) obj;
      QName relName = cmd.getParentRelationID();
      if (relName == StateScript.ID_ON) {
        MsgState atCmd = (MsgState) cmd;
        QName posID = atCmd.getPos();
        if (posID != null) {
          if (scheduleCmd == null) {
            scheduleCmd = new Cmd();
            SetCmd setCmd = new SetCmd( Locomotive.ACTION_ID_AUTODRIVE, "A", Integer.valueOf( -1 ) );
            scheduleCmd.setSet( setCmd );
          }
          if (startPos == null) {
            startPos = posID;
          }
          // posID also may be a complete block - loco then expects to see all IDs of that block
          scheduleCmd.addMsg( new MsgState( posID, atCmd.getCmd(), atCmd.getCmdDistInt() ) );
          endPos = posID;
        }
      }
      else if (relName == StateScript.ID_CALL) {
        CallCmd callCmd = (CallCmd) cmd;
        RailAction callAction = piRail.getModelRailway().getRailAction( null, callCmd.getActionID() );
        if (callAction instanceof RailFunction) {
          RailFunction callFunc = (RailFunction) callAction;
          if (startPos == null) {
            startPos = callFunc.getStartBlock();
          }
          QName endBlock = callFunc.getEndBlock( callCmd.getChar() );
          if (endBlock != null) {
            endPos = endBlock;
          }
        }
      }
    }
    if (scheduleCmd != null) {
      RailBlock startBlock = piRail.getModelRailway().getOrCreateBlock( startPos );
      Locomotive loco = startBlock.getCurrentLoco();
      if (loco == null) {
        Logger.warn( indent + "Cannot send schedule, no locomotive in " + startBlock );
        this.endBlock = null;
        return false;
      }
      else {
        piRail.sendCmd( loco, scheduleCmd );
        this.currentLoco = loco;
        this.endBlock = piRail.getModelRailway().getOrCreateBlock( endPos );
        Logger.info( indent + "Sent schedule to loco " + currentLoco.getDisplayName() + ", target is " + endBlock );
        return true;
      }
    }
    return true;
  }

  public boolean execScript( RailAction railAction, StateScript stateScript, String indent ) throws InterruptedException {
    Logger.info( indent + "Start " + stateScript );
    QName oldLockID = this.lockID;
    boolean locked;
    do {
      locked = applyLock( railAction, stateScript, this.lockID );
      if (!locked) {
        Thread.sleep( 1000 );
      }
    } while (!locked && (this.functionType == FunctionType.Job));
    if (locked) {
      if (!processPosIDs( stateScript, indent + "  " )) {
        this.lockID = oldLockID;
        Logger.info( indent + "Aborted " + stateScript );
        return false;
      }
      for (Object cmd : stateScript.iterator( null, null )) {
        if (cmd instanceof Command) {
          if (!execCmd( railAction, (Command) cmd, indent + "  " )) {
            Logger.info( indent + "Aborted " + stateScript );
            return false;
          }
        }
      }
      this.lockID = oldLockID;
      Logger.info( indent + "Done " + stateScript );
      return true;
    }
    else {
      this.lockID = oldLockID;
      Logger.info( indent + "Aborted " + stateScript );
      return false;
    }
  }

  private boolean callRemote( RailAction callAction, char charValue, int intValue, String indent ) throws InterruptedException {
    RailDevice callDevice = callAction.getRailDevice();
    if (callDevice == null) {
      //--- Track action - has to be processed by this ScriptRunner
      //    In case of schedule we will repeat until success or our Action's state has changed
      do {
        boolean success = callAction.execute( this, charValue, intValue, indent );
        if (success || (this.functionType != FunctionType.Job)) {
          return success;
        }
        Thread.sleep( 1000 );
      } while (this.mainAction.isActiveValue( this.mainValue ));
      return false;
    }
    else {
      // Device action - is processed on device.
      // We will wait until command is received by device, i.e. is seen in state of callAction.
      boolean received;
      State callDeviceState = callDevice.getState();

      //--- Apply lock to target itemConn if lockID is given. Abort if not successful within 5 sec.
      int timeout = 25;
      if (this.lockID != null) {
        while ((callDeviceState == null) || (this.lockID != callAction.getLockID() )) {
          PiRail.getInstance().sendLockCommand( callAction, this.lockID, true );
          Thread.sleep( 200 );
          timeout--;
          if (timeout <= 0) {
            return false;
          }
        }
      }
      //--- Loop until device 
      timeout = 25;
      do {
        callAction.setValue( charValue, intValue, this.lockID );
        Thread.sleep( 200 );
        received = true;
        ActionState callActionState = callAction.getActionState();
        if ((charValue != '?') && (charValue != callActionState.getTgtChar())) {
          received = false;
        }
        if (intValue != callActionState.getTgtInt()) {
          received = false;
        }
        if (!received) {
          if (callDevice.getWifiState() == WifiState.red) {
            Logger.info( indent + "Device "+callDevice.getDisplayName()+" is offline, call: " + callAction + "( " + charValue + ", " + intValue + " )" );
            return false;
          }
          else {
            Logger.info( indent + "Repeating call: " + callAction + "( " + charValue + ", " + intValue + " )" );
          }
        }
        timeout--;
        if (timeout <= 0) {
          return false;
        }
      }
      while (!received);
      return true;
    }
  }

  private boolean checkIf( IfCmd ifCmd, ItemConn itemConn, String indent ) {
    QName varName = ifCmd.getId();
    if (varName == null) {
      return false;
    }
    QName id = ifCmd.getId();
    Param param = itemConn.getParam( id );
    int checkInt;
    char checkChar;
    if (param == null) {
      Action action = itemConn.getAction( id );
      if (action == null) {
        return false;
      }
      RailAction railAction = PiRail.getInstance().getModelRailway().getRailAction( null, id );
      if (railAction == null) {
        return false;
      }
      ActionState state = railAction.getActionState();
      if (state == null) {
        return false;
      }
      checkChar = state.getCurChar();
      checkInt = state.getCurInt();
    }
    else {
      checkChar = param.getChar();
      checkInt = param.getInt();
    }
    char value = ifCmd.getChar();
    int intVal = ifCmd.getInt();
    if ((value == '=') || (value == 0)) {
      return (intVal == checkInt);
    }
    else if (value == '!') {
      return (intVal != checkInt);
    }
    else if (value == '<') {
      return (intVal < checkInt);
    }
    else if (value == '>') {
      return (intVal > checkInt);
    }
    else {
      return (value == checkChar);
    }
  }

  private boolean execCmd( RailAction railAction, Command cmd, String indent ) throws InterruptedException {
    Logger.info( indent + cmd );
    if (cmd instanceof CallCmd) {
      CallCmd callCmd = (CallCmd) cmd;
      QName actionID = callCmd.getActionID();
      RailAction callAction = PiRail.getInstance().getModelRailway().getRailAction( null, actionID );
      if (callAction == null) {
        Logger.warn( "action not found, id="+actionID );
      }
      else {
        ActionState actionState = railAction.getActionState();
        if (actionState != null) {
          char val = actionState.getCurChar();
          if (val == '$') {
            // Take value from state of this command's Action
            return callRemote( callAction, actionState.getTgtChar(), actionState.getTgtInt(), indent );
          }
          else if (val == '?') {
            // Take value from state of called Action, i.e. rerun that action with its current value
            return callRemote( callAction, '?', actionState.getCurInt(), indent );
          }
          else {
            return callRemote( callAction, callCmd.getChar(), callCmd.getInt(), indent );
          }
        }
      }
    }
    else if (cmd instanceof SetCmd) {
      SetCmd setCmd = (SetCmd) cmd;
      QName paramID = setCmd.getId();
      ItemConn itemConn = railAction.getAction().getItemConn();
      Param param = itemConn.getParam( paramID );
      if (param == null) {
        Logger.warn( indent + "param not found, id="+paramID );
        return false;
      }
      else {
        ActionState actionState = railAction.getActionState();
        if (actionState != null) {
          String val = setCmd.getValue();
          if ("?".equals( val )) {
            if (param.getType() == ValueType.intVal) {
              param.setIntVal( actionState.getTgtI() );
              return true;
            }
            else {
              param.setValue( actionState.getTgt() );
              return true;
            }
          }
          else {
            param.setValue( val );
            return true;
          }
        }
        else {
          if (param.getType() == ValueType.intVal) {
            param.setIntVal( setCmd.getIntVal() );
            return true;
          }
          else {
            param.setValue( setCmd.getValue() );
            return true;
          }
        }
      }
    }
    else if (cmd instanceof IfCmd) {
      IfCmd ifCmd = (IfCmd) cmd;
      ItemConn itemConn = railAction.getAction().getItemConn();
      if (checkIf( ifCmd, itemConn, indent )) {
        for (Object subCmd : ifCmd.iterator( null, null ) ) {
          if (subCmd instanceof Command) {
            if (!execCmd( railAction, (Command) subCmd, indent + "  " )) {
              Logger.warn( indent + "Aborted: "+cmd.getClass() );
              return false;
            }
          }
        }
      }
      return true;
    }
    else if (cmd instanceof MsgState) {
      // Nothing to do - used for collecting posIDs
      return true;
    }
    else {
      Logger.warn( indent + "Unsupported command: "+cmd.getClass() );
    }
    return false;
  }
}
