# PI-Rail

## useful links

### Z21 LAN protocoll

  https://www.z21.eu/media/Kwc_Basic_DownloadTag_Component/47-1652-959-downloadTag/default/69bad87e/1646977660/z21-lan-protokoll.pdf

### LocoNet overview

  https://www.digitrax.com/static/apps/cms/media/documents/documentation/LocoNet%20Overview%20App%20Note.pdf
  https://www.digitrax.com/static/apps/cms/media/documents/loconet/loconetpersonaledition.pdf


Backup copies of the PDFs are available on `projects/projekte/PI-Rail/Doku_extern.`